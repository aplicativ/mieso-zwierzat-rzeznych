import React, { useState, useEffect } from "react";
import clsx from "clsx";
import "./index.scss";

import CssBaseline from "@material-ui/core/CssBaseline";
import BottomNav from "./components/BottomNavigation";
import DataContext from "./context/DataContext";
import ModalContext from "./context/ModalContext";

import Header from "@components/Header";
import MainNavigation from "@components/MainNavigation";
import Content from "@components/Content";

import Modal from "@components/Modal";
import Alert from "@components/Alert";

import initialState from "@data/initialState";
import {
  dragAndDropData,
  meatAndDishesData,
  cullinaryEl,
  beefMeat,
  porkMeat,
  vealMeat,
  sheepMeat,
  deerMeat,
  roeDeerMeat,
  wildBoarMeat,
} from "@data/features";
import {
  task05Data,
  fifthTaskData,
  correctAnswerTask05,
} from "@data/task05/task05";

import { Task04Data } from "@data/task04/data";

import useStyles from "./styles";
import { SnackbarProvider } from "notistack";
import { MuiThemeProvider, createTheme } from "@material-ui/core/styles";

import StepsPdf from "@components/StepsPdf";

import DragDropTransfer from "@components/DragDropTransfer";

import { getLocalStorage, isValidLocalStorage } from "@lib/localStorage";

import printSteps from "@lib/pdf";
import Instruction from "@components/Instruction";

const theme = createTheme({
  palette: {
    primary: {
      main: "#1e1c37",
    },
    secondary: {
      main: "#ec8b00",
    },
  },
  typography: {
    fontFamily: ["OpenSans", "sans-serif"].join(","),
    h2: {
      fontSize: "24px",
      fontWeight: "bold",
      marginBottom: "10px",
      textAlign: "center",
      fontFamily: ["Oswald", "sans-serif"].join(","),
    },
  },
  overrides: {
    MuiTooltip: {
      popper: {
        zIndex: "999999 !important",
      },
    },
  },
});

const modalState = {
  isOpen: false,
  type: null,
  title: null,
  text: null,
};

const alertState = {
  isOpen: false,
  type: null,
  text: null,
};

const App = ({ getImagePath }) => {
  const classes = useStyles();
  const [inputData, setInputData] = useState([]);
  const [state, setState] = useState(initialState);
  const [isFullscreen, setIsFullscreen] = useState(false);
  const { level } = state;
  const { [level]: data } = state;
  const [isOpenNavigation, setIsOpenNavigation] = useState(true);
  const [modalParams, setModalParams] = useState(modalState);
  const [alertParams, setAlertParams] = useState(alertState);
  const [columns, setColumns] = useState(dragAndDropData);
  const [meatAndDishesColumns, setMeatAndDishesColumns] =
    useState(meatAndDishesData);

  const [cullinaryElColumns, setCullinaryElColumns] = useState(cullinaryEl);
  const [beefMeatColumns, setBeefMeatColumns] = useState(beefMeat);
  const [porkMeatColumns, setPorkMeatColumns] = useState(porkMeat);
  const [vealMeatColumns, setVealMeatColumns] = useState(vealMeat);
  const [sheepMeatColumns, setSheepMeatColumns] = useState(sheepMeat);
  const [deerMeatColumns, setDeerMeatColumns] = useState(deerMeat);
  const [roeDeerMeatColumns, setRoeDeerMeatColumns] = useState(roeDeerMeat);
  const [wildBoarMeatColumns, setWildBoarMeatColumns] = useState(wildBoarMeat);
  const [fifthTask, setFifthTask] = useState(fifthTaskData);

  //Drag and drop
  const [task4Columns, setTask4Columns] = useState(Task04Data);

  const [isSecondTask, setIsSecondTask] = useState(false);
  const [isThirdTask, setIsThirdTask] = useState(false);
  const [isFifthTask, setIsFifthTask] = useState(false);
  const [isFourTask, setIsFourTask] = useState(false);
  const [isSixTask, setIsSixTask] = useState(false);

  const [isBeefMeat, setIsBeefMeat] = useState(false);
  const [isPorkMeat, setIsPorkMeat] = useState(false);
  const [isVealMeat, setIsVealMeat] = useState(false);
  const [isSheepMeat, setIsSheepMeat] = useState(false);
  const [isDeerMeat, setIsDeerMeat] = useState(false);
  const [isRoeDeerMeat, setIsRoeDeerMeat] = useState(false);
  const [isWildBoarMeat, setIsWildBoarMeat] = useState(false);

  const appID = "miesozwierzat";
  const [isInstructionOpen, setIsInstructionOpen] = useState(false);

  useEffect(() => {
    if (isFullscreen) {
      setIsOpenNavigation(true);
    }
  }, [isFullscreen]);

  useEffect(() => {
    if (isValidLocalStorage(appID)) {
      setState(JSON.parse(getLocalStorage(appID)));
    }
    if (isValidLocalStorage(appID + "easy1")) {
      setBeefMeatColumns(JSON.parse(getLocalStorage(appID + "easy1")));
    }
    if (isValidLocalStorage(appID + "easy2")) {
      setPorkMeatColumns(JSON.parse(getLocalStorage(appID + "easy2")));
    }
    if (isValidLocalStorage(appID + "easy3")) {
      setVealMeatColumns(JSON.parse(getLocalStorage(appID + "easy3")));
    }
    if (isValidLocalStorage(appID + "easy4")) {
      setSheepMeatColumns(JSON.parse(getLocalStorage(appID + "easy4")));
    }
    if (isValidLocalStorage(appID + "easy5")) {
      setDeerMeatColumns(JSON.parse(getLocalStorage(appID + "easy5")));
    }
    if (isValidLocalStorage(appID + "easy6")) {
      setRoeDeerMeatColumns(JSON.parse(getLocalStorage(appID + "easy6")));
    }
    if (isValidLocalStorage(appID + "easy7")) {
      setWildBoarMeatColumns(JSON.parse(getLocalStorage(appID + "easy7")));
    }
    if (isValidLocalStorage(appID + "hard")) {
      setMeatAndDishesColumns(JSON.parse(getLocalStorage(appID + "hard")));
    }
    if (isValidLocalStorage(appID + "average")) {
      setCullinaryElColumns(JSON.parse(getLocalStorage(appID + "average")));
    }
    if (isValidLocalStorage(appID + "task04")) {
      setTask4Columns(JSON.parse(getLocalStorage(appID + "task04")));
    }
    if (isValidLocalStorage(appID + "task05")) {
      setFifthTask(JSON.parse(getLocalStorage(appID + "task05")));
    }
    if (isValidLocalStorage(appID + "task06")) {
      setInputData(JSON.parse(getLocalStorage(appID + "task06")));
    }
  }, []);

  let columnsInTasks;
  let setColumnsInTasks;
  if (level === "easy" && data.typeOfMeat === "beefMeat") {
    columnsInTasks = beefMeatColumns;
    setColumnsInTasks = setBeefMeatColumns;
  } else if (level === "easy" && data.typeOfMeat === "porkMeat") {
    columnsInTasks = porkMeatColumns;
    setColumnsInTasks = setPorkMeatColumns;
  } else if (level === "easy" && data.typeOfMeat === "vealMeat") {
    columnsInTasks = vealMeatColumns;
    setColumnsInTasks = setVealMeatColumns;
  } else if (level === "easy" && data.typeOfMeat === "sheepMeat") {
    columnsInTasks = sheepMeatColumns;
    setColumnsInTasks = setSheepMeatColumns;
  } else if (level === "easy" && data.typeOfMeat === "deerMeat") {
    columnsInTasks = deerMeatColumns;
    setColumnsInTasks = setDeerMeatColumns;
  } else if (level === "easy" && data.typeOfMeat === "roeDeerMeat") {
    columnsInTasks = roeDeerMeatColumns;
    setColumnsInTasks = setRoeDeerMeatColumns;
  } else if (level === "easy" && data.typeOfMeat === "wildBoarMeat") {
    columnsInTasks = wildBoarMeatColumns;
    setColumnsInTasks = setWildBoarMeatColumns;
  } else if (level === "average") {
    columnsInTasks = cullinaryElColumns;
    setColumnsInTasks = setCullinaryElColumns;
  } else if (level === "hard") {
    columnsInTasks = meatAndDishesColumns;
    setColumnsInTasks = setMeatAndDishesColumns;
  } else if (level === "task04") {
    columnsInTasks = task4Columns;
    setColumnsInTasks = setTask4Columns;
  } else {
    columnsInTasks = columns;
    setColumnsInTasks = setColumns;
  }

  return (
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
      >
        <ModalContext.Provider
          value={{
            modalParams,
            setModalParams,
            alertParams,
            setAlertParams,
            isInstructionOpen,
            setIsInstructionOpen,
          }}
        >
          <DataContext.Provider
            value={{
              inputData,
              setInputData,
              printSteps,
              state,
              data,
              setState,
              level,
              getImagePath,
              isFullscreen,
              setIsFullscreen,
              columns,
              setColumns,
              appID,
              columnsInTasks,
              setColumnsInTasks,
              meatAndDishesColumns,
              setMeatAndDishesColumns,
              isThirdTask,
              setIsThirdTask,
              isSecondTask,
              setIsSecondTask,
              isBeefMeat,
              setIsBeefMeat,
              isPorkMeat,
              setIsPorkMeat,
              isVealMeat,
              setIsVealMeat,
              isSheepMeat,
              setIsSheepMeat,
              isDeerMeat,
              setIsDeerMeat,
              isRoeDeerMeat,
              setIsRoeDeerMeat,
              isWildBoarMeat,
              setIsWildBoarMeat,
              cullinaryElColumns,
              isFifthTask,
              setIsFifthTask,
              setFifthTask,
              fifthTask,
              fifthTaskData,
              task05Data,
              correctAnswerTask05,
              task4Columns,
              isFourTask,
              setIsFourTask,
              beefMeatColumns,
              porkMeatColumns,
              vealMeatColumns,
              sheepMeatColumns,
              roeDeerMeatColumns,
              wildBoarMeatColumns,
              deerMeatColumns,
              isSixTask,
              setIsSixTask,
              setTask4Columns,
            }}
          >
            <div
              className={clsx(`${classes.root}`, {
                [classes.fullScreen]: isFullscreen,
              })}
            >
              <CssBaseline />
              <DragDropTransfer>
                <Header
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />
                <MainNavigation
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />

                <main
                  className={clsx(classes.content, {
                    [classes.contentShift]: isOpenNavigation,
                  })}
                >
                  <div className={classes.drawerHeader} />
                  <div className={classes.task} id="app">
                    <Content />
                  </div>
                  <footer
                    className={clsx(classes.footer, {
                      [classes.contentShift]: isOpenNavigation,
                    })}
                  >
                    <BottomNav />
                  </footer>
                </main>
              </DragDropTransfer>
            </div>

            <Modal />
            <Alert />
            <StepsPdf />
            <Instruction />
          </DataContext.Provider>
        </ModalContext.Provider>
      </SnackbarProvider>
    </MuiThemeProvider>
  );
};

export default App;
