export const isValidLocalStorage = (appID) => {
    const payload = getLocalStorage(appID);
    return !(payload === "undefined" || payload == undefined || payload === null || payload === "null")
        ? true
        : false;
}


const setLocalStorage = (name, payload) => {
    // console.log(name, JSON.stringify(payload));
    localStorage.setItem(name, JSON.stringify(payload))
}

const getLocalStorage = (name) => (
    localStorage.getItem(name)
)


export { setLocalStorage, getLocalStorage };
