const methods = [
  {
    key: "beefMeat",
    name: "Półtusza wołowa",
    level: ["all"],
  },
  {
    key: "porkMeat",
    name: "Półtusza wieprzowa",
    level: ["all"],
  },
  {
    key: "vealMeat",
    name: "Półtusza cielęca",
    level: ["all"],
  },
  {
    key: "sheepMeat",
    name: "Półtusza barania",
    level: ["all"],
  },
  {
    key: "deerMeat",
    name: "Półtusza jelenia",
    level: ["all"],
  },
  {
    key: "roeDeerMeat",
    name: "Półtusza sarny",
    level: ["all"],
  },
  {
    key: "wildBoarMeat",
    name: "Półtusza dzika",
    level: ["all"],
  },
];

const meatAndDishesData = {
  menu: {
    id: "menu",
    list: [
      { key: "5", text: "Antrykot" },
      { key: "10", text: "Frykando" },
      { key: "15", text: "Karkówka" },
      { key: "2", text: "Mostek" },
      { key: "7", text: "Polędwica" },
      { key: "16", text: "Polędwica" },
      { key: "8", text: "Rostbef" },
      { key: "4", text: "Rozbratel" },
      { key: "13", text: "Schab" },
      { key: "9", text: "Schab" },
      { key: "3", text: "Szponder" },
      { key: "6", text: "Szyja wołowa" },
      { key: "14", text: "Szynka" },
      { key: "11", text: "Szynka" },
      { key: "12", text: "Udziec" },
      { key: "17", text: "Karkówka wołowa" },
    ],
  },
  thirdTask1: {
    id: "thirdTask1",
    list: [],
  },
  thirdTask2: {
    id: "thirdTask2",
    list: [],
  },
  thirdTask3: {
    id: "thirdTask3",
    list: [],
  },
  thirdTask4: {
    id: "thirdTask4",
    list: [],
  },
  thirdTask5: {
    id: "thirdTask5",
    list: [],
  },

  prosAndCons: {
    id: "prosAndCons",
    list: [],
  },
};
const cullinaryEl = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Boczek", type: "element" },
      { key: "2", text: "Łopatka b/k", type: "element" },
      { key: "3", text: "Golonka przednia", type: "element" },
      { key: "4", text: "Biodrówka", type: "element" },
      { key: "5", text: "Szynka", type: "element" },
      { key: "6", text: "gotowane lub smażone", type: "application" },
      {
        key: "7",
        text: "potrawy pieczone, potrawy duszone, masa mielona",
        type: "application",
      },
      { key: "8", text: "gotowane", type: "application" },
      {
        key: "9",
        text: "potrawy pieczone, potrawy duszone, potrawy smażone",
        type: "application",
      },
      {
        key: "10",
        text: "pieczeń duszona, bryzole, zrazy bite, sznycle panierowane",
        type: "application",
      },
    ],
  },

  content18: {
    id: "content18",
    list: [],
  },
  content19: {
    id: "content19",
    list: [],
  },
  content20: {
    id: "content20",
    list: [],
  },
  content21: {
    id: "content21",
    list: [],
  },
  content22: {
    id: "content22",
    list: [],
  },
  content23: {
    id: "content23",
    list: [],
  },
  content24: {
    id: "content24",
    list: [],
  },
  content25: {
    id: "content25",
    list: [],
  },
  content26: {
    id: "content26",
    list: [],
  },
  content27: {
    id: "content27",
    list: [],
  },
};

const beefMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Szyja" },
      { key: "2", text: "Karkówka" },
      { key: "3", text: "Mięso z łopatki" },
      { key: "4", text: "Mostek" },
      { key: "5", text: "Szponder" },
      { key: "6", text: "Rozbratel" },
      { key: "7", text: "Antrykot" },
      { key: "8", text: "Pręga przednia" },
      { key: "9", text: "Polędwica" },
      { key: "10", text: "Udziec" },
      { key: "11", text: "Rostbef" },
      { key: "12", text: "Łata" },
      { key: "13", text: "Pręga tylna" },
      { key: "14", text: "Ogon" },
    ],
  },

  contentBeef1: {
    id: "contentBeef1",
    list: [],
  },
  contentBeef2: {
    id: "contentBeef2",
    list: [],
  },
  contentBeef3: {
    id: "contentBeef3",
    list: [],
  },
  contentBeef4: {
    id: "contentBeef4",
    list: [],
  },
  contentBeef5: {
    id: "contentBeef5",
    list: [],
  },
  contentBeef6: {
    id: "contentBeef6",
    list: [],
  },
  contentBeef7: {
    id: "contentBeef7",
    list: [],
  },
  contentBeef8: {
    id: "contentBeef8",
    list: [],
  },
  contentBeef9: {
    id: "contentBeef9",
    list: [],
  },
  contentBeef10: {
    id: "contentBeef10",
    list: [],
  },
  contentBeef11: {
    id: "contentBeef11",
    list: [],
  },
  contentBeef12: {
    id: "contentBeef12",
    list: [],
  },
  contentBeef13: {
    id: "contentBeef13",
    list: [],
  },
  contentBeef14: {
    id: "contentBeef14",
    list: [],
  },
};

const porkMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Głowa z ryjem" },
      { key: "2", text: "Podgardle" },
      { key: "3", text: "Karkówka" },
      { key: "4", text: "Schab" },
      { key: "5", text: "Biodrówka" },
      { key: "6", text: "Szynka" },
      { key: "7", text: "Łopatka" },
      { key: "8", text: "Żeberka" },
      { key: "9", text: "Boczek" },
      { key: "10", text: "Pachwina" },
      { key: "11", text: "Golonka tylna" },
      { key: "12", text: "Golonka przednia" },
      { key: "13", text: "Nogi" },
      { key: "14", text: "Słonina" },
      { key: "15", text: "Ogon" },
    ],
  },

  contentPork1: {
    id: "contentPork1",
    list: [],
  },
  contentPork2: {
    id: "contentPork2",
    list: [],
  },
  contentPork3: {
    id: "contentPork3",
    list: [],
  },
  contentPork4: {
    id: "contentPork4",
    list: [],
  },
  contentPork5: {
    id: "contentPork5",
    list: [],
  },
  contentPork6: {
    id: "contentPork6",
    list: [],
  },
  contentPork7: {
    id: "contentPork7",
    list: [],
  },
  contentPork8: {
    id: "contentPork8",
    list: [],
  },
  contentPork9: {
    id: "contentPork9",
    list: [],
  },
  contentPork10: {
    id: "contentPork10",
    list: [],
  },
  contentPork11: {
    id: "contentPork11",
    list: [],
  },
  contentPork12: {
    id: "contentPork12",
    list: [],
  },
  contentPork13: {
    id: "contentPork13",
    list: [],
  },
  contentPork14: {
    id: "contentPork14",
    list: [],
  },
  contentPork15: {
    id: "contentPork15",
    list: [],
  },
};

const vealMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Szyja" },
      { key: "2", text: "Łopatka" },
      { key: "3", text: "Goleń przednia" },
      { key: "4", text: "Karkówka" },
      { key: "5", text: "Górka" },
      { key: "6", text: "Nerkówka" },
      { key: "7", text: "Mostek" },
      { key: "8", text: "Łata" },
      { key: "9", text: "Udziec" },
      { key: "10", text: "Goleń tylna" },
      { key: "11", text: "Ogon" },
      { key: "12", text: "Szponder" },
    ],
  },

  contentVeal1: {
    id: "contentVeal1",
    list: [],
  },
  contentVeal2: {
    id: "contentVeal2",
    list: [],
  },
  contentVeal3: {
    id: "contentVeal3",
    list: [],
  },
  contentVeal4: {
    id: "contentVeal4",
    list: [],
  },
  contentVeal5: {
    id: "contentVeal5",
    list: [],
  },
  contentVeal6: {
    id: "contentVeal6",
    list: [],
  },
  contentVeal7: {
    id: "contentVeal7",
    list: [],
  },
  contentVeal8: {
    id: "contentVeal8",
    list: [],
  },
  contentVeal9: {
    id: "contentVeal9",
    list: [],
  },
  contentVeal10: {
    id: "contentVeal10",
    list: [],
  },
  contentVeal11: {
    id: "contentVeal11",
    list: [],
  },
  contentVeal12: {
    id: "contentVeal12",
    list: [],
  },
};
const sheepMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Karkówka" },
      { key: "2", text: "Górka" },
      { key: "3", text: "Antrykot" },
      { key: "4", text: "Comber" },
      { key: "5", text: "Goleń przednia" },
      { key: "6", text: "Mostek" },
      { key: "7", text: "Udziec" },
      { key: "8", text: "Goleń tylna" },
      { key: "9", text: "Ogon" },
    ],
  },

  contentSheep1: {
    id: "contentSheep1",
    list: [],
  },
  contentSheep2: {
    id: "contentSheep2",
    list: [],
  },
  contentSheep3: {
    id: "contentSheep3",
    list: [],
  },
  contentSheep4: {
    id: "contentSheep4",
    list: [],
  },
  contentSheep5: {
    id: "contentSheep5",
    list: [],
  },
  contentSheep6: {
    id: "contentSheep6",
    list: [],
  },
  contentSheep7: {
    id: "contentSheep7",
    list: [],
  },
  contentSheep8: {
    id: "contentSheep8",
    list: [],
  },
  contentSheep9: {
    id: "contentSheep9",
    list: [],
  },
};
const deerMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Udziec" },
      { key: "2", text: "Goleń tylna" },
      { key: "3", text: "Rostbef" },
      { key: "4", text: "Łata" },
      { key: "5", text: "Polędwica" },
      { key: "6", text: "Górnica" },
      { key: "7", text: "Szponder" },
      { key: "8", text: "Mostek" },
      { key: "9", text: "Łopatka" },
      { key: "10", text: "Goleń przednia" },
      { key: "11", text: "Szyja" },
    ],
  },

  contentDeer1: {
    id: "contentDeer1",
    list: [],
  },
  contentDeer2: {
    id: "contentDeer2",
    list: [],
  },
  contentDeer3: {
    id: "contentDeer3",
    list: [],
  },
  contentDeer4: {
    id: "contentDeer4",
    list: [],
  },
  contentDeer5: {
    id: "contentDeer5",
    list: [],
  },
  contentDeer6: {
    id: "contentDeer6",
    list: [],
  },
  contentDeer7: {
    id: "contentDeer7",
    list: [],
  },
  contentDeer8: {
    id: "contentDeer8",
    list: [],
  },
  contentDeer9: {
    id: "contentDeer9",
    list: [],
  },
  contentDeer10: {
    id: "contentDeer10",
    list: [],
  },
  contentDeer11: {
    id: "contentDeer11",
    list: [],
  },
};

const roeDeerMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Udziec" },
      { key: "2", text: "Goleń tylna" },
      { key: "3", text: "Comber" },
      { key: "4", text: "Łopatka" },
      { key: "5", text: "Goleń przednia" },
      { key: "6", text: "Mostek" },
      { key: "7", text: "Karczek" },
      { key: "8", text: "Szyja" },
    ],
  },

  contentRoeDeer1: {
    id: "contentRoeDeer1",
    list: [],
  },
  contentRoeDeer2: {
    id: "contentRoeDeer2",
    list: [],
  },
  contentRoeDeer3: {
    id: "contentRoeDeer3",
    list: [],
  },
  contentRoeDeer4: {
    id: "contentRoeDeer4",
    list: [],
  },
  contentRoeDeer5: {
    id: "contentRoeDeer5",
    list: [],
  },
  contentRoeDeer6: {
    id: "contentRoeDeer6",
    list: [],
  },
  contentRoeDeer7: {
    id: "contentRoeDeer7",
    list: [],
  },
  contentRoeDeer8: {
    id: "contentRoeDeer8",
    list: [],
  },
};

const wildBoarMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", text: "Głowa" },
      { key: "2", text: "Podgardle" },
      { key: "3", text: "Łopatka" },
      { key: "4", text: "Karkówka" },
      { key: "5", text: "Schab" },
      { key: "6", text: "Szynka z biodrówką" },
      { key: "7", text: "Żeberka z boczkiem" },
      { key: "8", text: "Goleń tylna" },
      { key: "9", text: "Goleń przednia" },
    ],
  },

  contentWildBoar1: {
    id: "contentWildBoar1",
    list: [],
  },
  contentWildBoar2: {
    id: "contentWildBoar2",
    list: [],
  },
  contentWildBoar3: {
    id: "contentWildBoar3",
    list: [],
  },
  contentWildBoar4: {
    id: "contentWildBoar4",
    list: [],
  },
  contentWildBoar5: {
    id: "contentWildBoar5",
    list: [],
  },
  contentWildBoar6: {
    id: "contentWildBoar6",
    list: [],
  },
  contentWildBoar7: {
    id: "contentWildBoar7",
    list: [],
  },
  contentWildBoar8: {
    id: "contentWildBoar8",
    list: [],
  },
  contentWildBoar9: {
    id: "contentWildBoar9",
    list: [],
  },
};
const dragAndDropData = {
  menu: {
    id: "menu",
    list: [],
  },
};

const typesOfMeat = {
  menu: {
    id: "menu",
    list: [
      { key: "1", type: "pork", text: "Golonka" },
      { key: "2", type: "pork", text: "Łopatka" },
      { key: "3", type: "pork", text: "Schab bez kości" },
      { key: "4", type: "beef", text: "Zrazowa" },
      { key: "5", type: "beef", text: "Rostbef" },
      { key: "6", type: "beef", text: "Skrzydło" },
      { key: "7", type: "veal", text: "Łopatka" },
      { key: "8", type: "veal", text: "Górka" },
      { key: "9", type: "veal", text: "Mostek" },
    ],
  },
};

const dataTask6 = [
  {
    key: "point1",
    title:
      "Głowizna to cienkie pasma tkanki mięśniowej, przerośnięte tłuszczem oraz warstwy skóry na grubym podkładzie tkanki tłuszczowej.",
    descriptionTrue:
      "Brawo! To jest poprawna odpowiedź. Głowizna to cienkie pasma tkanki mięśniowej, przerośnięte tłuszczem oraz warstwy skóry na grubym podkładzie tkanki tłuszczowej.",
    descriptionFalse:
      "Niestety nie! To nie jest poprawna odpowiedź. Głowizna to właśnie cienkie pasma tkanki mięśniowej, przerośnięte tłuszczem oraz warstwy skóry na grubym podkładzie tkanki tłuszczowej.",
    correctAnswer: true,
  },
  {
    key: "point2",
    title:
      "Górka to element odcięty z tylnego odcinka górnej partii piersiowej półtuszy wieprzowej. Jest mięśniem zwartym, jednolitym, przebiegającym wzdłuż kręgosłupa zwierzęcia, od udźca aż do karku, o dość delikatnej strukturze..",
    descriptionTrue:
      "Niestety, to nie jest poprawna odpowiedź. Górka to element odcięty z tylnego odcinka górnej partii piersiowej półtuszy cielęcej. Jest mięśniem zwartym, jednolitym, przebiegającym wzdłuż kręgosłupa zwierzęcia, od udźca aż do karku, o dość delikatnej strukturze.",
    descriptionFalse:
      "Brawo! To jest poprawna odpowiedź. Górka to element odcięty z tylnego odcinka górnej partii piersiowej półtuszy cielęcej. Jest mięśniem zwartym, jednolitym, przebiegającym wzdłuż kręgosłupa zwierzęcia, od udźca aż do karku, o dość delikatnej strukturze.",
    correctAnswer: false,
  },

  {
    key: "point3",
    title:
      "Do przygotowania paprykarza wykorzystuje się cielęcinę lub jagnięcinę z kością. Najpierw mięso kroi się na małe kawałki, a następnie dodaje się cebulę, sproszkowaną słodką paprykę, pomidory oraz kwaśną śmietanę.",
    descriptionTrue:
      "Brawo! To jest poprawna odpowiedź. Do przygotowania paprykarza wykorzystuje się cielęcinę lub jagnięcinę z kością. Najpierw mięso kroi się na małe kawałki, a następnie dodaje się cebulę, sproszkowaną słodką paprykę, pomidory oraz kwaśną śmietanę.",
    descriptionFalse:
      "Niestety nie! To nie jest poprawna odpowiedź. Do przygotowania paprykarza wykorzystuje się właśnie cielęcinę lub jagnięcinę z kością. Najpierw mięso kroi się na małe kawałki, a następnie dodaje się cebulę, sproszkowaną słodką paprykę, pomidory oraz kwaśną śmietanę.",
    correctAnswer: true,
  },
  {
    key: "point4",
    title:
      "Do sporządzania potraw duszonych wykorzystuje się mięso zawierające niewielką ilość tkanki łącznej.",
    descriptionTrue:
      "Niestety, to nie jest poprawna odpowiedź. Do sporządzania potraw duszonych wykorzystuje się mięso zawierające średnią ilość tkanki łącznej.",
    descriptionFalse:
      "Brawo! To jest poprawna odpowiedź. Do sporządzania potraw duszonych wykorzystuje się mięso zawierające średnią ilość tkanki łącznej.",
    correctAnswer: false,
  },
  {
    key: "point5",
    title: "Befsztyk po angielsku przygotowuje się z polędwicy wołowej.",
    descriptionTrue:
      "Brawo! To jest poprawna odpowiedź. Befsztyk po angielsku przygotowuje się z polędwicy wołowej.",
    descriptionFalse:
      "Niestety nie! To nie jest poprawna odpowiedź. Befsztyk po angielsku przygotowuje się z polędwicy wołowej.",
    correctAnswer: true,
  },
  {
    key: "point6",
    title:
      "Cyrkulator temperatury sous vide służy do gotowania obgotowanych produktów pakowanych próżniowo, w niskiej temperaturze. Można w nim gotować takie produkty jak: <ul><li>cielęcinę, 30-35ºC;</li><li>wołowinę, 45-63°C;</li> <li>wieprzowinę, 40- 68°C.</li></ul>",
    descriptionTrue:
      "Niestety, to nie jest poprawna odpowiedź. Cyrkulator temperatury sous vide służy do gotowania surowych produktów pakowanych próżniowo, w niskiej temperaturze. Można w nim gotować takie produkty jak: <ul> <li>cielęcinę, 65-68ºC;</li> <li>wołowinę, 54-63°C</li><li>wieprzowinę, 52-68°C.</li></ul>",
    descriptionFalse:
      "Brawo! To jest poprawna odpowiedź. Cyrkulator temperatury sous vide służy do gotowania surowych produktów pakowanych próżniowo, w niskiej temperaturze. Można w nim gotować takie produkty jak: <ul><li>cielęcinę, 65-68ºC;</li><li>wołowinę, 54-63°C</li><li>wieprzowinę, 52-68°C</li>",
    correctAnswer: false,
  },
  {
    key: "point7",
    title:
      "Kotły warzelne służą do gotowania potraw płynnych i półpłynnych, w komorze roboczej o dużej pojemności – nawet do 120 l, takich jak:<ul><li> zupy</li> <li>wywary</li><li>potrawki.</li>",
    descriptionTrue:
      "Brawo! To jest poprawna odpowiedź. Kotły warzelne służą do gotowania potraw płynnych i półpłynnych, w komorze roboczej o dużej pojemności – nawet do 120 l, takich jak: <ul><li>zupy;</li><li>wywary;</li> <li>potrawki.</li></ul>",
    descriptionFalse:
      "Niestety nie! To nie jest poprawna odpowiedź. Kotły warzelne służą do gotowania potraw płynnych i półpłynnych, w komorze roboczej o dużej pojemności nawet do 120 l, takich jak: <ul><li>zupy;</li> <li>wywary;</li> <li>potrawki.</li> </ul>",
    correctAnswer: true,
  },
];

export {
  dragAndDropData,
  methods,
  meatAndDishesData,
  cullinaryEl,
  beefMeat,
  porkMeat,
  vealMeat,
  sheepMeat,
  deerMeat,
  roeDeerMeat,
  wildBoarMeat,
  typesOfMeat,
  dataTask6,
};
