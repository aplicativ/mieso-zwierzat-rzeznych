const options = [
  {
    key: "Pork",
    name: "Półtłusza wiepszowa",
    level: ["all"],
  },
  {
    key: "Beef",
    name: "Półtłusza wołowa",
    level: ["all"],
  },
  {
    key: "Veal",
    name: "Półtłusza cielenca",
    level: ["all"],
  },
];

const subOptions = [
  {
    key: "element",
    name: "Element gastronomiczny",
    level: ["all"],
  },
  {
    key: "dish",
    name: "Potrawa",
    level: ["all"],
  },
  {
    key: "method",
    name: "Metoda obróbki cieplnej",
    level: ["all"],
  },
];

const Task04Data = {
  menu: {
    id: "menu",
    list: [
      { key: "0", text: "Karkówka", category: "Elements", type: "Pork" },
      {
        key: "1",
        text: "Golonka przednia i tylna",
        category: "Elements",
        type: "Pork",
      },
      {
        key: "2",
        text: "Łopatka wieprzowa",
        category: "Elements",
        type: "Pork",
      },
      { key: "3", text: "Szynka", category: "Elements", type: "Pork" },
      { key: "4", text: "Żeberka", category: "Elements", type: "Pork" },
      { key: "5", text: "Schab", category: "Elements", type: "Pork" },
      {
        key: "6",
        text: "Zrazowa wewnętrzna",
        category: "Elements",
        type: "Beef",
      },
      { key: "7", text: "Rostbef", category: "Elements", type: "Beef" },
      { key: "8", text: "Pręga przednia", category: "Elements", type: "Beef" },
      { key: "9", text: "Polędwica", category: "Elements", type: "Beef" },
      { key: "10", text: "Skrzydło", category: "Elements", type: "Beef" },
      { key: "11", text: "Górka", category: "Elements", type: "Veal" },
      { key: "12", text: "Mostek", category: "Elements", type: "Veal" },
      {
        key: "13",
        text: "Łopatka cielęca",
        category: "Elements",
        type: "Veal",
      },
      { key: "14", text: "Golonka peklowana", category: "Dish", type: "Pork" },
      { key: "15", text: "Gulasz wieprzowy", category: "Dish", type: "Pork" },
      {
        key: "16",
        text: "Klopsiki w sosie pomidorowym",
        category: "Dish",
        type: "Pork",
      },
      { key: "17", text: "Szaszłyki", category: "Dish", type: "Pork" },
      {
        key: "18",
        text: "Żeberka wieprzowe gotowane",
        category: "Dish",
        type: "Pork",
      },
      { key: "19", text: "Filet wieprzowy", category: "Dish", type: "Pork" },
      {
        key: "20",
        text: "Zrazy wołowe bite w sosie śmietanowym",
        category: "Dish",
        type: "Beef",
      },
      { key: "21", text: "Boeuf Strogonow", category: "Dish", type: "Beef" },
      { key: "22", text: "Bulion mięsny", category: "Dish", type: "Beef" },
      { key: "23", text: "Bryzol wołowy", category: "Dish", type: "Beef" },
      {
        key: "24",
        text: "Befsztyk po angielsku",
        category: "Dish",
        type: "Beef",
      },
      {
        key: "25",
        text: "Sznycel ministerski",
        category: "Dish",
        type: "Veal",
      },
      { key: "26", text: "Potrawka cielęca", category: "Dish", type: "Veal" },
      {
        key: "27",
        text: "Mostek cielęcy panierowany po wiedeńsku",
        category: "Dish",
        type: "Veal",
      },
      { key: "28", text: "Z masy mielonej", category: "Methods" },
      { key: "29", text: "Gotowanie", category: "Methods" },
      { key: "30", text: "Duszenie", category: "Methods" },
      { key: "31", text: "Smażenie", category: "Methods" },
      { key: "32", text: "Gotowanie", category: "Methods" },
      { key: "33", text: "Smażenie", category: "Methods" },
      { key: "34", text: "Gotowanie", category: "Methods" },
      { key: "35", text: "Smażenie", category: "Methods" },
      { key: "36", text: "Duszenie", category: "Methods" },
      { key: "37", text: "Duszenie", category: "Methods" },
      { key: "38", text: "Smażenie po angielsku", category: "Methods" },
      { key: "39", text: "Smażenie", category: "Methods" },
      { key: "40", text: "Gotowanie", category: "Methods" },
      { key: "41", text: "Z masy mielonej", category: "Methods" },
    ],
  },

  //Element

  contentPorkElements0: {
    id: "contentPorkElements0",
    list: [],
  },
  contentPorkElements1: {
    id: "contentPorkElements1",
    list: [],
  },
  contentPorkElements2: {
    id: "contentPorkElements2",
    list: [],
  },
  contentPorkElements3: {
    id: "contentPorkElements3",
    list: [],
  },
  contentPorkElements4: {
    id: "contentPorkElements4",
    list: [],
  },
  contentPorkElements5: {
    id: "contentPorkElements5",
    list: [],
  },

  contentBeefElements0: {
    id: "contentBeefElements0",
    list: [],
  },
  contentBeefElements1: {
    id: "contentBeefElements1",
    list: [],
  },
  contentBeefElements2: {
    id: "contentBeefElements2",
    list: [],
  },
  contentBeefElements3: {
    id: "contentBeefElements3",
    list: [],
  },
  contentBeefElements4: {
    id: "contentBeefElements4",
    list: [],
  },

  contentVealElements0: {
    id: "contentVealElements0",
    list: [],
  },
  contentVealElements1: {
    id: "contentVealElements1",
    list: [],
  },
  contentVealElements2: {
    id: "contentVealElements2",
    list: [],
  },

  //Dish

  contentPorkDish0: {
    id: "contentPorkDish0",
    list: [],
  },
  contentPorkDish1: {
    id: "contentPorkDish1",
    list: [],
  },
  contentPorkDish2: {
    id: "contentPorkDish2",
    list: [],
  },
  contentPorkDish3: {
    id: "contentPorkDish3",
    list: [],
  },
  contentPorkDish4: {
    id: "contentPorkDish4",
    list: [],
  },
  contentPorkDish5: {
    id: "contentPorkDish5",
    list: [],
  },

  contentBeefDish0: {
    id: "contentBeefDish0",
    list: [],
  },
  contentBeefDish1: {
    id: "contentBeefDish1",
    list: [],
  },
  contentBeefDish2: {
    id: "contentBeefDish2",
    list: [],
  },
  contentBeefDish3: {
    id: "contentBeefDish3",
    list: [],
  },
  contentBeefDish4: {
    id: "contentBeefDish4",
    list: [],
  },

  contentVealDish0: {
    id: "contentVealDish0",
    list: [],
  },
  contentVealDish1: {
    id: "contentVealDish1",
    list: [],
  },
  contentVealDish2: {
    id: "contentVealDish2",
    list: [],
  },

  //Methods

  contentPorkMethods0: {
    id: "contentPorkMethods0",
    list: [],
  },
  contentPorkMethods1: {
    id: "contentPorkMethods1",
    list: [],
  },
  contentPorkMethods2: {
    id: "contentPorkMethods2",
    list: [],
  },
  contentPorkMethods3: {
    id: "contentPorkMethods3",
    list: [],
  },
  contentPorkMethods4: {
    id: "contentPorkMethods4",
    list: [],
  },
  contentPorkMethods5: {
    id: "contentPorkMethods5",
    list: [],
  },

  contentBeefMethods0: {
    id: "contentBeefMethods0",
    list: [],
  },
  contentBeefMethods1: {
    id: "contentBeefMethods1",
    list: [],
  },
  contentBeefMethods2: {
    id: "contentBeefMethods2",
    list: [],
  },
  contentBeefMethods3: {
    id: "contentBeefMethods3",
    list: [],
  },
  contentBeefMethods4: {
    id: "contentBeefMethods4",
    list: [],
  },

  contentVealMethods0: {
    id: "contentVealMethods0",
    list: [],
  },
  contentVealMethods1: {
    id: "contentVealMethods1",
    list: [],
  },
  contentVealMethods2: {
    id: "contentVealMethods2",
    list: [],
  },
};

const correctAnswerTask04 = [
  { name: "Łopatka wieprzowa", dish: "Gulasz wieprzowy", methods: "Duszenie" },
  {
    name: "Golonka przednia i tylna",
    dish: "Golonka peklowana",
    methods: "Gotowanie",
  },
  {
    name: "Karkówka",
    dish: "Klopsiki w sosie pomidorowym",
    methods: "Z masy mielonej",
  },
  {
    name: "Schab",
    dish: "Filet wieprzowy",
    methods: "Smażenie",
  },
  {
    name: "Żeberka",
    dish: "Żeberka wieprzowe gotowane",
    methods: "Gotowanie",
  },
  {
    name: "Szynka",
    dish: "Szaszłyki",
    methods: "Smażenie",
  },
  {
    name: "Pręga przednia",
    dish: "Bulion mięsny",
    methods: "Gotowanie",
  },
  {
    name: "Rostbef",
    dish: "Boeuf Strogonow",
    methods: "Duszenie",
  },
  {
    name: "Zrazowa wewnętrzna",
    dish: "Zrazy wołowe bite w sosie śmietanowym",
    methods: "Duszenie",
  },
  {
    name: "Skrzydło",
    dish: "Bryzol wołowy",
    methods: "Smażenie",
  },
  {
    name: "Polędwica",
    dish: "Befsztyk po angielsku",
    methods: "Smażenie po angielsku",
  },
  {
    name: "Mostek",
    dish: "Mostek cielęcy panierowany po wiedeńsku",
    methods: "Smażenie",
  },
  {
    name: "Górka",
    dish: "Potrawka cielęca",
    methods: "Gotowanie",
  },
  {
    name: "Łopatka cielęca",
    dish: "Sznycel ministerski",
    methods: "Z masy mielonej",
  },
];

export { Task04Data, options, subOptions, correctAnswerTask04 };
