const wheatData = [
  {
    key: "450",
    name: "typ 450",
    level: ["average"],
    description: "",
  },
  {
    key: "550",
    name: "typ 550",
    level: ["average"],
    description: "",
  },

  {
    key: "650",
    name: "typ 650",
    level: ["average"],
    description: "",
  },
  {
    key: "750",
    name: "typ 750",
    level: ["average"],
    description: "",
  },
  {
    key: "1050",
    name: "typ 1050",
    level: ["average"],
    description: "",
  },
  {
    key: "1400",
    name: "typ 1400",
    level: ["average"],
    description: "",
  },
  {
    key: "1850",
    name: "typ 1850",
    level: ["average"],
    description: "",
  },
  {
    key: "2000",
    name: "typ 2000",
    level: ["average"],
    description: "",
  },
];
const productsData = [
  {
    key: "beef",
    name: "Półtusza wołowa",
    level: ["average"],

    validFabrics: ["linen", "cotton", "jeans", "velvet"],
    validType: ["woman"],
  },
  {
    key: "pork",
    name: "Półtusza wieprzowa",
    level: ["average"],
    validFabrics: ["linen", "cotton", "jeans", "velvet"],
    validType: ["woman"],
  },
  {
    key: "veal",
    name: "Półtusza cielęca",
    level: ["average"],
    validFabrics: ["linen", "cotton", "jeans", "velvet"],
    validType: ["woman"],
  },
  {
    key: "sheep",
    name: "Półtusza barania",
    level: ["average"],
    validFabrics: ["linen", "cotton", "jeans", "velvet"],
    validType: ["woman"],
  },
  {
    key: "deer",
    name: "Półtusza jelenia",
    level: ["average"],
    validFabrics: ["linen", "cotton", "jeans", "velvet"],
    validType: ["woman"],
  },
  {
    key: "roeDeer",
    name: "Półtusza sarny",
    level: ["average"],
    validFabrics: ["linen", "cotton", "jeans", "velvet"],
    validType: ["woman"],
  },
  {
    key: "wildBoar",
    name: "Półtusza dzika",
    level: ["average"],
    validFabrics: ["linen", "cotton", "jeans", "velvet"],
    validType: ["woman"],
  },
];

export { wheatData, productsData };
