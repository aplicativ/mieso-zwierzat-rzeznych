import img1 from './img/Półtusza_wieprzowa-golonka_przednia.png'
import img2 from './img/Półtusza_wieprzowa-łopatka.png'
import img3 from './img/Półtusza_wieprzowa-schab.png'
import img4 from './img/Półtusza_wieprzowa-golonka_przednia.png'
import img5 from './img/Połtusza_wolowa_rostbef.png'
import img6 from './img/Półtusza_wieprzowa-golonka_przednia.png'
import img7 from './img/Półtusza_cielęca-łopatka.png'
import img8 from './img/Półtusza_cielęca-górka.png'
import img9 from './img/Półtusza_cielęca-mostek.png'

const task05Data = [
  {
    first: 'Potrawa',
    second: 'Element gastronomiczny półtuszy wieprzowej',
    type: 'pork',
    items: [{ id: '1',  name: 'Golonka', type: 'pork', dish: 'Gulasz wieprzowy',src: img1, defaultValue: '1'},
    { id: '2',   name: 'Łopatka wieprzowa', type: 'pork',  dish: 'Filet', src: img2, defaultValue: '2'},
    { id: '3',  name: 'Schab bez kości', type: 'pork', dish: 'Golonka peklowana', src: img3, defaultValue: '3'},]
  },
  {
    first: 'Potrawa',
    second: 'Element gastronomiczny półtuszy wołowej',
    type: 'beef',
    items: [{ id: '4',   name: 'Zrazowa', type: 'beef', dish: 'Boeuf Strogonow',src: img4, defaultValue: '4' },
    { id: '5',  name: 'Rostbef', type: 'beef', dish: 'Bryzol',src: img5,defaultValue: '5'},
    { id: '6',  name: 'Skrzydło', type: 'beef', dish: 'Zrazy wołowe bite w sosie śmietanowym',src: img6,defaultValue: '6'},]
  },
  {
    first: 'Potrawa',
    second: 'Element gastronomiczny półtuszy cielęcej',
    type: 'veal',
    items: [{ id: '7',  name: 'Łopatka cielęca', type: 'veal', dish: 'Potrawka cielęca',src: img7,defaultValue: '7'},
  { id: '8',   name: 'Górka', type: 'veal', dish: 'Mostek panierowany po wiedeńsku',src: img8, defaultValue: '8'},
  { id: '9',   name: 'Mostek', type: 'veal', dish: 'Sznycel ministerski',src: img9, defaultValue: '9'},]
  },
];

const fifthTaskData = [
    {key: '1', chosen: null},
    {key: '2', chosen: null},
    {key: '3', chosen: null},
    {key: '4', chosen: null},
    {key: '5', chosen: null},
    {key: '6', chosen: null},
    {key: '7', chosen: null},
    {key: '8', chosen: null},
    {key: '9', chosen: null},
];

const correctAnswerTask05 = [
    {key: '1', correctAnswer: 'Łopatka'},
    {key: '2', correctAnswer: 'Schab bez kości'},
    {key: '3', correctAnswer: 'Golonka'},
    {key: '4', correctAnswer: 'Rostbef'},
    {key: '5', correctAnswer: 'Skrzydło'},
    {key: '6', correctAnswer: 'Zrazowa'},
    {key: '7', correctAnswer: 'Górka'},
    {key: '8', correctAnswer: 'Mostek'},
    {key: '9', correctAnswer: 'Łopatka cielęca'},
]

export {
    task05Data,
    fifthTaskData,
    correctAnswerTask05
}
