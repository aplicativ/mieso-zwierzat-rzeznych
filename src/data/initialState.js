const initialState = {
  level: "easy",
  easy: {
    product: null,
    type: null,
    fabric: null,
    typeOfMeat: "beefMeat",
    bannedFeatures: ["type"],
  },
  average: {
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: [],
  },

  hard: {
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: [],
  },

  task04: {
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: [],
  },

  task05: {
    product: null,
    type: null,
    fabric: null,
    bannedFeatures: [],
  },
};

export default initialState;
