const correctAnswers = {
  thirdTask1: [
    "Karkówka wołowa",
    "Mostek",
    "Szponder",
    "Rozbratel",
    "Antrykot",
    "Szyja wołowa",
  ],
  thirdTask2: ["Polędwica", "Rostbef"],
  thirdTask3: ["Szynka", "Schab", "Frykando"],
  thirdTask4: ["Szynka", "Schab", "Udziec", "Karkówka"],
  thirdTask5: ["Polędwica"],
};

export { correctAnswers };
