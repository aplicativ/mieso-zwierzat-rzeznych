import React, { useContext } from "react";
import clsx from "clsx";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import DataContex from "@context/DataContext";
import useStyles from "./styles";

import FullscreenIn from "@material-ui/icons/Fullscreen";
import FullscreenExit from "@material-ui/icons/FullscreenExit";
import Tooltip from "@material-ui/core/Tooltip";

const tabProps = (index) => {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
};

const Header = ({ isOpenNavigation, setIsOpenNavigation }) => {
  const classes = useStyles();
  const { setState, level, state, isFullscreen, setIsFullscreen } =
    useContext(DataContex);
  const handleLevelChange = (event, newValue) => {
    setState({ ...state, level: newValue });
  };
  const handleChangeFullscreen = () => {
    setIsFullscreen(!isFullscreen);
  };

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: isOpenNavigation,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <Tabs value={level} onChange={handleLevelChange} aria-label="">
          <Tab
            className={classes.menuItem}
            value="easy"
            label="Ćwiczenie 1"
            {...tabProps(0)}
          />{" "}
          <Tab
            className={classes.menuItem}
            value="average"
            label="Ćwiczenie 2"
            {...tabProps(1)}
          />
          <Tab
            className={classes.menuItem}
            value="hard"
            label="Ćwiczenie 3"
            {...tabProps(2)}
          />
          <Tab
            className={classes.menuItem}
            value="task04"
            label="Ćwiczenie 4"
            {...tabProps(3)}
          />
          <Tab
            className={classes.menuItem}
            value="task05"
            label="Ćwiczenie 5"
            {...tabProps(4)}
          />
          <Tab
            className={classes.menuItem}
            value="task06"
            label="Ćwiczenie 6"
            {...tabProps(5)}
          />
        </Tabs>
      </Toolbar>
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={() => setIsOpenNavigation(!isOpenNavigation)}
        edge="start"
        className={clsx(
          classes.menuButtonMenu,
          isOpenNavigation && classes.hide
        )}
      >
        <MenuIcon />
      </IconButton>
      <Tooltip
        placement="bottom-start"
        arrow={true}
        title={
          isFullscreen ? "Wyłącz tryb pełnoekranowy" : "Tryb pełnoekranowy"
        }
      >
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleChangeFullscreen}
          edge="start"
          className={clsx(classes.menuButtonFullscreen)}
        >
          {isFullscreen ? <FullscreenExit /> : <FullscreenIn />}
        </IconButton>
      </Tooltip>
    </AppBar>
  );
};

export default Header;
