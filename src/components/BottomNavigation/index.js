import React, { useContext, useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import Modal from '@components/ModalInstruction';
import saveScreen from '@lib/screen';

import GetAppIcon from '@material-ui/icons/GetApp';
import SaveIcon from '@material-ui/icons/Save';
import CameraAltIcon from '@material-ui/icons/CameraAlt';

import DataContext from '@context/DataContext';
import SyncIcon from '@mui/icons-material/Sync';
import InfoIcon from '@mui/icons-material/Info';

import ModalCard from '@components/ModalCard';
import ModalDocumentation from '@components/ModalDocumentation';
import PermDeviceInformationIcon from '@mui/icons-material/PermDeviceInformation';
import SchoolIcon from '@mui/icons-material/School';

import Tooltip from '@material-ui/core/Tooltip';

import { setLocalStorage } from '@lib/localStorage';
import {
  beefMeat,
  porkMeat,
  vealMeat,
  sheepMeat,
  deerMeat,
  roeDeerMeat,
  wildBoarMeat,
  cullinaryEl,
} from '@data/features';

import { fifthTaskData } from '@data/task05/task05';
import { Task04Data } from '@data/task04/data';

const useStyles = makeStyles({
  root: {
    width: '100%',
    height: '65px',
    '& button': {
      paddingTop: '30px 0',
    },
  },
});

export default function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [isOpenCard, setIsOpenCard] = useState(false);
  const [isOpenModalInst, setIsOpenModalInst] = useState(false);
  const {
    setFifthTask,
    appID,
    printSteps,
    level,
    data,
    setColumnsInTasks,
    setInputData,
    fifthTask,
    inputData,
    meatAndDishesColumns,
    cullinaryElColumns,
    beefMeatColumns,
    porkMeatColumns,
    vealMeatColumns,
    sheepMeatColumns,
    roeDeerMeatColumns,
    wildBoarMeatColumns,
    deerMeatColumns,
    task4Columns,
    setTask4Columns,
  } = useContext(DataContext);
  const [isDocumentationOpen, setIsDocumentationOpen] = useState(false);

  const resetState = () => {
    // setState(initialState);
    // updateCookieState(appID, null);
    if (level === 'easy') {
      if (data.typeOfMeat === 'beefMeat') {
        setColumnsInTasks(beefMeat);
      } else if (data.typeOfMeat === 'porkMeat') {
        setColumnsInTasks(porkMeat);
      } else if (data.typeOfMeat === 'vealMeat') {
        setColumnsInTasks(vealMeat);
      } else if (data.typeOfMeat === 'sheepMeat') {
        setColumnsInTasks(sheepMeat);
      } else if (data.typeOfMeat === 'deerMeat') {
        setColumnsInTasks(deerMeat);
      } else if (data.typeOfMeat === 'roeDeerMeat') {
        setColumnsInTasks(roeDeerMeat);
      } else if (data.typeOfMeat === 'wildBoarMeat') {
        setColumnsInTasks(wildBoarMeat);
      }
    } else if (level === 'average') {
      setColumnsInTasks(cullinaryEl);
    } else if (level === 'hard') {
      setColumnsInTasks({
        menu: {
          id: 'menu',
          list: [
            { key: '5', text: 'Antrykot' },
            { key: '10', text: 'Frykando' },
            { key: '15', text: 'Karkówka' },
            { key: '2', text: 'Mostek' },
            { key: '7', text: 'Polędwica' },
            { key: '16', text: 'Polędwica' },
            { key: '8', text: 'Rostbef' },
            { key: '4', text: 'Rozbratel' },
            { key: '13', text: 'Schab' },
            { key: '9', text: 'Schab' },
            { key: '3', text: 'Szponder' },
            { key: '6', text: 'Szyja wołowa' },
            { key: '14', text: 'Szynka' },
            { key: '11', text: 'Szynka' },
            { key: '12', text: 'Udziec' },
          ],
        },
        thirdTask1: {
          id: 'thirdTask1',
          list: [],
        },
        thirdTask2: {
          id: 'thirdTask2',
          list: [],
        },
        thirdTask3: {
          id: 'thirdTask3',
          list: [],
        },
        thirdTask4: {
          id: 'thirdTask4',
          list: [],
        },
        thirdTask5: {
          id: 'thirdTask5',
          list: [],
        },
      });
    } else if (level === 'task04') {
      setTask4Columns({
        menu: {
          id: 'menu',
          list: [
            { key: '0', text: 'Karkówka', category: 'Elements', type: 'Pork' },
            {
              key: '1',
              text: 'Golonka przednia i tylna',
              category: 'Elements',
              type: 'Pork',
            },
            { key: '2', text: 'Łopatka', category: 'Elements', type: 'Pork' },
            { key: '3', text: 'Szynka', category: 'Elements', type: 'Pork' },
            { key: '4', text: 'Żeberka', category: 'Elements', type: 'Pork' },
            { key: '5', text: 'Schab', category: 'Elements', type: 'Pork' },
            {
              key: '6',
              text: 'Zrazowa wewnętrzna',
              category: 'Elements',
              type: 'Beef',
            },
            { key: '7', text: 'Rostbef', category: 'Elements', type: 'Beef' },
            {
              key: '8',
              text: 'Pręga przednia',
              category: 'Elements',
              type: 'Beef',
            },
            { key: '9', text: 'Polędwica', category: 'Elements', type: 'Beef' },
            { key: '10', text: 'Skrzydło', category: 'Elements', type: 'Beef' },
            { key: '11', text: 'Górka', category: 'Elements', type: 'Veal' },
            { key: '12', text: 'Mostek', category: 'Elements', type: 'Veal' },
            {
              key: '13',
              text: 'Łopatka cielęca',
              category: 'Elements',
              type: 'Veal',
            },
            {
              key: '14',
              text: 'Golonka peklowana',
              category: 'Dish',
              type: 'Pork',
            },
            {
              key: '15',
              text: 'Gulasz wieprzowy',
              category: 'Dish',
              type: 'Pork',
            },
            {
              key: '16',
              text: 'Klopsiki w sosie pomidorowym',
              category: 'Dish',
              type: 'Pork',
            },
            { key: '17', text: 'Szaszłyki', category: 'Dish', type: 'Pork' },
            {
              key: '18',
              text: 'Żeberka wieprzowe gotowane',
              category: 'Dish',
              type: 'Pork',
            },
            {
              key: '19',
              text: 'Filet wieprzowy',
              category: 'Dish',
              type: 'Pork',
            },
            {
              key: '20',
              text: 'Zrazy wołowe bite w sosie śmietanowym',
              category: 'Dish',
              type: 'Beef',
            },
            {
              key: '21',
              text: 'Boeuf Strogonow',
              category: 'Dish',
              type: 'Beef',
            },
            {
              key: '22',
              text: 'Bulion mięsny',
              category: 'Dish',
              type: 'Beef',
            },
            {
              key: '23',
              text: 'Bryzol wołowy',
              category: 'Dish',
              type: 'Beef',
            },
            {
              key: '24',
              text: 'Befsztyk po angielsku',
              category: 'Dish',
              type: 'Beef',
            },
            {
              key: '25',
              text: 'Sznycel ministerski',
              category: 'Dish',
              type: 'Veal',
            },
            {
              key: '26',
              text: 'Potrawka cielęca',
              category: 'Dish',
              type: 'Veal',
            },
            {
              key: '27',
              text: 'Mostek cielęcy panierowany po wiedeńsku',
              category: 'Dish',
              type: 'Veal',
            },
            { key: '28', text: 'Z masy mielonej', category: 'Methods' },
            { key: '29', text: 'Gotowanie', category: 'Methods' },
            { key: '30', text: 'Duszenie', category: 'Methods' },
            { key: '31', text: 'Smażenie', category: 'Methods' },
            { key: '32', text: 'Gotowanie', category: 'Methods' },
            { key: '33', text: 'Smażenie', category: 'Methods' },
            { key: '34', text: 'Gotowanie', category: 'Methods' },
            { key: '35', text: 'Smażenie', category: 'Methods' },
            { key: '36', text: 'Duszenie', category: 'Methods' },
            { key: '37', text: 'Duszenie', category: 'Methods' },
            { key: '38', text: 'Smażenie po angielsku', category: 'Methods' },
            { key: '39', text: 'Smażenie', category: 'Methods' },
            { key: '40', text: 'Gotowanie', category: 'Methods' },
            { key: '41', text: 'Z masy mielonej', category: 'Methods' },
          ],
        },

        //Element

        contentPorkElements0: {
          id: 'contentPorkElements0',
          list: [],
        },
        contentPorkElements1: {
          id: 'contentPorkElements1',
          list: [],
        },
        contentPorkElements2: {
          id: 'contentPorkElements2',
          list: [],
        },
        contentPorkElements3: {
          id: 'contentPorkElements3',
          list: [],
        },
        contentPorkElements4: {
          id: 'contentPorkElements4',
          list: [],
        },
        contentPorkElements5: {
          id: 'contentPorkElements5',
          list: [],
        },

        contentBeefElements0: {
          id: 'contentBeefElements0',
          list: [],
        },
        contentBeefElements1: {
          id: 'contentBeefElements1',
          list: [],
        },
        contentBeefElements2: {
          id: 'contentBeefElements2',
          list: [],
        },
        contentBeefElements3: {
          id: 'contentBeefElements3',
          list: [],
        },
        contentBeefElements4: {
          id: 'contentBeefElements4',
          list: [],
        },

        contentVealElements0: {
          id: 'contentVealElements0',
          list: [],
        },
        contentVealElements1: {
          id: 'contentVealElements1',
          list: [],
        },
        contentVealElements2: {
          id: 'contentVealElements2',
          list: [],
        },

        //Dish

        contentPorkDish0: {
          id: 'contentPorkDish0',
          list: [],
        },
        contentPorkDish1: {
          id: 'contentPorkDish1',
          list: [],
        },
        contentPorkDish2: {
          id: 'contentPorkDish2',
          list: [],
        },
        contentPorkDish3: {
          id: 'contentPorkDish3',
          list: [],
        },
        contentPorkDish4: {
          id: 'contentPorkDish4',
          list: [],
        },
        contentPorkDish5: {
          id: 'contentPorkDish5',
          list: [],
        },

        contentBeefDish0: {
          id: 'contentBeefDish0',
          list: [],
        },
        contentBeefDish1: {
          id: 'contentBeefDish1',
          list: [],
        },
        contentBeefDish2: {
          id: 'contentBeefDish2',
          list: [],
        },
        contentBeefDish3: {
          id: 'contentBeefDish3',
          list: [],
        },
        contentBeefDish4: {
          id: 'contentBeefDish4',
          list: [],
        },

        contentVealDish0: {
          id: 'contentVealDish0',
          list: [],
        },
        contentVealDish1: {
          id: 'contentVealDish1',
          list: [],
        },
        contentVealDish2: {
          id: 'contentVealDish2',
          list: [],
        },

        //Methods

        contentPorkMethods0: {
          id: 'contentPorkMethods0',
          list: [],
        },
        contentPorkMethods1: {
          id: 'contentPorkMethods1',
          list: [],
        },
        contentPorkMethods2: {
          id: 'contentPorkMethods2',
          list: [],
        },
        contentPorkMethods3: {
          id: 'contentPorkMethods3',
          list: [],
        },
        contentPorkMethods4: {
          id: 'contentPorkMethods4',
          list: [],
        },
        contentPorkMethods5: {
          id: 'contentPorkMethods5',
          list: [],
        },

        contentBeefMethods0: {
          id: 'contentBeefMethods0',
          list: [],
        },
        contentBeefMethods1: {
          id: 'contentBeefMethods1',
          list: [],
        },
        contentBeefMethods2: {
          id: 'contentBeefMethods2',
          list: [],
        },
        contentBeefMethods3: {
          id: 'contentBeefMethods3',
          list: [],
        },
        contentBeefMethods4: {
          id: 'contentBeefMethods4',
          list: [],
        },

        contentVealMethods0: {
          id: 'contentVealMethods0',
          list: [],
        },
        contentVealMethods1: {
          id: 'contentVealMethods1',
          list: [],
        },
        contentVealMethods2: {
          id: 'contentVealMethods2',
          list: [],
        },
      });
    } else if (level === 'task05') {
      setFifthTask(fifthTaskData);
    } else if (level === 'task06') {
      setInputData([]);
    }
    setLocalStorage(appID + 'easy1', null);
    setLocalStorage(appID + 'easy2', null);
    setLocalStorage(appID + 'easy3', null);
    setLocalStorage(appID + 'easy4', null);
    setLocalStorage(appID + 'easy5', null);
    setLocalStorage(appID + 'easy6', null);
    setLocalStorage(appID + 'easy7', null);
    setLocalStorage(appID + 'average', null);
    setLocalStorage(appID + 'hard', null);

    setLocalStorage(appID + 'task04', null);
    setLocalStorage(appID + 'task05', null);
    setLocalStorage(appID + 'task06', null);
  };

  const saveToLocalStorage = () => {
    setLocalStorage(appID + 'easy1', beefMeatColumns);
    setLocalStorage(appID + 'easy2', porkMeatColumns);
    setLocalStorage(appID + 'easy3', vealMeatColumns);
    setLocalStorage(appID + 'easy4', sheepMeatColumns);
    setLocalStorage(appID + 'easy5', deerMeatColumns);
    setLocalStorage(appID + 'easy6', roeDeerMeatColumns);
    setLocalStorage(appID + 'easy7', wildBoarMeatColumns);

    setLocalStorage(appID + 'average', cullinaryElColumns);
    setLocalStorage(appID + 'hard', meatAndDishesColumns);
    setLocalStorage(appID + 'task04', task4Columns);
    setLocalStorage(appID + 'task05', fifthTask);
    setLocalStorage(appID + 'task06', inputData);
  };

  <ul>
    <li>
      {' '}
      <a
        className="link"
        aria-label="Przejdź do galerii"
        target="_blank"
        href="https://edytor.zpe.gov.pl/a/Dn0fVks4h"
        rel="noreferrer"
      >
        Przejdź do Animacji 3D
      </a>
    </li>
    <li>
      {' '}
      <a
        className="link"
        aria-label="Przejdź do galerii"
        target="_blank"
        href="https://edytor.zpe.gov.pl/x/D1Wx6hydi?lang=pl&wcag=atlas"
        rel="noreferrer"
      >
        Przejdź do Atlasu Interaktywnego
      </a>
    </li>
    <li>
      {' '}
      <a
        className="link"
        aria-label="Przejdź do galerii"
        target="_blank"
        href="https://edytor.zpe.gov.pl/x/D1Fxw4ZsH?lang=pl&wcag=film"
        rel="noreferrer"
      >
        Przejdź do Filmu Edukacyjnego
      </a>
    </li>
  </ul>;

  return (
    <>
      <Modal isOpen={isOpenModalInst} setIsOpen={setIsOpenModalInst} />
      <ModalCard isOpen={isOpenCard} setIsOpen={setIsOpenCard} />
      <ModalDocumentation
        isOpen={isDocumentationOpen}
        setIsOpen={setIsDocumentationOpen}
      />
      <BottomNavigation
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        showLabels
        className={classes.root}
      >
        <Tooltip
          title={
            <span style={{fontSize: '12px'}}>
              Kliknij, aby zobaczyć instrukcję obsługi programu
            </span>
          }
          arrow={true}
          placement={'top'}
        >
          <BottomNavigationAction
            label="Instrukcja"
            icon={<InfoIcon />}
            onClick={() => setIsOpenModalInst(true)}
          />
        </Tooltip>

        <Tooltip
          title={
            <span style={{fontSize: '12px'}}>
              Kliknij, aby zapoznaćsię z dodatkowymi materiałami oraz
              multimediami
            </span>
          }
          arrow={true}
          placement={'top'}
        >
          <BottomNavigationAction
            label="Baza wiedzy"
            icon={<SchoolIcon />}
            onClick={() => setIsDocumentationOpen(true)}
          />
        </Tooltip>

        <Tooltip
          title={
            <span style={{fontSize: '12px'}}>
              Kliknij, aby zapoznać się z dodatkowymi materiałami oraz
              multimediami
            </span>
          }
          arrow={true}
          placement={'top'}
        >
          <BottomNavigationAction
            label="Multimedia"
            icon={<PermDeviceInformationIcon />}
            onClick={() => setIsOpenCard(true)}
          />
        </Tooltip>

        <Tooltip
          title={
            <span style={{fontSize: '12px'}}>
              Kliknij, aby pobrać plik PDF z Twoimi wyborami
            </span>
          }
          arrow={true}
          placement={'top'}
        >
          <BottomNavigationAction
            onClick={() => printSteps()}
            label="Pobierz listę kroków"
            icon={<GetAppIcon />}
          />
        </Tooltip>

        <Tooltip
          title={
            <span style={{fontSize: '12px'}}>
              Kliknij, aby zapisać postęp pracy. Po zamknięciu karty
              przeglądarki i ponownym jej otwarciu, pozwoli to na powrót do tego
              samego miejsca (powrót do ostatnio dokonanych wyborów)
            </span>
          }
          arrow={true}
          placement={'top'}
        >
          <BottomNavigationAction
            onClick={saveToLocalStorage}
            label="Zapisz efekty pracy"
            icon={<SaveIcon />}
          />
        </Tooltip>

        <Tooltip
          title={
            <span style={{fontSize: '12px'}}>
              Kliknij, aby zrobić zrzut ekranu
            </span>
          }
          arrow={true}
          placement={'top'}
        >
          <BottomNavigationAction
            onClick={() => saveScreen('app')}
            label="Zrzut ekranu"
            icon={<CameraAltIcon />}
          />
        </Tooltip>

        <Tooltip
          title={
            <span style={{fontSize: '12px'}}>
              Kliknij, aby usunąć wszystkie wybory i ponownie rozwiązać
              ćwiczenie
            </span>
          }
          arrow={true}
          placement={'top'}
        >
          <BottomNavigationAction
            onClick={() => resetState()}
            label="Spróbuj ponownie"
            icon={<SyncIcon />}
          />
        </Tooltip>

        {/* <BottomNavigationAction label="Odsłuchaj" icon={<VolumeUpIcon />} /> */}
      </BottomNavigation>
    </>
  );
}
