import React, { useContext } from "react";
import { useSnackbar } from "notistack";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import DataContex from "@context/DataContext";
import ModalContext from "@context/ModalContext";

import useStyles from "./styles";

const RadioGroupPrimary = ({
  title,
  items,
  keyData,
  isNested = false,
  expended,
  defaultValue,
}) => {
  const classes = useStyles();
  const { data, setState, state, level } = useContext(DataContex);
  const { setModalFunctionalParams } = useContext(ModalContext);
  let { [keyData]: selectedDefault } = data;
  const selected = selectedDefault;
  const sortedItems = !isNested
    ? items.filter(
        (item) => item.level.includes("all") || item.level.includes(level)
      )
    : [];

  const initModalFunctional = ({ type, text }) => {
    setModalFunctionalParams({ type, text, isOpen: true });
  };

  const updateData = (value) => {
    const newValue =
      value.target.value === selected ? null : value.target.value;
    setState({ ...state, [level]: { ...state[level], [keyData]: newValue } });
  };

  return (
    <div>
      <Accordion defaultExpanded={expended ? true : false}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>{title}</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.bannedWrap}>
          {isNested ? (
            <RadioGroup aria-label="transport" value={selected}>
              {items.map((section) => {
                const { items, title } = section;
                const sortedData = items.filter(
                  (item) =>
                    item.level.includes("all") || item.level.includes(level)
                );
                return (
                  <>
                    <Typography className={classes.subitemTitle}>
                      {title}
                    </Typography>
                    {sortedData.map((item, index) => (
                      <FormControlLabel
                        value={item.key}
                        control={<Radio color="secondary" />}
                        label={item.name}
                        key={index}
                        className={classes.formControlLabel}
                        control={<Radio onClick={updateData} />}
                      />
                    ))}
                  </>
                );
              })}
            </RadioGroup>
          ) : (
            <RadioGroup aria-label="transport" value={selected ? selected : ""}>
              {sortedItems.map((item, index) => (
                <FormControlLabel
                  value={item.key}
                  control={<Radio color="secondary" />}
                  label={item.name}
                  key={index}
                  className={classes.formControlLabel}
                  control={<Radio onClick={updateData} />}
                />
              ))}
            </RadioGroup>
          )}
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default RadioGroupPrimary;
