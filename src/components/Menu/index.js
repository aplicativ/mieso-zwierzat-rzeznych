import React, { useContext } from "react";

import { makeStyles } from "@material-ui/core/styles";

import { methods } from "@data/features";
import { options, subOptions } from "@data/task04/data";
import RadioGroupPrimary from "./subcomponents/RadioGroupPrimary";
import Box from "@mui/material/Box";
import DataContext from "@context/DataContext";
import DragDropItem from "@components/DragDropTransfer/subcomponents/item";
import DragDropItemTask04 from "@components/DragDropTransfer/subcomponents/item-task04-menu";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "auto",
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },

  formControlLabel: {
    "& span": {
      fontSize: "0.8em",
    },
  },
  title: {
    fontSize: theme.typography.pxToRem(15),
    padding: "20px 20px",
    textAlign: "left",
  },
}));

const MenuForFirstTask = ({ choosenMethod }) => {
  return (
    <div>
      <RadioGroupPrimary
        expended
        items={methods}
        keyData="typeOfMeat"
        title="Półtusza:"
        key="typeOfMeat"
      />
    </div>
  );
};

const Menu = () => {
  const classes = useStyles();
  const { data, level, state } = useContext(DataContext);

  if (data === undefined) {
    return null;
  }
  const { typeOfMeat: method } = data;
  // console.log("co", methods);
  return (
    <>
      {level === "easy" && (
        <>
          <MenuForFirstTask className={classes.root} choosenMethod={method} />
          <DragDropItem keyData="menu" />
        </>
      )}
      {level === "average" && (
        <>
          <h3 style={{ textAlign: "left", fontSize: 17, padding: "0 5px" }}>
            Elementy gastronomiczne i ich przeznaczenie kulinarne:
          </h3>
          <DragDropItem keyData="menu" />
        </>
      )}
      {level === "hard" && (
        <>
          <h3 style={{ textAlign: "left", fontSize: 17, padding: "0 6px" }}>
            Asortyment mięsa
          </h3>
          <DragDropItem keyData="menu" />
        </>
      )}

      {level === "task04" && (
        <>
          <DragDropItemTask04
            keyData="menu"
            type={data.categoryOptions + data.typeOfMeat}
          />
        </>
      )}
    </>
  );
};

export default Menu;
