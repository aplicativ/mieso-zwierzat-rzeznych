import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";

import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import AudioPlayer from "../AudioPlayer";

import instrukcja from "./../../sounds/instrukcja.mp3";

const Modal = ({ isOpen, setIsOpen }) => {
  const useStyles = makeStyles((theme) => ({
    inner: {
      padding: "30px 30px",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: "33.33%",
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    description: {
      padding: "15px",
    },
    close: {
      position: "absolute",
      top: "10px",
      right: "10px",
    },
    paragraph: {
        marginTop: 30
    }
  }));
  const classes = useStyles();

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Dialog
      style={{ zIndex: "999999" }}
      maxWidth="xs"
      fullWidth
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={isOpen}
    >
      <div className={classes.inner}>
        <div className={classes.close}>
          <IconButton onClick={handleClose} aria-label="close" component="span">
            <CloseIcon
              style={{
                stroke: "#757575",
                strokeWidth: 2,
              }}
            />
          </IconButton>
        </div>
        <Typography variant="h2" component="h2">
          Multimedia
        </Typography>
        <p className={classes.paragraph}>
          <ul style={{fontSize: 15,textAlign:"left"}}>
            <li>
              <a
                className="link"
                aria-label="Przejdź do Grafiki interaktywnej"
                target="_blank"
                href="https://edytor.zpe.gov.pl/a/Dn0fVks4h"
              >
              Przejdź do Animacji 3D
              </a>
            </li>
            <li>
              <a
                className="link"
                aria-label="Przejdź do Sekwencji filmowej"
                target="_blank"
                href="https://edytor.zpe.gov.pl/x/D1Wx6hydi?lang=pl&wcag=atlas"
              >
              Przejdź do Atlasu Interaktywnego
              </a>
            </li>
            <li>
              <a
                className="link"
                aria-label="Przejdź do Wycieczki Wirtualnej"
                target="_blank"
                href="https://edytor.zpe.gov.pl/x/DhwncpJ47?lang=pl&wcag="
              >
              Przejdź do Filmu Edukacyjnego
              </a>
            </li>
          </ul>
        </p>
      </div>
    </Dialog>
  );
};

export default Modal;
