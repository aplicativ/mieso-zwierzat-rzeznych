import React, { useContext } from 'react';
import { Button } from '@material-ui/core';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import ModalContext from '@context/ModalContext';
import DataContext from '@context/DataContext';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import AudioPlayer from '../AudioPlayer';

import polecenie1 from './../../sounds/polecenie1.mp3';
import polecenie2 from './../../sounds/polecenie2.mp3';
import polecenie3 from './../../sounds/polecenie3.mp3';
import polecenie4 from './../../sounds/polecenie4.mp3';
import polecenie5 from './../../sounds/polecenie5.mp3';
import polecenie6 from './../../sounds/polecenie6.mp3';

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: 'rgba(65,172,38,0.3)',
  },
  backDrop: {},
  icon: {
    textAlign: 'center',
    marginBottom: '10px',
    width: '100%',
  },
  iconImg: {
    margin: '0 auto',
  },
  title: {
    textAlign: 'center',
    position: 'relative',
    maxWidth: '100%',
    marginBottom: '20px',
    color: '#595959',
    fontSize: '25px',
    fontWeight: '600',
    textTransform: 'none',
    wordWrap: 'break-word',
  },
  description: {
    color: '#595959',
    fontWeight: '600',
    textAlign: 'center',
    fontSize: '16px',
    marginBottom: '20px',
  },
  buttonsWrap: {
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    margin: '10px',
  },
}));

const Instruction = () => {
  const { isInstructionOpen, setIsInstructionOpen } = useContext(ModalContext);
  const classes = useStyles();
  const { level } = useContext(DataContext);

  const handleClose = () => {
    setIsInstructionOpen(false);
  };
  return (
    <>
      <Dialog
        fullWidth
        open={isInstructionOpen}
        onClose={handleClose}
        style={{ zIndex: '999999' }}
      >
        <DialogContent style={{ padding: '25px 40px' }}>
          <h4
            style={{
              marginBottom: '20px',
              fontSize: '25px',
              textAlign: 'center',
            }}
          >
            Polecenie
          </h4>
          {level === 'easy' && (
            <>
              <p
                style={{
                  marginBottom: '20px',
                  fontSize: 16,
                  textAlign: 'left',
                }}
              >
                Z menu bocznego wybierz jedną z półtusz (np. wołową czy
                wieprzową), a następnie przeciągnij nazwy poszczególnych jej
                elementów (np. łopatka czy mostek) i umieść w prawidłowym
                miejscu na widocznym schemacie. Pamiętaj, że w dolnym menu
                znajdziesz również ikonę, po kliknięciu na którą będziesz mógł
                wrócić do wiedzy dostępnej w innych multimediach e-materiału.
              </p>
              <AudioPlayer
                url={polecenie1}
                label="Polecenie do programu ćwiczeniowego"
              />
            </>
          )}
          {level === 'average' && (
            <>
              <p
                style={{
                  marginBottom: '20px',
                  fontSize: 16,
                  textAlign: 'left',
                }}
              >
                Z menu bocznego wybierz po kolei części kulinarne półtuszy
                wieprzowej (np. boczek czy biodrówkę), a następnie dopasuj do
                nich zastosowanie gastronomiczne (np. potrawy pieczone czy
                gotowane). Oba elementy przeciągnij pod zdjęcia widoczne na
                ekranie. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po
                kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w
                innych multimediach e-materiału.
              </p>
              <AudioPlayer
                url={polecenie2}
                label="Polecenie do programu ćwiczeniowego"
              />
            </>
          )}
          {level === 'hard' && (
            <>
              <p
                style={{
                  marginBottom: '20px',
                  fontSize: 16,
                  textAlign: 'left',
                }}
              >
                Z menu bocznego wybierz odpowiedni asortyment mięsa i spróbuj
                dopasować go do konkretnej potrawy. Następnie przeciągnij
                poszczególne elementy (np. antrykot czy karkówkę) i umieść w
                prawidłowej rubryce tabeli widocznej na ekranie. Pamiętaj, że w
                dolnym menu znajdziesz również ikonę, po kliknięciu na którą
                będziesz mógł wrócić do wiedzy dostępnej w innych multimediach
                e-materiału.
              </p>
              <AudioPlayer
                url={polecenie3}
                label="Polecenie do programu ćwiczeniowego"
              />
            </>
          )}
          {level === 'task04' && (
            <>
              <p
                style={{
                  marginBottom: '20px',
                  fontSize: 16,
                  textAlign: 'left',
                }}
              >
                Z menu bocznego wybierz elementy gastronomiczne półtuszy
                wieprzowej, wołowej lub cielęcej (np. boczek czy górkę), a
                następnie zaplanuj potrawę z zastosowaniem danego elementu,
                uwzględniając również właściwą metodę obróbki cieplnej (np.
                gotowanie czy smażenie). Następnie poszczególne elementy umieść
                w prawidłowej rubryce tabeli widocznej na ekranie. Pamiętaj, że
                w dolnym menu znajdziesz również ikonę, po kliknięciu na którą
                będziesz mógł wrócić do wiedzy dostępnej w innych multimediach
                e-materiału oraz skorzystać z Bazy wiedzy, w której znajdziesz
                charakterystykę elementów gastronomicznych mięsa zwierząt
                rzeźnych, metody obróbki cieplnej oraz bazę potraw.
              </p>
              <AudioPlayer
                url={polecenie4}
                label="Polecenie do programu ćwiczeniowego"
              />
            </>
          )}
          {level === 'task05' && (
            <>
              <p
                style={{
                  marginBottom: '20px',
                  fontSize: 16,
                  textAlign: 'left',
                }}
              >
                Wykonaj korektę błędnych kompozycji przedstawionych w tabeli,
                przesuwając poszczególne elementy gastronomiczne tak, aby
                odpowiadały właściwej potrawie. Pamiętaj, że w dolnym menu
                znajdziesz również ikonę, po kliknięciu na którą będziesz mógł
                wrócić do wiedzy dostępnej w innych multimediach e-materiału
                oraz skorzystać z Bazy wiedzy, w której znajdziesz
                charakterystykę elementów gastronomicznych mięsa zwierząt
                rzeźnych, metody obróbki cieplnej oraz bazę potraw.
              </p>
              <AudioPlayer
                url={polecenie5}
                label="Polecenie do programu ćwiczeniowego"
              />
            </>
          )}
          {level === 'task06' && (
            <>
              <p
                style={{
                  marginBottom: '20px',
                  fontSize: 16,
                  textAlign: 'left',
                }}
              >
                Oceń prawdziwość podanych zdań. Zaznacz Prawda, jeśli zdanie
                jest prawdziwe, lub Fałsz – jeśli jest fałszywe. Pamiętaj, że w
                dolnym menu znajdziesz również ikonę, po kliknięciu na którą
                będziesz mógł wrócić do wiedzy dostępnej w innych multimediach
                e-materiału oraz skorzystać z Bazy wiedzy, w której znajdziesz
                charakterystykę elementów gastronomicznych mięsa zwierząt
                rzeźnych, metody obróbki cieplnej, bazę potraw oraz urządzeń do
                obróbki cieplnej.
              </p>
              <AudioPlayer
                url={polecenie6}
                label="Polecenie do programu ćwiczeniowego"
              />
            </>
          )}
          <div style={{ position: 'absolute', top: '15px', right: '15px' }}>
            <IconButton
              onClick={() => setIsInstructionOpen(false)}
              aria-label="Example"
            >
              <CloseIcon />
            </IconButton>
          </div>

          <div className={classes.instruction}>
            {/* <p>Zaprojektuj wnętrze pokoju biurowego, pokoju dziennego i sypialni przez dobór odpowiednich inteligentnych włókienniczych wyrobów dekoracyjnych. W rozwijanym menu po lewej stronie znajduje się lista mebli i wyrobów dekoracyjnych.</p>
                        <p>W celu zaprojektowania myślącego pokoju wskaż na liście elementy dekoracyjne, które spełniają cechy inteligentnego lub tekstronicznego wyrobu włókienniczego i odpowiadają funkcji danego pomieszczenia, a następnie wybierz przycisk „sprawdź”, aby zweryfikować poprawność swoich odpowiedzi.</p>
                        <p>Jeśli poprawnie dobierzesz tylko część elementów, spróbuj rozwiązać zadanie ponownie, tak aby uzyskać kompletny efekt myślącego pokoju.
                        Pamiętaj, że każdy pokój to kolejny poziom trudności. Na każdym poziomie do dopasowania jest inna liczba elementów inteligentnych.</p> */}
            {/* <ul style={{ marginTop: "20px" }}>
                            <li>Standard sieci 1000Base-T</li>
                            <li>W sieci pracują 2 routery połączone ze sobą poprzez interfejsy WAN</li>
                            <li>Interfejsy WAN routerów zaadresowane są adresami IP należącymi do sieci o adresie 192.168.20.20/30</li>
                            <li>Do interfejsu LAN routera R1 podłączony jest komputer K1. Adres sieci pomiędzy tymi urządzeniami to: 10.0.0.32/29</li>
                            <li>Do interfejsu LAN routera R2 podłączony jest komputer K2. Adres sieci pomiędzy tymi urządzeniami to: 172.19.32.16/29</li>
                            <li>Dobierz odpowiednie urządzenia oraz medium transmisyjne</li>
                            <li>Zaadresuj poszczególne interfejsy routerów oraz komputerów w taki sposób, aby była możliwa komunikacja pomiędzy komputerem K1 i K2</li>
                        </ul> */}
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default Instruction;
