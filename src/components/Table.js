import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataContex from "../context/DataContext";
import biodrowka from "../data/products/img/img/biodrówka.png";
import boczek from "../data/products/img/img/boczek.png";
import golonkaprzednia from "../data/products/img/img/golonkaprzednia.png";
import szynka from "../data/products/img/img/szynka.png";
import łopatka from "../data/products/img/img/łopatka.png";
import DragDropItem from "@components/DragDropTransfer/subcomponents/item-average-hard";

const useStyles = makeStyles({
  photo: {
    maxWidth: "300px",
    display: "block",
  },
  div: {
    color: "white",
    height: "30px",
    display: "flex",
    fontSize: "29px",
    border: "1px solid black",
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  name: {
    border: "1px solid black",
    display: "flex",
  },
  itemNameOrApplication: {
    border: "1px solid black",
  },
});

export default function Table() {
  const classes = useStyles();
  const { getImagePath } = useContext(DataContex);

  return (
    <>
      <div style={{color:"black"}}>
        <img className={classes.photo} src={getImagePath(boczek)} />
        <div>
          Element:
          <div className={classes.itemNameOrApplication}>
            <DragDropItem keyData="content18" />
          </div>
          Zastosowanie:
          <div className={classes.itemNameOrApplication}>
            <DragDropItem keyData="content19" />
          </div>
        </div>
        <img className={classes.photo} src={getImagePath(łopatka)} />
        Element:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content20" />
        </div>
        Zastosowanie:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content21" />
        </div>
        <img className={classes.photo} src={getImagePath(golonkaprzednia)} />
        Element:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content22" />
        </div>
        Zastosowanie:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content23" />
        </div>
        <img className={classes.photo} src={getImagePath(biodrowka)} />
        Element:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content24" />
        </div>
        Zastosowanie:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content25" />
        </div>
        <img className={classes.photo} src={getImagePath(szynka)} /> Element:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content26" />
        </div>
        Zastosowanie:
        <div className={classes.itemNameOrApplication}>
          <DragDropItem keyData="content27" />
        </div>
      </div>
    </>
  );
}
