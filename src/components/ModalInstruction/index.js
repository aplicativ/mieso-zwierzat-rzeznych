import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";

import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import AudioPlayer from "../AudioPlayer";

import instrukcja from "./../../sounds/instrukcja.mp3";

const Modal = ({ isOpen, setIsOpen }) => {
  const useStyles = makeStyles((theme) => ({
    inner: {
      padding: "30px 30px",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: "33.33%",
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    description: {
      padding: "15px",
    },
    close: {
      position: "absolute",
      top: "10px",
      right: "10px",
    },
    paragraph: {fontSize: 16, textAlign: "left" }
  }));
  const classes = useStyles();

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Dialog
      style={{ zIndex: "999999" }}
      maxWidth="md"
      fullWidth
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={isOpen}
    >
      <div className={classes.inner}>
        <div className={classes.close}>
          <IconButton onClick={handleClose} aria-label="close" component="span">
            <CloseIcon
              style={{
                stroke: "#757575",
                strokeWidth: 2,
              }}
            />
          </IconButton>
        </div>
        <Typography variant="h2" component="h2">
          Instrukcja obsługi do programu ćwiczeniowego „Zastosowanie elementów gastronomicznych mięsa do produkcji potraw”
        </Typography>
        <br />
        <p className={classes.paragraph}>
          Przed przystąpieniem do wykonania ćwiczenia zapoznaj się z poleceniem.
        </p>
        <p className={classes.paragraph}>
          Dla wygody korzystania z programu ćwiczeniowego kliknij ikonę trybu pełnoekranowego. Umożliwia on przeglądarce zajęcie całego ekranu.
        </p>
        <p className={classes.paragraph}>
          W celu odsłuchania treści zawartych w programie ćwiczeniowym wybierz ikonę „Odsłuchaj”. W dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł zapoznać się z innymi materiałami multimedialnymi z e-zasobu.
        </p>
        <p className={classes.paragraph}>
          Jeśli chcesz zapisać postępy swojej pracy użyj ikony „Pobierz listę kroków”, a plik zapisz na dysku komputera.
        </p>
        <p className={classes.paragraph}>
          Program ćwiczeniowy umożliwia również zapisanie całej swojej pracy, wykonanie zrzutu ekranu oraz ponownego wykonania ćwiczenia.
        </p>
        <p className={classes.paragraph}>Po wykonaniu zadania, niezależnie od Twojego wyniku, otrzymasz informację zwrotną.</p>
        <p className={classes.paragraph}>W przypadku korzystania wyłącznie z klawiatury należy użyć poniższych klawiszy:</p>
        <ul>
          <li className={classes.paragraph}>Tab - poruszanie się do przodu po elementach</li>
          <li className={classes.paragraph}>Shift + Tab - poruszanie się do tyłu po elementach</li>
          <li className={classes.paragraph}>Spacja - podnoszenie i upuszczanie elementów</li>
          <li className={classes.paragraph}>Escape - anulowanie przeciągania</li>
          <li className={classes.paragraph}>Strzałki - przenoszenie elementów do sąsiadujących stref upuszczania</li>
        </ul>
        <AudioPlayer url={instrukcja} label="Instrukcja obsługi do programu ćwiczeniowego" />
      </div>
    </Dialog>
  );
};

export default Modal;
