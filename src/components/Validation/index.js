import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataContex from '../../context/DataContext';
import { Button } from '@material-ui/core';
import InputContext from '@context/InputContext';
import ModalContext from '../../context/ModalContext';
import _ from 'lodash';
import { correctAnswers } from '@data/correctAnswersToThirdTask';
import { correctAnswerTask05 } from '@data/task05/task05';
import { dataTask6 } from '../../data/features';
import { correctAnswerTask04 } from '@data/task04/data';

import Tooltip from '@material-ui/core/Tooltip';

//informacja zwrotna pozytywna - uzupełniamy tekstem od redaktora
let successInfo =
  'Poprawnie dobrałeś przyczyny powstawania wad w cieście drożdźowym!';
//informacja zwrotna negatywna - uzupełniamy tekstem od redaktora

const useStyles = makeStyles(() => ({
  wrap: {
    marginTop: '10px',
  },
}));

const Validation = () => {
  const classes = useStyles();
  const {
    data,
    level,
    columnsInTasks,
    setIsSecondTask,
    setIsThirdTask,
    setIsBeefMeat,
    setIsPorkMeat,
    setIsVealMeat,
    setIsSheepMeat,
    setIsDeerMeat,
    setIsRoeDeerMeat,
    setIsWildBoarMeat,
    setIsFifthTask,
    fifthTask,
    inputData,
    task4Columns,
    setIsFourTask,
    setIsSixTask,
  } = useContext(DataContex);

  // const { product, type, fabric } = data
  let { setModalParams } = useContext(ModalContext);
  const initModal = ({ type, title, text }) => {
    setModalParams({ type, title, text, isOpen: true });
  };
  // if (data === undefined) {
  //   return null;
  // }

  // if ("list" in columnsInTasks.thirdTask1) {
  //   console.log("exit list");
  //
  //
  // } else {
  //   console.log("list not exist");
  // }
  // let taskNotCompleted = Object.values(columnsInTasks).filter(
  //   (value) => !value.list.length && value.id !== "menu"
  // );

  // const validMeatForChickenSoup = ["Rozbratel", "Antrykot"];
  // const selectedMeatForChickenSoup = columnsInTasks.thirdTask1.list.map(
  //   (item) => item.text
  // );
  // let doArraysContainSameElements =
  //   _.xor(validMeatForChickenSoup, selectedMeatForChickenSoup).length === 0;
  // console.log(selectedMeatForChickenSoup, doArraysContainSameElements);
  // // const productInfo = dataHelper.findItem(productsData, data.product);
  // console.log({ data, columnsInTasks, wildBoarMeatColumns });
  const isValid = () => {
    if (level === 'easy') {
      if (columnsInTasks.contentBeef1) {
        if (
          columnsInTasks.contentBeef1.list[0] &&
          columnsInTasks.contentBeef2.list[0] &&
          columnsInTasks.contentBeef3.list[0] &&
          columnsInTasks.contentBeef4.list[0] &&
          columnsInTasks.contentBeef5.list[0] &&
          columnsInTasks.contentBeef6.list[0] &&
          columnsInTasks.contentBeef7.list[0] &&
          columnsInTasks.contentBeef8.list[0] &&
          columnsInTasks.contentBeef9.list[0] &&
          columnsInTasks.contentBeef10.list[0] &&
          columnsInTasks.contentBeef11.list[0] &&
          columnsInTasks.contentBeef12.list[0] &&
          columnsInTasks.contentBeef13.list[0] &&
          columnsInTasks.contentBeef14.list[0]
        ) {
          if (
            columnsInTasks.contentBeef1.list[0].text !== 'Rozbratel' ||
            columnsInTasks.contentBeef2.list[0].text !== 'Antrykot' ||
            columnsInTasks.contentBeef3.list[0].text !== 'Rostbef' ||
            columnsInTasks.contentBeef4.list[0].text !== 'Polędwica' ||
            columnsInTasks.contentBeef5.list[0].text !== 'Udziec' ||
            columnsInTasks.contentBeef6.list[0].text !== 'Ogon' ||
            columnsInTasks.contentBeef7.list[0].text !== 'Pręga tylna' ||
            columnsInTasks.contentBeef8.list[0].text !== 'Łata' ||
            columnsInTasks.contentBeef9.list[0].text !== 'Szponder' ||
            columnsInTasks.contentBeef10.list[0].text !== 'Pręga przednia' ||
            columnsInTasks.contentBeef11.list[0].text !== 'Mostek' ||
            columnsInTasks.contentBeef12.list[0].text !== 'Mięso z łopatki' ||
            columnsInTasks.contentBeef13.list[0].text !== 'Karkówka' ||
            columnsInTasks.contentBeef14.list[0].text !== 'Szyja'
          ) {
            setIsBeefMeat(false);
            initModal({
              type: 'error',
              title: 'Niestety...',
              text: 'Nie wszystko poszło dobrze. Ten element tuszy został umieszczony w nieprawidłowym miejscu na widocznym schemacie. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
            });
          } else {
            setIsBeefMeat(true);
            initModal({
              type: 'success',
              title: 'Brawo!',
              text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący półtusz i poszczególnych ich elementów.',
            });
          }
        } else {
          setIsBeefMeat(false);
          initModal({
            type: 'warning',
            title: 'Niestety...',
            text: 'Nie uzupełniłeś wszystkich elementów na schemacie półtuszy. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału.',
          });
        }
      } else if (columnsInTasks.contentPork1) {
        if (
          columnsInTasks.contentPork1.list[0] &&
          columnsInTasks.contentPork2.list[0] &&
          columnsInTasks.contentPork3.list[0] &&
          columnsInTasks.contentPork4.list[0] &&
          columnsInTasks.contentPork5.list[0] &&
          columnsInTasks.contentPork6.list[0] &&
          columnsInTasks.contentPork7.list[0] &&
          columnsInTasks.contentPork8.list[0] &&
          columnsInTasks.contentPork9.list[0] &&
          columnsInTasks.contentPork10.list[0] &&
          columnsInTasks.contentPork11.list[0] &&
          columnsInTasks.contentPork12.list[0] &&
          columnsInTasks.contentPork13.list[0] &&
          columnsInTasks.contentPork14.list[0] &&
          columnsInTasks.contentPork15.list[0]
        ) {
          if (
            columnsInTasks.contentPork1.list[0].text !== 'Głowa z ryjem' ||
            columnsInTasks.contentPork2.list[0].text !== 'Karkówka' ||
            columnsInTasks.contentPork3.list[0].text !== 'Schab' ||
            columnsInTasks.contentPork4.list[0].text !== 'Słonina' ||
            columnsInTasks.contentPork5.list[0].text !== 'Biodrówka' ||
            columnsInTasks.contentPork6.list[0].text !== 'Ogon' ||
            columnsInTasks.contentPork7.list[0].text !== 'Szynka' ||
            columnsInTasks.contentPork8.list[0].text !== 'Golonka tylna' ||
            columnsInTasks.contentPork9.list[0].text !== 'Nogi' ||
            columnsInTasks.contentPork10.list[0].text !== 'Pachwina' ||
            columnsInTasks.contentPork11.list[0].text !== 'Boczek' ||
            columnsInTasks.contentPork12.list[0].text !== 'Żeberka' ||
            columnsInTasks.contentPork13.list[0].text !== 'Łopatka' ||
            columnsInTasks.contentPork14.list[0].text !== 'Golonka przednia' ||
            columnsInTasks.contentPork15.list[0].text !== 'Podgardle'
          ) {
            setIsPorkMeat(false);

            initModal({
              type: 'error',
              title: 'Niestety...',
              text: 'Nie wszystko poszło dobrze. Ten element tuszy został umieszczony w nieprawidłowym miejscu na widocznym schemacie. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
            });
          } else {
            setIsPorkMeat(true);

            initModal({
              type: 'success',
              title: 'Brawo!',
              text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący półtusz i poszczególnych ich elementów.',
            });
          }
        } else {
          setIsPorkMeat(false);

          initModal({
            type: 'warning',
            title: 'Niestety...',
            text: 'Nie uzupełniłeś wszystkich elementów na schemacie półtuszy. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału.',
          });
        }
      } else if (columnsInTasks.contentVeal1) {
        if (
          columnsInTasks.contentVeal1.list[0] &&
          columnsInTasks.contentVeal2.list[0] &&
          columnsInTasks.contentVeal3.list[0] &&
          columnsInTasks.contentVeal4.list[0] &&
          columnsInTasks.contentVeal5.list[0] &&
          columnsInTasks.contentVeal6.list[0] &&
          columnsInTasks.contentVeal7.list[0] &&
          columnsInTasks.contentVeal8.list[0] &&
          columnsInTasks.contentVeal9.list[0] &&
          columnsInTasks.contentVeal10.list[0] &&
          columnsInTasks.contentVeal11.list[0] &&
          columnsInTasks.contentVeal12.list[0]
        ) {
          if (
            columnsInTasks.contentVeal1.list[0].text !== 'Szyja' ||
            columnsInTasks.contentVeal2.list[0].text !== 'Górka' ||
            columnsInTasks.contentVeal3.list[0].text !== 'Nerkówka' ||
            columnsInTasks.contentVeal4.list[0].text !== 'Udziec' ||
            columnsInTasks.contentVeal5.list[0].text !== 'Ogon' ||
            columnsInTasks.contentVeal6.list[0].text !== 'Goleń tylna' ||
            columnsInTasks.contentVeal7.list[0].text !== 'Łata' ||
            columnsInTasks.contentVeal8.list[0].text !== 'Szponder' ||
            columnsInTasks.contentVeal9.list[0].text !== 'Goleń przednia' ||
            columnsInTasks.contentVeal10.list[0].text !== 'Mostek' ||
            columnsInTasks.contentVeal11.list[0].text !== 'Łopatka' ||
            columnsInTasks.contentVeal12.list[0].text !== 'Karkówka'
          ) {
            setIsVealMeat(false);

            initModal({
              type: 'error',
              title: 'Niestety...',
              text: 'Nie wszystko poszło dobrze. Ten element tuszy został umieszczony w nieprawidłowym miejscu na widocznym schemacie. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
            });
          } else {
            setIsVealMeat(true);

            initModal({
              type: 'success',
              title: 'Brawo!',
              text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący półtusz i poszczególnych ich elementów.',
            });
          }
        } else {
          setIsVealMeat(false);

          initModal({
            type: 'warning',
            title: 'Niestety...',
            text: 'Nie uzupełniłeś wszystkich elementów na schemacie półtuszy. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału.',
          });
        }
      } else if (columnsInTasks.contentSheep1) {
        if (
          columnsInTasks.contentSheep1.list[0] &&
          columnsInTasks.contentSheep2.list[0] &&
          columnsInTasks.contentSheep3.list[0] &&
          columnsInTasks.contentSheep4.list[0] &&
          columnsInTasks.contentSheep5.list[0] &&
          columnsInTasks.contentSheep6.list[0] &&
          columnsInTasks.contentSheep7.list[0] &&
          columnsInTasks.contentSheep8.list[0] &&
          columnsInTasks.contentSheep9.list[0]
        ) {
          if (
            columnsInTasks.contentSheep1.list[0].text !== 'Górka' ||
            columnsInTasks.contentSheep2.list[0].text !== 'Karkówka' ||
            columnsInTasks.contentSheep3.list[0].text !== 'Antrykot' ||
            columnsInTasks.contentSheep4.list[0].text !== 'Comber' ||
            columnsInTasks.contentSheep5.list[0].text !== 'Ogon' ||
            columnsInTasks.contentSheep6.list[0].text !== 'Udziec' ||
            columnsInTasks.contentSheep7.list[0].text !== 'Goleń przednia' ||
            columnsInTasks.contentSheep8.list[0].text !== 'Goleń tylna' ||
            columnsInTasks.contentSheep9.list[0].text !== 'Mostek'
          ) {
            setIsSheepMeat(false);

            initModal({
              type: 'error',
              title: 'Niestety...',
              text: 'Nie wszystko poszło dobrze. Ten element tuszy został umieszczony w nieprawidłowym miejscu na widocznym schemacie. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
            });
          } else {
            setIsSheepMeat(true);

            initModal({
              type: 'success',
              title: 'Brawo!',
              text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący półtusz i poszczególnych ich elementów.',
            });
          }
        } else {
          setIsSheepMeat(false);

          initModal({
            type: 'warning',
            title: 'Niestety...',
            text: 'Nie uzupełniłeś wszystkich elementów na schemacie półtuszy. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału.',
          });
        }
      } else if (columnsInTasks.contentDeer1) {
        if (
          columnsInTasks.contentDeer1.list[0] &&
          columnsInTasks.contentDeer2.list[0] &&
          columnsInTasks.contentDeer3.list[0] &&
          columnsInTasks.contentDeer4.list[0] &&
          columnsInTasks.contentDeer5.list[0] &&
          columnsInTasks.contentDeer6.list[0] &&
          columnsInTasks.contentDeer7.list[0] &&
          columnsInTasks.contentDeer8.list[0] &&
          columnsInTasks.contentDeer9.list[0] &&
          columnsInTasks.contentDeer10.list[0] &&
          columnsInTasks.contentDeer11.list[0]
        ) {
          if (
            columnsInTasks.contentDeer1.list[0].text !== 'Szyja' ||
            columnsInTasks.contentDeer2.list[0].text !== 'Rostbef' ||
            columnsInTasks.contentDeer3.list[0].text !== 'Polędwica' ||
            columnsInTasks.contentDeer4.list[0].text !== 'Udziec' ||
            columnsInTasks.contentDeer5.list[0].text !== 'Łata' ||
            columnsInTasks.contentDeer6.list[0].text !== 'Goleń tylna' ||
            columnsInTasks.contentDeer7.list[0].text !== 'Goleń przednia' ||
            columnsInTasks.contentDeer8.list[0].text !== 'Łopatka' ||
            columnsInTasks.contentDeer9.list[0].text !== 'Mostek' ||
            columnsInTasks.contentDeer10.list[0].text !== 'Szponder' ||
            columnsInTasks.contentDeer11.list[0].text !== 'Górnica'
          ) {
            setIsDeerMeat(false);

            initModal({
              type: 'error',
              title: 'Niestety...',
              text: 'Nie wszystko poszło dobrze. Ten element tuszy został umieszczony w nieprawidłowym miejscu na widocznym schemacie. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
            });
          } else {
            setIsDeerMeat(true);

            initModal({
              type: 'success',
              title: 'Brawo!',
              text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący półtusz i poszczególnych ich elementów.',
            });
          }
        } else {
          setIsDeerMeat(false);

          initModal({
            type: 'warning',
            title: 'Niestety...',
            text: 'Nie uzupełniłeś wszystkich elementów na schemacie półtuszy. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału.',
          });
        }
      } else if (columnsInTasks.contentRoeDeer1) {
        if (
          columnsInTasks.contentRoeDeer1.list[0] &&
          columnsInTasks.contentRoeDeer2.list[0] &&
          columnsInTasks.contentRoeDeer3.list[0] &&
          columnsInTasks.contentRoeDeer4.list[0] &&
          columnsInTasks.contentRoeDeer5.list[0] &&
          columnsInTasks.contentRoeDeer6.list[0] &&
          columnsInTasks.contentRoeDeer7.list[0] &&
          columnsInTasks.contentRoeDeer8.list[0]
        ) {
          if (
            columnsInTasks.contentRoeDeer1.list[0].text !== 'Szyja' ||
            columnsInTasks.contentRoeDeer2.list[0].text !== 'Karczek' ||
            columnsInTasks.contentRoeDeer3.list[0].text !== 'Comber' ||
            columnsInTasks.contentRoeDeer4.list[0].text !== 'Udziec' ||
            columnsInTasks.contentRoeDeer5.list[0].text !== 'Goleń tylna' ||
            columnsInTasks.contentRoeDeer6.list[0].text !== 'Mostek' ||
            columnsInTasks.contentRoeDeer7.list[0].text !== 'Goleń przednia' ||
            columnsInTasks.contentRoeDeer8.list[0].text !== 'Łopatka'
          ) {
            setIsRoeDeerMeat(false);

            initModal({
              type: 'error',
              title: 'Niestety...',
              text: 'Nie wszystko poszło dobrze. Ten element tuszy został umieszczony w nieprawidłowym miejscu na widocznym schemacie. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
            });
          } else {
            setIsRoeDeerMeat(true);

            initModal({
              type: 'success',
              title: 'Brawo!',
              text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący półtusz i poszczególnych ich elementów.',
            });
          }
        } else {
          setIsRoeDeerMeat(false);

          initModal({
            type: 'warning',
            title: 'Niestety...',
            text: 'Nie uzupełniłeś wszystkich elementów na schemacie półtuszy. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału.',
          });
        }
      } else if (columnsInTasks.contentWildBoar1) {
        if (
          columnsInTasks.contentWildBoar1.list[0] &&
          columnsInTasks.contentWildBoar2.list[0] &&
          columnsInTasks.contentWildBoar3.list[0] &&
          columnsInTasks.contentWildBoar4.list[0] &&
          columnsInTasks.contentWildBoar5.list[0] &&
          columnsInTasks.contentWildBoar6.list[0] &&
          columnsInTasks.contentWildBoar7.list[0] &&
          columnsInTasks.contentWildBoar8.list[0] &&
          columnsInTasks.contentWildBoar9.list[0]
        ) {
          if (
            columnsInTasks.contentWildBoar1.list[0].text !== 'Karkówka' ||
            columnsInTasks.contentWildBoar2.list[0].text !== 'Schab' ||
            columnsInTasks.contentWildBoar3.list[0].text !==
              'Szynka z biodrówką' ||
            columnsInTasks.contentWildBoar4.list[0].text !== 'Goleń tylna' ||
            columnsInTasks.contentWildBoar5.list[0].text !==
              'Żeberka z boczkiem' ||
            columnsInTasks.contentWildBoar6.list[0].text !== 'Goleń przednia' ||
            columnsInTasks.contentWildBoar7.list[0].text !== 'Łopatka' ||
            columnsInTasks.contentWildBoar8.list[0].text !== 'Podgardle' ||
            columnsInTasks.contentWildBoar9.list[0].text !== 'Głowa'
          ) {
            setIsWildBoarMeat(false);

            initModal({
              type: 'error',
              title: 'Niestety...',
              text: 'Nie wszystko poszło dobrze. Ten element tuszy został umieszczony w nieprawidłowym miejscu na widocznym schemacie. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
            });
          } else {
            setIsWildBoarMeat(true);

            initModal({
              type: 'success',
              title: 'Brawo!',
              text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący półtusz i poszczególnych ich elementów.',
            });
          }
        } else {
          setIsWildBoarMeat(false);

          initModal({
            type: 'warning',
            title: 'Niestety...',
            text: 'Nie uzupełniłeś wszystkich elementów na schemacie półtuszy. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału.',
          });
        }
      } else {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: 'Nie udzieliłeś żadnej odpowiedzi i na schemacie wybranej półtuszy nie uzupełniłeś poszczególnych jej elementów (np. łopatka czy mostek). Wróć do materiału i jeszcze raz zapoznaj się z częściami różnych półtusz. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
        });
      }
    } else if (level === 'average') {
      if (
        columnsInTasks.content18.list[0] &&
        columnsInTasks.content19.list[0] &&
        columnsInTasks.content20.list[0] &&
        columnsInTasks.content21.list[0] &&
        columnsInTasks.content22.list[0] &&
        columnsInTasks.content23.list[0] &&
        columnsInTasks.content24.list[0] &&
        columnsInTasks.content25.list[0] &&
        columnsInTasks.content26.list[0] &&
        columnsInTasks.content27.list[0]
      ) {
        if (
          columnsInTasks.content18.list[0].text !== 'Boczek' ||
          columnsInTasks.content19.list[0].text !== 'gotowane lub smażone' ||
          columnsInTasks.content20.list[0].text !== 'Łopatka b/k' ||
          columnsInTasks.content21.list[0].text !==
            'potrawy pieczone, potrawy duszone, masa mielona' ||
          columnsInTasks.content22.list[0].text !== 'Golonka przednia' ||
          columnsInTasks.content23.list[0].text !== 'gotowane' ||
          columnsInTasks.content24.list[0].text !== 'Biodrówka' ||
          columnsInTasks.content25.list[0].text !==
            'pieczeń duszona, bryzole, zrazy bite, sznycle panierowane' ||
          columnsInTasks.content26.list[0].text !== 'Szynka' ||
          columnsInTasks.content27.list[0].text !==
            'potrawy pieczone, potrawy duszone, potrawy smażone'
        ) {
          setIsSecondTask(false);
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: 'Nie wszystko poszło dobrze. Ten element został umieszczony w nieprawidłowym miejscu. Wróć do materiału i jeszcze raz zapoznaj się z poszczególnymi częściami kulinarnymi półtuszy wieprzowej oraz z zastosowaniem gastronomicznym poszczególnych części półtuszy wieprzowej. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
          });
        } else {
          setIsSecondTask(true);

          initModal({
            type: 'success',
            title: 'Brawo!',
            text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący części kulinarnych półtuszy wieprzowej i ich zastosowania gastronomicznego.',
          });
        }
      } else {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: 'Nie udzieliłeś żadnej odpowiedzi i pod ilustracjami nie uzupełniłeś części kulinarnych półtuszy wieprzowej. Wróć do materiału i jeszcze raz zapoznaj się z częściami kulinarnymi półtuszy wieprzowej oraz ich zastosowaniem gastronomicznym. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
        });
      }
    } else if (level === 'hard') {
      const selectedMeatForChickenSoup = columnsInTasks.thirdTask1.list.map(
        (item) => item.text
      );

      let areGoodIngredientsForChickenSoup =
        _.xor(correctAnswers.thirdTask1, selectedMeatForChickenSoup).length ===
        0;

      const selectedMeatForRumpSteak = columnsInTasks.thirdTask2.list.map(
        (item) => item.text
      );

      let areGoodIngredientsForRumpSteak =
        _.xor(correctAnswers.thirdTask2, selectedMeatForRumpSteak).length === 0;

      const selectedMeatForMedallion = columnsInTasks.thirdTask3.list.map(
        (item) => item.text
      );

      let areGoodIngredientsForMedallion =
        _.xor(correctAnswers.thirdTask3, selectedMeatForMedallion).length === 0;

      const selectedMeatForSteak = columnsInTasks.thirdTask4.list.map(
        (item) => item.text
      );

      let areGoodIngredientsForSteak =
        _.xor(correctAnswers.thirdTask4, selectedMeatForSteak).length === 0;

      const selectedMeatForBoefStrogonow = columnsInTasks.thirdTask5.list.map(
        (item) => item.text
      );

      let areGoodIngredientsForBoefStrogonow =
        _.xor(correctAnswers.thirdTask5, selectedMeatForBoefStrogonow)
          .length === 0;

      const areFieldsSelected = () => {
        if (
          columnsInTasks.thirdTask1.list.length > 0 &&
          columnsInTasks.thirdTask2.list.length > 0 &&
          columnsInTasks.thirdTask3.list.length > 0 &&
          columnsInTasks.thirdTask4.list.length > 0 &&
          columnsInTasks.thirdTask5.list.length > 0
        ) {
          return true;
        } else {
          return false;
        }
      };

      if (areFieldsSelected()) {
        if (
          areGoodIngredientsForChickenSoup &&
          areGoodIngredientsForBoefStrogonow &&
          areGoodIngredientsForMedallion &&
          areGoodIngredientsForRumpSteak &&
          areGoodIngredientsForSteak
        ) {
          setIsThirdTask(true);
          initModal({
            type: 'success',
            title: 'Brawo!',
            text: 'Dobra robota! Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący mięsa i jego wykorzystania do przygotowania konkretnej potrawy.',
          });
        } else {
          setIsThirdTask(false);
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: 'Nie wszystko poszło dobrze. Ten element został umieszczony w nieprawidłowym miejscu w tabeli i nie pasuje do potrawy. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami półtusz i ich poszczególnymi elementami. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
          });
        }
      } else {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: 'Nie udzieliłeś żadnej odpowiedzi i nie dopasowałeś asortymentu mięsa do konkretnej potrawy. Wróć do materiału i jeszcze raz zapoznaj się z rodzajami  mięsa i ich wykorzystaniem do przygotowania konkretnej potrawy. Pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału. Dzięki temu następnym razem poprawnie rozwiążesz zadanie. Powodzenia!',
        });
      }

      // if (
      //   areGoodIngredientsForChickenSoup &&
      //   areGoodIngredientsForBoefStrogonow &&
      //   areGoodIngredientsForMedallion &&
      //   areGoodIngredientsForRumpSteak &&
      //   areGoodIngredientsForSteak
      // ) {
      //   setIsThirdTask(true);
      //   initModal({
      //     type: "success",
      //     title: "Brawo!",
      //     text: "To jest dobra odpowiedź..",
      //   });
      // } else {
      //   setIsThirdTask(false);
      //   initModal({
      //     type: "error",
      //     // title: "Niestety...",
      //     text: "Czy na pewno? Spróbuj jeszcze raz.",
      //   });
      // }
    } else if (level === 'task05') {
      const correct = correctAnswerTask05.map((item) => item.correctAnswer);
      const chosen = fifthTask.map((item) => item.chosen);

      if (chosen.includes(null)) {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: 'Niestety, nie uporządkowałeś wszystkich elementów. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz do Bazy wiedzy.!',
        });
      } else if (
        chosen !== null &&
        JSON.stringify(chosen) !== JSON.stringify(correct)
      ) {
        setIsFifthTask(false);
        initModal({
          type: 'error',
          title: 'Niestety...',
          text: 'Niestety, nie wszystko poszło dobrze. Wróć do materiału i jeszcze raz zapoznaj się z zagadnieniami dotyczącymi elementów gastronomicznych oraz potraw, które można z nich przyrządzić. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz do Bazy wiedzy. ',
        });
      }
      if (JSON.stringify(chosen) === JSON.stringify(correct)) {
        setIsFifthTask(true);
        initModal({
          type: 'success',
          title: 'Dobra robota!',
          text: 'Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący elementów gastronomicznych oraz potraw, które można z nich przyrządzić. Brawo!',
        });
      }
    }
    if (level === 'task06') {
      if (inputData.length === dataTask6.length) {
        let error = false;
        for (let i = 0; i < dataTask6.length; i++) {
          let filter = inputData.filter(
            (item) => item.key === dataTask6[i].key
          )[0];
          if (filter.value !== dataTask6[i].correctAnswer.toString()) {
            error = true;
          }
        }

        if (error) {
          setIsSixTask(false);
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: 'Nie wszystko poszło dobrze. Wróć do materiału i jeszcze raz zapoznaj się z zagadnieniami dotyczącymi elementów gastronomicznych, metod i urządzeń do obróbki cieplnej oraz potraw. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz do Bazy wiedzy.',
          });
        } else {
          setIsSixTask(true);
          initModal({
            type: 'success',
            title: 'Dobra robota!',
            text: 'Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący elementów gastronomicznych, metod i urządzeń do obróbki cieplnej oraz potraw. Brawo!',
          });
        }
      } else {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: 'Nie wybrałeś wszystkich odpowiedzi. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz do Bazy wiedzy.',
        });
      }
    }
    if (level === 'task04') {
      if (
        CheckColumn('Pork', 6) === false ||
        CheckColumn('Beef', 5) === false ||
        CheckColumn('Veal', 3) === false
      ) {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: 'Nie wybrałeś wszystkich odpowiedzi. Spróbuj dokończyć ćwiczenie. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz do Bazy wiedzy.',
        });
      } else {
        ValidationTask04();
      }
    }
  };

  const ValidationTask04 = () => {
    if (
      CheckError('Pork', 6) === false ||
      CheckError('Beef', 5) === false ||
      CheckError('Veal', 3) === false
    ) {
      setIsFourTask(false);
      initModal({
        type: 'error',
        title: 'Niestety...',
        text: 'Nie wszystko poszło dobrze. Wróć do materiału i jeszcze raz zapoznaj się z zagadnieniami dotyczącymi elementów gastronomicznych, potraw oraz metod obróbki cieplnej. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikonę, po kliknięciu na którą będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz do Bazy wiedzy.',
      });
    } else {
      setIsFourTask(true);
      initModal({
        type: 'success',
        title: 'Dobra robota!',
        text: 'Poprawnie wykonałeś ćwiczenie. Doskonale wykorzystałeś przyswojony materiał dotyczący elementów gastronomicznych, potraw oraz metod obróbki cieplnej. Brawo!',
      });
    }
  };

  const CheckColumn = (type, rows) => {
    for (let index = 0; index < rows; index++) {
      if (!task4Columns[`content${type}Elements${index}`].list.length > 0)
        return false;

      if (!task4Columns[`content${type}Dish${index}`].list.length > 0)
        return false;

      if (!task4Columns[`content${type}Methods${index}`].list.length > 0)
        return false;
    }
  };

  const CheckError = (type, rows) => {
    for (let index = 0; index < rows; index++) {
      let filter = correctAnswerTask04.find(
        (item) =>
          item.name ===
          task4Columns[`content${type}Elements${index}`].list[0].text
      );

      if (filter) {
        if (
          filter.dish ===
            task4Columns[`content${type}Dish${index}`].list[0].text &&
          filter.methods ===
            task4Columns[`content${type}Methods${index}`].list[0].text
        ) {
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  };

  const onClick = () => {
    if (isValid()) {
      initModal({ type: 'success', title: 'Brawo!', text: successInfo });
    }
  };

  return (
    <div className={classes.wrap}>
      <Tooltip
        title={
          <span style={{fontSize: '12px'}}>
            Kliknij, aby sprawdzić, czy dokonałeś poprawnego wyboru
          </span>
        }
        arrow={true}
        placement={'top'}
      >
        <Button onClick={onClick} variant="contained" color="primary">
          Sprawdź
        </Button>
      </Tooltip>
    </div>
  );
};

export default Validation;
