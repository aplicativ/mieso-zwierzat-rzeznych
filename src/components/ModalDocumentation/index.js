import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import { Box, Tab } from "@mui/material";
import { TabContext, TabList, TabPanel } from "@mui/lab";

import useStyles from "./styles";

import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";

import KnowledgeBaseData01 from "./subcomponents/KnowledgeBaseData01";
import KnowledgeBaseData02 from "./subcomponents/KnowledgeBaseData02";
import KnowledgeBaseData03 from "./subcomponents/KnowledgeBaseData03";
import KnowledgeBaseData04 from "./subcomponents/KnowledgeBaseData04";
import KnowledgeBaseData05 from "./subcomponents/KnowledgeBaseData05";

const Modal = ({ isOpen, setIsOpen }) => {
  const [value, setValue] = useState("1");
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const classes = useStyles();

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Dialog
      style={{ zIndex: "999999", fontSize: "18px" }}
      maxWidth="lg"
      fullWidth
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={isOpen}
    >
      <DialogContent>
        <div className={classes.close}>
          <IconButton onClick={handleClose} aria-label="close" component="span">
            <CloseIcon
              style={{
                stroke: "#757575",
                strokeWidth: 2,
              }}
            />
          </IconButton>
        </div>

        <Box>
          <TabContext value={value}>
            <Box sh={{ borderBottom: 1, borderColor: "divider" }}>
              <TabList aria-label="knowledgeTabs" onChange={handleChange}>
                <Tab
                  label={
                    <span className={classes.label}>
                      Charakterystyka elementów gastronomicznych mięsa zwierząt
                      rzeźnych
                    </span>
                  }
                  value="1"
                />
                <Tab
                  label={
                    <span className={classes.label}>
                      Asortyment potraw mięsnych
                    </span>
                  }
                  value="2"
                />
                <Tab
                  label={
                    <span className={classes.label}>
                      Rodzaje metod obróbki cieplnej
                    </span>
                  }
                  value="3"
                />
                <Tab
                  label={<span className={classes.label}>Baza potraw</span>}
                  value="4"
                />
                <Tab
                  label={
                    <span className={classes.label}>
                      Urządzenia do obróbki cieplnej potraw z mięsa wraz z
                      metodami obróbki cieplnej
                    </span>
                  }
                  value="5"
                />
              </TabList>
            </Box>
            <TabPanel value="1">
              <KnowledgeBaseData01 />
            </TabPanel>
            <TabPanel value="2">
              <KnowledgeBaseData02 />
            </TabPanel>
            <TabPanel value="3">
              <KnowledgeBaseData03 />
            </TabPanel>
            <TabPanel value="4">
              <KnowledgeBaseData04 />
            </TabPanel>
            <TabPanel value="5">
              <KnowledgeBaseData05 />
            </TabPanel>
          </TabContext>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default Modal;
