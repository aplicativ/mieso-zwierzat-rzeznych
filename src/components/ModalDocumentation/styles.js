import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  heading: {
    margin: '0 0 10px 0',
    textAlign: 'left',
    textTransform: 'uppercase'
  },
  description: {
    padding: '0 0 0 15px',
    margin: '0',
  },
  close: {
    position: "absolute",
      top: "15px",
      right: "15px",
  },
  paragraph: { fontSize: 18, textAlign: 'left', margin: '0' },
  table: {
    margin: '20px 0 20px 0',
    textAlign: 'left',
    border: '1px solid black',
    borderCollapse: 'collapse',
    fontSize: '18px',
    width: '100%'
  },
  th: {
    textAlign: 'left',
    border: '1px solid black',
    verticalAlign: 'top',
    padding: '10px',
  },
  td: {
    border: '1px solid black',
    padding: '10px',
    verticalAlign: 'top',
  },
  ul: {
    listStyle: 'none',
    padding: '0',
    margin: '0',
  },
  ol: {
    padding: '0 0 0 20px',
    margin: '0',
  },
  caption: {
    fontSize: '18px',
    textAlign: 'left',
    padding: ' 0 10px 10px 0',
  },
  label: {
    fontSize: '18px',
    width: '210px',
  },
}));

export default useStyles;
