import React from 'react';
import useStyles from '../styles';

const KnowledgeBaseData02 = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h3 className={classes.heading}>Asortyment potraw mięsnych</h3>
      <h4 className={classes.heading}>Potrawy z mięsa gotowanego</h4>

      <p className={classes.paragraph}>
        Do przygotowania potraw gotowanych wykorzystuje się elementy
        gastronomiczne, które charakteryzują się mięsem poprzerastanym tkanką
        łączną. Są to zwykle elementy pochodzące przede wszystkim z przedniej
        części tuszy. Dzięki długiemu procesowi gotowania, mięso ulega
        zmiękczeniu.
      </p>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Do gotowania używa się następujących elementów gastronomicznych:
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj mięsa</th>
            <th className={classes.th}>Element gastronomiczny</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Wieprzowina</td>
            <td className={classes.td}>
              Golonka, boczek, żeberka, głowizna, nogi, ucho, ryj, ogon,
              karkówka, łopatka
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Wołowina</td>
            <td className={classes.td}>
              Krzyżowa, pręga, łata, rostbef, karkówka, rozbratel, szponder,
              mostek, antrykot, łopatka, ogon
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Cielęcina</td>
            <td className={classes.td}>
              Górka, karkówka, goleń, mostek, łata, szyja
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Baranina</td>
            <td className={classes.td}>
              Karkówka, górka, antrykot, mostek, goleń
            </td>
          </tr>
        </tbody>
      </table>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Charakterystyka potraw z mięsa gotowanego
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj potrawy</th>
            <th className={classes.th}>Sposób przyrządzania</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Rosół/bulion i sztuka mięsa</td>
            <td className={classes.td}>
              Rosół/bulion (z mniejszą ilością wody) i sztukę mięsa przygotowuje
              się z mięsa wołowego (bez lub z kością). Do tego celu najczęściej
              używa się krzyżowej i zrazowej wewnętrznej, rostbefu, pręgi,
              antrykotu, szpondra i mostka. Wybrane mięso umieszczamy we wrzącej
              wodzie i powoli gotujemy, w temperaturze poniżej 100°C. Długość
              gotowania uzależniona jest od ilości tkanki łącznej w elemencie
              gastronomicznym. Im jest jej więcej, tym czas gotowania jest
              dłuższy.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Potrawki mięsne</td>
            <td className={classes.td}>
              Potrawki mięsne Potrawki przygotowuje się z łopatki, mostka i
              górki cielęcej lub z mostka i karkówki jagnięcej. Mięso gotuje
              się, a z powstałego wywaru sporządza sos.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Mięsa peklowane i wędzone</td>
            <td className={classes.td}>
              Do przygotowania mięsa peklowanego wykorzystuje się golonkę,
              boczek i karkówkę wieprzową. Obróbka termiczna (peklowanie)
              powoduje, że mięso ma intensywny różowy kolor. Przed spożycie
              mmięso należy ostudzić. Do wędzenia można użyć również szynki lub
              polędwicy.
            </td>
          </tr>
        </tbody>
      </table>

      <h4 className={classes.heading}>Potrawy z mięsa smażonego</h4>

      <p className={classes.paragraph}>
        Do przygotowania potraw smażonych wykorzystuje się mięso dojrzałe, z
        niewielką ilością tłuszczu, o delikatnej strukturze tkanek, nieznacznie
        poprzerastane błonami. Natomiast mięso z młodych sztuk, które zawiera
        większą ilość tkanki łącznej, można wykorzystać do przygotowania potraw
        smażonych, ale najpierw należy je ugotować. Mięsa smażone można
        przyrządzać również w panierce (np. kotlety, medaliony, sznycle, mięsa w
        cieście).
      </p>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Do smażenia używa się następujących elementów gastronomicznych:
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj mięsa</th>
            <th className={classes.th}>Element gastronomiczny</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Wieprzowina</td>
            <td className={classes.td}>Schab, szynka, karkówka, biodrówka</td>
          </tr>
          <tr>
            <td className={classes.td}>Wołowina</td>
            <td className={classes.td}>
              Polędwica, rostbef, skrzydło, zrazowa wewnętrzna
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Cielęcina</td>
            <td className={classes.td}>
              Udziec, nerkówka, górka, mostek (po uprzednim ugotowaniu)
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Baranina</td>
            <td className={classes.td}>Comber, udziec, górka</td>
          </tr>
        </tbody>
      </table>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Charakterystyka potraw z mięsa smażonego
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj potrawy</th>
            <th className={classes.th}>Sposób przyrządzania</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Bryzol</td>
            <td className={classes.td}>
              Bryzol to płat mięsa, który należy rozbić na grubość około 0,5 cm
              przy średnicy 14-18 cm. Do sporządzania bryzolu wykorzystuje się
              mięso wieprzowe, wołowe i cielęce.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Filet</td>
            <td className={classes.td}>
              Do przygotowania filetu stosuje się schab wieprzowy, udziec
              cielęcy oraz polędwicę wołową, którym nadaje się kształt
              wydłużonego liścia, o grubości 0,5 cm.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Befsztyk</td>
            <td className={classes.td}>
              Do przygotowania befsztyku wykorzystuje się polędwicę i nadaje jej
              okrągły kształt, o grubości 2 cm.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Szaszłyk</td>
            <td className={classes.td}>
              Do przygotowania szaszłyka wykorzystuje się polędwicę. Niewielkie
              kawałki mięsa nadziewa się na szpadki lub patyczki szaszłykowe i
              przeplata fragmentami słoniny lub boczku, a także warzyw.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Rumsztyk</td>
            <td className={classes.td}>
              Do przygotowania rumsztyku wykorzystuje się polędwicę oraz rostbef
              i nadaje im lekko wydłużony kształt.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Kotlet</td>
            <td className={classes.td}>
              Do przygotowania kotleta wykorzystuje dowolny element mięsa
              (kotlet z wołowiny nosi nazwę antrykotu) i nadaje się mu owalny
              kształt, o grubości 1 cm.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Stek</td>
            <td className={classes.td}>
              Do przygotowania steku wykorzystuje się udziec cielęcy, schab,
              karkówkę i szynkę wieprzową, udziec barani oraz rostbef,
              polędwicę, środkową część szpondra i antrykot wołowy. Stek ma
              kształt okrągły, o średnicy 8-10 cm i grubości 1 cm. Wyróżniamy
              T-bone stek (z polędwicy i rostbefu z kością), Rib Eye (antrykot z
              części między 5 a 12 żebrem) oraz rib (z antrykotu, z kostką
              żebrową).
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Medalion</td>
            <td className={classes.td}>
              Do przygotowania medalionu wykorzystuje się schab i szynkę
              wieprzową oraz frykando cielęce i nadaje im okrągły kształt, o
              grubości około 1 cm.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Mięsa po angielsku</td>
            <td className={classes.td}>
              Smażone mięsa po angielsku, do których używa się najwyższej
              jakości mięs wołowych, przygotowuje się w taki sposób, aby
              zachowały bladoróżowy kolor w przekroju, w partii środkowej były
              czerwone i miały zarumienioną skórkę. W celu sporządzenia mięsa w
              ten sposób, jego temperatura w środku nie możeprzekroczyć 63°C.
              Smażone mięsa po angielsku to: stek, befsztyk, rumsztyk.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Mięsa po wiedeńsku</td>
            <td className={classes.td}>
              Mięsa po wiedeńsku to mięsa w cieście, do przyrządzenia których
              używa się części z młodych sztuk z dużą ilością chrząstek i błon
              (np. mostek cielęcy z kością, kotlet wieprzowy z kością, sznycel
              górski wieprzowy, medalion cielęcy). Mięso najpierw gotuje się,
              potem schładza, a następnie panieruje i smaży.
            </td>
          </tr>
        </tbody>
      </table>

      <h4 className={classes.heading}>Potrawy z mięsa duszonego</h4>

      <p className={classes.paragraph}>
        Do przygotowania potraw duszonych wykorzystuje się mięso przerośnięte
        błonami i ścięgnami. Dzięki procesowi duszenia dochodzi do rozklejenia
        tkanki łącznej, a mięso staje sięmiękkie i nabiera dodatkowych walorów
        smakowych
      </p>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Do smażenia używa się następujących elementów gastronomicznych:
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj mięsa</th>
            <th className={classes.th}>Element gastronomiczny</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Wieprzowina</td>
            <td className={classes.td}>
              Karkówka, łopatka, biodrówka, schab, szynka, żeberka
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Wołowina</td>
            <td className={classes.td}>
              Mostek, rozbratel, łopatka, antrykot, pręga, łata, karkówka,
              rostbef, skrzydło, zrazowa, krzyżowa, ligawa
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Cielęcina</td>
            <td className={classes.td}>
              Łopatka, udziec, nerkówka, górka, mostek, karkówka, łata
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Baranina</td>
            <td className={classes.td}>
              Comber, udziec, górka, mostek, antrykot, łopatka
            </td>
          </tr>
        </tbody>
      </table>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Charakterystyka potraw z mięsa duszonego
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj potrawy</th>
            <th className={classes.th}>Sposób przyrządzania</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Pieczeń duszona</td>
            <td className={classes.td}>
              Do przygotowania pieczeni duszonych wykorzystuje się duże elementy
              mięsa, bez kości i dzieli na podłużne kawałki o masie 1 do 2,5 kg.
              Mięsa przeznaczone na pieczeń na dziko to wołowina i baranina,
              które najpierw należy zamarynować przez 2 do 4 dni, aby mięso
              skruszało.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Sztufada</td>
            <td className={classes.td}>
              Przygotowanie sztufady rozpoczynamy od zamarynowania mięsa co
              najmniej 24 godziny przed przyrządzeniem. Kawałki słoniny,
              oprószone solą i pieprzem, obtacza się w drobno poszatkowanej
              cebuli i majeranku, a następnie szpikuje nimi mięso.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Zrazy bite (bitki) i nadziewane</td>
            <td className={classes.td}>
              Do przygotowania zrazów bitych i nadziewanych wykorzystuje się
              mięso wołowe, wieprzowe i cielęce, pokrojone w poprzek włókien
              tak, aby powstała duża płaszczyzna przekroju. Płat mięsa rozbija
              się na grubość około 1,5 cm i nadaje mu okrągły kształt. Zrazy
              nadziewane (zawijane) rozbija się i formuje na kształt owalny, a
              następnie nakłada nadzienie lub np. pokrojony boczek, słoninę,
              cebulę itp. i zawija.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Gulasz</td>
            <td className={classes.td}>
              Do przygotowania gulaszu wykorzystuje się małe kawałki mięsa
              wieprzowego, wołowego czy cielęcego, które zawierają dużą ilość
              tkanki łącznej, kroi się je w kostkę (3 x 3 cm).
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Paprykarz</td>
            <td className={classes.td}>
              Do przygotowania paprykarza wykorzystuje się cielęcinę lub
              jagnięcinę z kością. Najpierw mięso kroi się na małekawałki, a
              następnie dodaje się cebulę, sproszkowaną słodką paprykę, pomidory
              oraz kwaśną śmietanę.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Boeuf Strogonow</td>
            <td className={classes.td}>
              Do przygotowania potrawy Boeuf Strogonow wykorzystuje mięsowołowe
              (klasyczne danie sporządzasię z polędwicy), pokrojone w paski o
              długości 5 cm oraz szerokości i grubości 1 cm. Po usmażeniu mięso
              łączy się z cebulą, koncentratem pomidorowym, sproszkowaną słodką
              papryką oraz śmietaną.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Ragout</td>
            <td className={classes.td}>
              Do przygotowania ragout wykorzystuje się elementy z kością, np.
              mostek, z różnych gatunków mięsa, które dzieli się na mniejsze
              kawałki i poddaje obróbce cieplnej.
            </td>
          </tr>
        </tbody>
      </table>

      <h4 className={classes.heading}>Potrawy z mięsa pieczonego</h4>

      <p className={classes.paragraph}>
        Do przygotowania potraw pieczonych wykorzystuje się mięso z młodych
        sztuk, lekko przerośnięte.
      </p>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Do pieczenia używa się następujących elementów gastronomicznych:
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj mięsa</th>
            <th className={classes.th}>Element gastronomiczny</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Wieprzowina</td>
            <td className={classes.td}>
              Karkówka, łopatka, schab, szynka, boczek
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Wołowina</td>
            <td className={classes.td}>
              Polędwica,łopatka, rostbef, skrzydło, zrazowa
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Cielęcina</td>
            <td className={classes.td}>Łopatka, udziec, mostek</td>
          </tr>
          <tr>
            <td className={classes.td}>Baranina</td>
            <td className={classes.td}>Comber, udziec, łopatka</td>
          </tr>
        </tbody>
      </table>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Charakterystyka potraw z mięsa pieczonego
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj potrawy</th>
            <th className={classes.th}>Sposób przyrządzania</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Pieczeń z rostbefu</td>
            <td className={classes.td}>
              Do przygotowania pieczeni duszonych wykorzystuje się rostbef, z
              którego usuwa się powięzie, ścięgna i nadmiar tłuszczu oraz
              trybuje kości.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Pieczeń z mostka cielęcego</td>
            <td className={classes.td}>
              Przygotowanie rozpoczynamy od usunięcia kości żebrowych z mostka
              cielęcego, następnie przecina się powięzi między płatami mięśni
              mostka, w wyniku czego powstaje kieszeń, do której wkłada się
              nadzienie.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Pieczeń zawijana</td>
            <td className={classes.td}>
              Do przygotowania pieczeni zwijanych wykorzystuje się elementy
              słabo umięśnione (np. łopatkę), usuwa kości, mięso rozbija i
              formuje w płat o kształcie prostokąta, a następnie zwija w rulon.
            </td>
          </tr>
        </tbody>
      </table>

      <h4 className={classes.heading}>Potrawy z mięsnej masy mielonej</h4>

      <p className={classes.paragraph}>
        Do przygotowania potraw z mięsnej masy mielonej wykorzystuje się łopatkę
        wieprzową, kark, łopatkę i pręgę wołową, kark i łopatkę cielęcą oraz
        łopatkę baranią. Dodatkowo można użyć również okrawków mięsa, które
        pozostają po trybowaniu czy porcjowaniu.
      </p>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Do potraw z mięsnej masy mielonej używa się następujących elementów
          gastronomicznych:
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj mięsa</th>
            <th className={classes.th}>Element gastronomiczny</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Wieprzowina</td>
            <td className={classes.td}>Łopatka</td>
          </tr>
          <tr>
            <td className={classes.td}>Wołowina</td>
            <td className={classes.td}>Łopatka, kark, pręga</td>
          </tr>
          <tr>
            <td className={classes.td}>Cielęcina</td>
            <td className={classes.td}>Łopatka, kark</td>
          </tr>
          <tr>
            <td className={classes.td}>Baranina</td>
            <td className={classes.td}>Łopatka</td>
          </tr>
        </tbody>
      </table>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Charakterystyka potraw z mięsnej masy mielonej
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj potrawy</th>
            <th className={classes.th}>Sposób przyrządzania</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Pulpety gotowane</td>
            <td className={classes.td}>
              Do przygotowania pulpetów wykorzystuje się mięso cielęce lub
              wołowe, któremu po zmieleniu nadaje się kształt kul. Kule przed
              gotowaniem można obtoczyć, np. w mące.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Kotlety mielone smażone</td>
            <td className={classes.td}>
              Do przygotowania kotletów mielonych wykorzystuje się cielęcinę,
              wieprzowinę lub mięso mieszane i nadaje im owalny lub sercowaty
              kształt, o grubości około 1,5 cm, a następnie obtacza w bułce
              tartej.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Kotlety pożarskie smażone</td>
            <td className={classes.td}>
              Do przygotowania kotletów pożarskich wykorzystuje się cielęcinę,
              wieprzowinę lub mięso mieszane i nadaje im owalny kształt, o
              grubości około 1,5 cm, a następnie panieruje podwójnie w mące,
              jaju i bułce tartej.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Klopsy duszone</td>
            <td className={classes.td}>
              Do przygotowania klopsów wykorzystuje się mięso wołowe, wieprzowe
              lub mieszane i nadaje mu okrągły lub jajowaty kształt, a następnie
              obtacza w mące
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Zrazy duszone</td>
            <td className={classes.td}>
              Do przygotowania zrazów wykorzystuje się dowolne mięso i nadaje mu
              walcowaty kształt, o średnicy około 4 cm, a następnie obtacza w
              mące.
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Pieczeń rzymska</td>
            <td className={classes.td}>
              Do przygotowania pieczeni rzymskiej wykorzystuje się mięso wołowe,
              wieprzowe oraz mieszane i nadaje mu walcowaty kształt, o średnicy
              około 8 cm i długości 30 cm, a następnie całość obtacza w bułce
              tartej.
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default KnowledgeBaseData02;
