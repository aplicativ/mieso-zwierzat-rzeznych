import React from 'react';
import useStyles from '../styles';

const KnowledgeBaseData01 = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h3 className={classes.heading}>
        Charakterystyka elementów gastronomicznych mięsa zwierząt rzeźnych
      </h3>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Elementy gastronomiczne półtuszy wieprzowej
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Element gastronomiczny</th>
            <th className={classes.th}>Cechy charakterystyczne</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Głowizna</td>
            <td className={classes.td}>
              Głowizna to cienkie pasma tkanki mięśniowej, przerośnięte
              tłuszczem oraz warstwy skóry na grubym podkładzie tkanki
              tłuszczowej.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Ucho</td>
            <td className={classes.td}>
              Ucho jest zbudowane z tkanki chrzestnej, powięzi, tkanki
              tłuszczowej i skóry.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Ryj</td>
            <td className={classes.td}>
              Ryj to warstwa skóry na podkładzie z tkanki tłuszczowej.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Podgardle</td>
            <td className={classes.td}>
              Podgardle to część tłuszczowo-mięsna, mocno poprzerastana tkanką
              łączną.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Łopatka</td>
            <td className={classes.td}>
              Łopatka to element złożony ze średniej grubości warstw mięśni
              poprzerastanych tkanką łączną i z zewnątrz równomiernie pokryty
              warstwą tłuszczu. Łopatka jest odcięta od półtuszy (bez skóry i
              tłuszczu pachowego).
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Golonka przednia i tylna</td>
            <td className={classes.td}>
              Golonka przednia i tylna to nogi odcięte na wysokości 1/3 kości
              goleni, powyżej stawu skokowo-goleniowego. Golonka przednia i
              tylna zawierają 2/3 kości goleniowych bez nasady dolnej, są
              pokryte grubą skórą, którą od mięsa oddziela podściółka
              tłuszczowa, a małe pęczki mięśni są poprzerastane powięziami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Nogi</td>
            <td className={classes.td}>
              Nogi to element zbudowany z dużej ilości powięzi, tłuszczu,
              ścięgien i skóry, które oddziela się w stawie
              podramienno-nadgarstkowym w przypadku nogi przedniej oraz powyżej
              stawu skokowego w przypadku nogi tylnej.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Karkówka</td>
            <td className={classes.td}>
              Karkówka to karkowa część najdłuższego mięśnia grzbietu, odcięta z
              odcinka szyjnego półtuszy,składającasię z tkanki mięsnej grubo
              włóknistej, poprzerastanej tkanką łączną i tłuszczową.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Schab</td>
            <td className={classes.td}>
              Schab to najdłuższy mięsień grzbietu, odcięty z odcinka
              piersiowo-lędźwiowego półtuszy, jego mięso jest chude i jednolite,
              bez przerostów tłuszczowych, soczyste i o bardzo niskiej
              marmurkowatości. Ma barwę różową, dopuszczalna jest też lekko
              czerwona.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Biodrówka</td>
            <td className={classes.td}>
              Biodrówka to delikatne, jednolite i soczyste mięso, które nie ma
              zbyt wiele tłuszczu. Biodrówka jest odcięta z odcinka krzyżowego
              półtuszy.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Żeberka</td>
            <td className={classes.td}>
              Żeberka to mięso dość intensywnie różowe, długie i cienkie, z
              klawiaturą żeber, z przerostami tłuszczowymi oraz błonąt łuszczową
              na górze. Żeberka odcina się z odcinka piersiowego półtuszy.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Boczek</td>
            <td className={classes.td}>
              Boczek to części tłuszczu poprzerastane cienkimi warstwami mięsa,
              odcięte od półtuszy wraz z dolnymi częściami od piątego do
              ostatniego żebra.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Szynka</td>
            <td className={classes.td}>
              Szynka to tkanka mięsna, która jest delikatna, drobnowłóknista,
              miękka i soczysta, miejscami przerośnięta powięziami (cienkimi
              błonami). Jej stopień marmurkowatości jest często dość wysoki. Na
              górze może mieć też błonę tłuszczową. Jest odcięta z tylnej części
              półtuszy bez nogi. Szynka dzieli się na: frykando I – tylną,
              zewnętrzną część z delikatną, drobno włóknistą tkanką mięsną;
              frykando II – tylną, zewnętrzną część z delikatną tkanką mięsną
              poprzerastaną powięziami; oraz frykando III, czyli przednią część
              szynki z mięsem z warstwami przerośniętymi powięziami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Ogon</td>
            <td className={classes.td}>
              Ogon to cienka warstwa tkanki mięśniowej umieszczona na drobnych
              kostkach i pokryta warstwą skóry.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Polędwiczka</td>
            <td className={classes.td}>
              Polędwiczka to długi, jednorodny, delikatny, drobno włóknisty
              mięsień, w jednej części przylegający do biodrówki, zaś w drugiej
              do schabu. Jest to jeden z najcenniejszych elementów tuszy
              wieprzowej.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Pachwina</td>
            <td className={classes.td}>
              Pachwina to pasma mięśni przerośnięte błonami i tłuszczem, które
              są odcinane z podbrzusza wzdłuż linii podziału półtusz.
            </td>
          </tr>
        </tbody>
      </table>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Elementy gastronomiczne półtuszy wołowej
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Element gastronomiczny</th>
            <th className={classes.th}>Cechy charakterystyczne</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Szyja</td>
            <td className={classes.td}>
              Szyja to przedni odcinek karku i oddziela się ją od ćwierćtuszy
              przedniej. Mięso szyi jest wiotkie, o strukturze pasmowej, z
              licznymi przerostami tłuszczowymi, ścięgnami i błonami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Karkówka</td>
            <td className={classes.td}>
              Karkówka to część z dalszej części karku i przedniej części
              grzbietu, oddzielona od ćwierćtuszy przedniej, o mięsie o dość
              dużym stopniu marmurkowatości i przerostach tłuszczowych, z
              błonami i ścięgnami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Mostek</td>
            <td className={classes.td}>
              Mostek to dolna część partii piersiowej z okrywą tłuszczową. Mięso
              mostka jest twarde, z długimi i mocnymi włóknami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Szponder</td>
            <td className={classes.td}>
              Szponder to element ze środkowej części partii piersiowej z
              mięśniem przepony brzusznej wraz z okrywą tłuszczową. Mięso ma
              liczne przerosty tłuszczowe oraz dużo włókien.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Rozbratel</td>
            <td className={classes.td}>
              Rozbratel jest mięśniem położonym wzdłuż kręgosłupa, z przedniego
              odcinka górnej partii piersiowej wraz z przylegającą chrząstką
              łopatkową. Jest grubowłóknisty, obrośnięty tłuszczem i błonami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Antrykot</td>
            <td className={classes.td}>
              Antrykot to element z górnej części piersiowej ćwierćtuszy
              przedniej z naturalnie przyległą powięzią podskórną i okrywą
              tłuszczową, ma długie przerosty tłuszczowe i charakteryzuje się
              dużą marmurkowatością.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Łopatka</td>
            <td className={classes.td}>
              Łopatka to element górnej częścić wierćtuszy przedniej, bez
              chrząstki łopatkowej, okryty warstwą tłuszczu zewnętrznego. Mięso
              łopatki jest luźne, o niejednorodnej strukturze, z błoną
              tłuszczową przebiegającą w poprzek.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Pręga przednia</td>
            <td className={classes.td}>
              Goleń (pręga) przednia to część środkowa kończyny przedniej,
              odcięta od ćwierćtuszy przedniej w stawie łokciowym. Mięso pręgi
              pochodzi ze środkowej części przedniej nogi i jest mięsem dość
              twardym.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Rostbef</td>
            <td className={classes.td}>
              Rostbef to górny odcinek części lędźwiowo-brzusznej ćwierćtuszy
              tylnej. Uważany jest za jeden z najlepszych i najdelikatniejszych
              kawałków mięsa wołowego. Ma niewiele tłuszczu.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Ligawa</td>
            <td className={classes.td}>
              Ligawa to część ćwierćtuszy tylnej, o zwartej tkance mięśniowej,
              przerośniętej cienkimi błonami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Zrazowa wewnętrzna</td>
            <td className={classes.td}>
              Zrazowa wewnętrzna to to mięsień zwarty, drobnowłóknisty oraz
              jednolity. Jest to mięso chude, o niewielkim stopniu
              marmurkowatości.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Krzyżowa</td>
            <td className={classes.td}>
              Krzyżowa to mięsień przerośnięty cienkimi błonami i tłuszczem.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Skrzydło</td>
            <td className={classes.td}>
              Skrzydło to mięso o soczystej tkance, drobnowłókniste.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Zrazowa zewnętrzna</td>
            <td className={classes.td}>
              Zrazowa to mięsień zwarty, drobnowłóknisty, jednolity oraz
              delikatny. Jest to mięso chude, o niewielkim stopniu
              marmurkowatości.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Łata</td>
            <td className={classes.td}>
              Łatę wołową uzyskuje się, wycinając dolny odcinek ostatniego żebra
              i część zespołu mięśni brzucha. Cienkie warstwy mięśni
              poprzerastane są powięziami i tkanką tłuszczową.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Goleń tylna</td>
            <td className={classes.td}>
              Goleń to środkowa część kończyny tylnej. Jest odcięta wzdłuż linii
              biegnącej przez staw kolanowy. Mięso jest silnie poprzerastane
              błonami i ścięgnami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Ogon</td>
            <td className={classes.td}>
              Ogon wołowy to cienkie warstwy mięśni i powięzi pokrywające kręgi
              ogonowe, jest odcinany od ćwierćtuszy tylnej u nasady, tzn. na
              ostatnim kręgu nieruchomym.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Polędwica</td>
            <td className={classes.td}>
              Polędwica to najmniejsza część tuszy wołowej, waży zaledwie 2-3
              kg. Jest oddzielona od ćwierćtuszy tylnej w okolicy kości
              biodrowej, a potem od kręgów lędźwiowych. Jest chuda i bez
              przerostów tłuszczowych, cechuje się lekką marmurkowatością.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Pachwina</td>
            <td className={classes.td}>
              Pachwina to pasma mięśni przerośnięte błonami i tłuszczem, które
              są odcinane z podbrzusza wzdłuż linii podziału półtusz.
            </td>
          </tr>
        </tbody>
      </table>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Elementy gastronomiczne półtuszy cielęcej
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Element gastronomiczny</th>
            <th className={classes.th}>Cechy charakterystyczne</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Szyja</td>
            <td className={classes.td}>
              Szyja to cienkie warstwy mięśni poprzerastane powięziami, które są
              oddzielone od półtuszy między drugim a trzecim kręgiem szyjnym.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Karkówka</td>
            <td className={classes.td}>
              Karkówka to tylna część karku i przednia część grzbietu, ma
              delikatne przerosty tłuszczowe.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Mostek</td>
            <td className={classes.td}>
              Mostek to dolno-środkowaczęść partii piersiowej półtuszy z połową
              mięśnia przepony brzusznej. To mięso bardzo miękkie i kruche. Jest
              zbudowany z cienkiej warstwy wiotkich mięśni, które są
              przerośnięte grubymi powięziami oraz dużą ilością tkanki
              chrzęstnej.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Górka</td>
            <td className={classes.td}>
              Górka to element odcięty z tylnego odcinka górnej partii
              piersiowej półtuszy. Jest mięśniem zwartym, jednolitym,
              przebiegającym wzdłuż kręgosłupa zwierzęcia, od udźcaaż do karku,
              o dość delikatnej strukturze.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Łopatka</td>
            <td className={classes.td}>
              Łopatka to element odcięty z części górnej przedniej półtuszy, o
              mięsie dość chudym, z delikatną tkanką łączną. Ma dużo błon i
              ścięgien, dlatego w trakcie obróbki termicznej nabiera kleistej
              konsystencji.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Goleń (pręga) przednia i tylna</td>
            <td className={classes.td}>
              Goleń przednia jest oddzielona od odcinka nasadowego kończyny
              przedniej w stawie łokciowym, natomiast goleń tylna od odcinka
              nasadowego kończyny tylnej w stawie kolanowym. Mięso goleni ma
              dużo ścięgien, chrząstek i kostek.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Nerkówka</td>
            <td className={classes.td}>
              Nerkówka zawiera sześć przeciętych na pół kręgów lędźwiowych, a
              także dolne odcinki dwóch ostatnich żeber. Mięso jest miękkie,
              soczyste, z zewnątrz pokryte powięziami, a czasem obrośnięte
              tłuszczem.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Łata</td>
            <td className={classes.td}>
              Łata to dolny odcinek mięśni brzucha, a cienkie warstwy mięśnisą
              poprzerastane błonami.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Udziec (frykando I, II, III)</td>
            <td className={classes.td}>
              Udziec cielęcy zawiera mięsień dwugłowy uda oraz mięśnie
              pośladkowe. Udziec dzieli się na trzy frykanda. Frykando I to
              tylna, wewnętrzna część udźca, o mięsie delikatnym, soczystym i
              jędrnym; frykando II to częśćtylno-zewnętrzna, o delikatnej
              tkance, a frykando III to część przednia, cienka warstwa mięśni z
              grubymi powięziami po stronie zewnętrznej.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Ogon</td>
            <td className={classes.td}>
              Ogon to cienkie pasma mięśni, z dużą liczbą chrząstek i powięzi,
              które odcina się od nasady tylnej części tułowia, na ostatnim
              kręgu nieruchomym.
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default KnowledgeBaseData01;
