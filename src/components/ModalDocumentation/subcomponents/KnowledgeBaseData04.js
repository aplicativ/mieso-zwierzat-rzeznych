import React from 'react';
import useStyles from '../styles';

const KnowledgeBaseData04 = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h3 className={classes.heading}>Baza potraw</h3>

      <table className={classes.table}>
        <caption className={classes.caption}>
          Podany normatyw surowcowy na 5 porcji potrawy
        </caption>
        <thead>
          <tr>
            <th className={classes.th}>Potrawa</th>
            <th className={classes.th}>Składniki</th>
            <th className={classes.th}>Sposób wykonania</th>
            <th className={classes.th}>Rodzaj obróbki cieplnej</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Bulion mięsny</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Pręgawołowa – 600 g</li>
                <li>Kości kruche – 300 g</li>
                <li>Włoszczyzna – 175 g</li>
                <li>Cebula – 35 g</li>
                <li>Białko jaja – 1 szt.</li>
                <li>Natka pietruszki – 5 g</li>
                <li>Ziele angielskie 0,1 g</li>
                <li>Woda – 1300 &#13220;</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Pręgę i kościo płukać. Jeśli jest potrzeba, kości porąbać, a
                  następnie włożyć razem z mięsem do zimnej wody.
                </li>
                <li>Gotować przez 3 godziny, nie szumować bulionu.</li>
                <li>
                  Dodać włoszczyznę, przyrumienioną (opaloną) cebulę i
                  przyprawy.
                </li>
                <li>Gotować przez 45 minut.</li>
                <li>Wyjąć włoszczyznę, mięso i kości.</li>
                <li>Zebrać tłuszcz, a bulion przecedzić.</li>
                <li>Sklarować za pomocą białka jaja.</li>
                <li>
                  Podawać w bulionówkach, np. z posiekaną natką pietruszki.
                </li>
              </ol>
            </td>
            <td className={classes.td}>Gotowanie</td>
          </tr>

          <tr>
            <td className={classes.td}>Potrawka cielęca</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Górka cielęca – 700 g (lub mostek cielęcy 650 g)</li>
                <li>Włoszczyzna (bez kapusty) – 150 g</li>
                <li>Woda – 750 &#13220;</li>
                <li>Mąka pszenna – 50 g</li>
                <li>Masło – 50 g</li>
                <li>Sól</li>
                <li>Natka pietruszki</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Górkę lub mostek poddać obróbce wstępnej.</li>
                <li>Mięso włożyć do wrzącej i osolonej wody.</li>
                <li>
                  Powoli gotować, a gdy mięso będzie na wpół miękkie, dodać
                  włoszczyznę.
                </li>
                <li>
                  Gotować do miękkości, a następnie ugotowane mięso wyjąć,
                  pokroić w poprzek włókien, porcjując z kością.
                </li>
                <li>
                  Z mąki i masła zrobić podprawkę zacieraną i rozprowadzić z
                  niewielką ilością wywaru.
                </li>
                <li>
                  Podprawkę dodać do wywaru, zagotować, a całość doprawić do
                  smaku.
                </li>
                <li>Mięso podgrzać w sosie.</li>
                <li>
                  Podawać z wybranym dodatkiem i posypane natką pietruszki.
                </li>
              </ol>
            </td>
            <td className={classes.td}>Gotowanie</td>
          </tr>

          <tr>
            <td className={classes.td}>Golonka peklowana</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Golonka wieprzowa – 1,5 kg</li>
                <li>Włoszczyzna (bez kapusty) – 150 g</li>
                <li>Pieprz</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Zapeklować golonkę.</li>
                <li>
                  Po peklowaniu golonkę opłukać i włożyć do wrzącego wywaru z
                  włoszczyzny.
                </li>
                <li>Gotować.</li>
                <li>Pod koniec dodać przyprawy.</li>
                <li>Serwować z kością i z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Gotowanie</td>
          </tr>

          <tr>
            <td className={classes.td}>Żeberka wieprzowe</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Żeberka wieprzowe – 1 kg</li>
                <li>Marchew – 60 g</li>
                <li>Pietruszka – 60 g</li>
                <li>Seler – 60 g</li>
                <li>Por – 60 g</li>
                <li>Sól</li>
                <li>Ziele angielskie</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Mięso umyć i osuszyć, podzielić na porcje.</li>
                <li>Zagotować i posolić wodę.</li>
                <li>Mięso włożyć do wrzącej wody.</li>
                <li>Gotować wolno.</li>
                <li>Pod koniec gotowania dodać włoszczyznę.</li>
                <li>Gotować do miękkości.</li>
                <li>Serwować z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Gotowanie</td>
          </tr>

          <tr>
            <td className={classes.td}>Bryzol wołowy</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Wołowina bez kości(skrzydło) – 700 g</li>
                <li>Mąka – 10 g</li>
                <li>Smalec – 50 g</li>
                <li>Masło – 20 g</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso umyć, oczyścić z błon i pokroić w poprzek włókien.
                </li>
                <li>
                  Rozbić na płaty o grubości 0,5 cm, o okrągłym kształcie.
                </li>
                <li>Przed smażeniem posolić i oprószyć mąką.</li>
                <li>
                  Smażyć szybko, na silnie rozgrzanym smalcu, a pod koniec
                  smażenia dodać masło.
                </li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Smażenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Filet wieprzowy</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Schab bez kości – 650 g</li>
                <li>Mąka – 10 g</li>
                <li>Masło – 25 g</li>
                <li>Tłuszcz – 50 g</li>
                <li>Sól</li>
                <li>Pieprz</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso umyć, oczyścić z błon i pokroić w poprzek włókien.
                </li>
                <li>
                  Rozbić na płaty o grubości 0,5 cm, o kształcie wydłużonego
                  liści
                </li>
                <li>Przed smażeniem oprószyć mąką i przyprawami.</li>
                <li>
                  Smażyć na rozgrzanym tłuszczu, a pod koniec smażenia dodać
                  masło.
                </li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Smażenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Szaszłyki</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Szynka wieprzowa – 600 g</li>
                <li>Papryka – 100 g</li>
                <li>Pieczarki – 120 g</li>
                <li>Cebula – 100 g</li>
                <li>Oliwa – 20 &#13220;</li>
                <li>Sok z cytryny</li>
                <li>Sól</li>
                <li>Pieprz</li>
                <li>Papryka w proszku</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Wykonać obróbkę wstępną mięsa i warzyw.</li>
                <li>Mięso wygnieść ręką, pokroić na kostkę o boku 2 cm.</li>
                <li>
                  Skropić sokiem z cytryny, posmarować oliwą, oprószyć solą,
                  pieprzem i papryką.
                </li>
                <li>Tak przygotowane mięso pozostawić na pół godziny.</li>
                <li>
                  Cebulę pokroić w plastry, paprykę w kostkę, pieczarki w grube
                  talarki.
                </li>
                <li>
                  Poszczególne składniki nadziewać naprzemiennie na szpadki.
                </li>
                <li>Smażyć na rozgrzanym tłuszczu z każdej strony.</li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Smażenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Befsztyk po angielsku</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Polędwica wołowa – 625 g</li>
                <li>Smalec – 50 g</li>
                <li>Masło – 25 g</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso opłukać i odsączyć, a następnie oczyścić z nadmiaru
                  tłuszczu, usunąć ścięgna i błony.
                </li>
                <li>
                  Podzielić na porcje i delikatnie rozbić tłuczkiem zwilżonym w
                  wodzie.
                </li>
                <li>
                  Ręką i nożem formować befsztyki o grubości 2 cm, o kształcie
                  okrągłym.
                </li>
                <li>Przed smażeniem oprószyć solą.</li>
                <li>
                  Smażyć na rozgrzanym smalcu, rumieniąc z obydwu stron, a pod
                  koniec smażenia dodać masło.
                </li>
                <li>
                  Podawać z dodatkiem masła (np. chrzanowego) lub smażoną
                  cebulą.
                </li>
              </ol>
            </td>
            <td className={classes.td}>Smażenie po angielsku</td>
          </tr>

          <tr>
            <td className={classes.td}>
              Mostek cielęcy panierowany po wiedeńsku
            </td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Mostek cielęcy – 600 g</li>
                <li>Jaja – 1 szt.</li>
                <li>Bułka tarta – 100 g</li>
                <li>Smalec – 100 g</li>
                <li>Mąka pszenna – 50 g</li>
                <li>Włoszczyzna – 150 g</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Sporządzić wywar z włoszczyzny.</li>
                <li>
                  Umyty mostek cielęcy gotować we wrzącym i osolonym wywarze.
                </li>
                <li>Wyjąć, usunąć kość, ostudzić.</li>
                <li>Porcje mięsa panierować w mące, jaju i bułce tartej.</li>
                <li>Smażyć na rozgrzanym smalcu, rumieniąc na złoty kolor.</li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Smażenie</td>
          </tr>

          <tr>
            <td className={classes.td}>
              Zrazy wołowe bite w sosie śmietanowym
            </td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Zrazowa wołowa – 875 g</li>
                <li>Smalec – 75 g</li>
                <li>Mąka pszenna – 25 g</li>
                <li>Cebula – 100 g</li>
                <li>Śmietana – 75 g</li>
                <li>Sól</li>
                <li>Pieprz</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Mięso opłukać i odsączyć, a następnie pokroić w plastry</li>
                <li>Rozbić tłuczkiem, nadając okrągły kształt.</li>
                <li>Przed smażeniem oprószyć solą, pieprzem i mąką.</li>
                <li>Smażyć na rozgrzanym smalcu, rumieniąc z obydwu stron</li>
                <li>
                  Mięso przełożyć do rondla, zalać niewielką ilością wody lub
                  wywaru, a na pozostałym tłuszczu zarumienić cebulę i dodać do
                  mięsa.
                </li>
                <li>Mięso dusić pod przykryciem do miękkości.</li>
                <li>
                  Pod koniec duszenia do sosu dodać zawiesinę ze śmietany i
                  pozostałej części mąki.
                </li>
                <li>Zagotować.</li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Duszenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Zrazy kasztelańskie</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Mięsowołowe (zrazowa) – 1 kg</li>
                <li>Smalec – 50 g</li>
                <li>Masło – 20 g</li>
                <li>Mąka – 10 g</li>
                <li>Cebula – 100 g</li>
                <li>Śmietana – 125 g</li>
                <li>Chleb razowy – 100 g</li>
                <li>Grzyby suszone – 15 g</li>
                <li>Sól</li>
                <li>Pieprz</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Grzyby umyć i namoczyć w wodzie, a następnie ugotować i
                  pokroić w grube paski.
                </li>
                <li>
                  Cebulę drobno posiekać, zrumienić na maśle i połączyć z
                  grzybami. Doprawić solą i pieprzem.
                </li>
                <li>Chleb pokroić w paski.</li>
                <li>
                  Mięso opłukać i odsączyć, a następnie pokroić w plastry.
                </li>
                <li>Rozbić tłuczkiem na cienkie płaty.</li>
                <li>Na każdym płacie ułożyć chleb oraz grzyby z cebulą.</li>
                <li>Zwinąć i spiąć wykałaczką.</li>
                <li>Przed smażeniem oprószyć solą i mąką.</li>
                <li>Smażyć na rozgrzanym smalcu, rumieniąc z obydwu stron.</li>
                <li>
                  Mięso przełożyć do rondla, zalać niewielką ilością wywaru z
                  grzybów.
                </li>
                <li>Mięso dusić pod przykryciem do miękkości.</li>
                <li>
                  Pod koniec duszenia doprawić solą i pieprzem oraz śmietaną.
                </li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Duszenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Gulasz wieprzowy</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Wieprzowina z kością(łopatka) – 1 kg</li>
                <li>Smalec – 80 g</li>
                <li>Mąka – 25 g</li>
                <li>Cebula – 100 g</li>
                <li>Koncentrat pomidorowy – 30 g</li>
                <li>Sól</li>
                <li>Papryka</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso opłukać i odsączyć, wyluzować z kości i pokroić w kostkę
                  3 x 3 cm
                </li>
                <li>
                  Mięso posolić, oprószyć mąką i obsmażyć na rozgrzanym smalcu.
                </li>
                <li>
                  Mięso przełożyć do rondla, zalać wrzącą wodą i dusić pod
                  przykryciem.
                </li>
                <li>
                  Obsmażyć cebulę i wraz z koncentratem pomidorowym i papryką
                  dodać do duszonego mięsa.
                </li>
                <li>Dusić do miękkości, w miarę potrzeby uzupełniając wodę.</li>
              </ol>
            </td>
            <td className={classes.td}>Duszenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Boeuf Strogonow</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Rostbef – 1,2 kg</li>
                <li>Smalec – 75 g</li>
                <li>Cebula – 100 g</li>
                <li>Koncentrat pomidorowy – 50 g</li>
                <li>Śmietana – 100 g</li>
                <li>Mąka – 30 g</li>
                <li>Sól</li>
                <li>Pieprz</li>
                <li>Papryka</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso opłukać i odsączyć, oddzielić od kości i zdjąć błonę
                  otaczającą mięso.
                </li>
                <li>
                  Mięso pokroić w paski o długości 5 cm i szerokości 1 cm.
                </li>
                <li>
                  Mięso posolić, oprószyć pieprzem i mąką i obsmażyć na silnie
                  rozgrzanym smalcu.
                </li>
                <li>
                  Mięso przełożyć do rondla, podlać wywarem lub wodą i dusić.
                </li>
                <li>
                  Na pozostałym po smażeniu tłuszczu podsmażyć pokrojoną w
                  piórka cebulę i dodać do mięsa wraz z koncentratem
                  pomidorowym.
                </li>
                <li>Dusić do miękkości.</li>
                <li>Podprawić śmietaną i dodać pozostałe przyprawy.</li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Duszenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Polędwica po angielsku</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Polędwica wołowa – 700 g</li>
                <li>Tłuszcz – 50 g</li>
                <li>Masło – 25 g</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso opłukać i odsączyć, usunąć tkankę łączną z powierzchni
                  mięsa.
                </li>
                <li>
                  Mięso posolić, obrumienić szybko na patelni i ułożyć w
                  brytfannie.
                </li>
                <li>Wstawić do nagrzanego piekarnika i piec około 15 minut.</li>
                <li>Do sosu dodać masło.</li>
                <li>Porcjować mięso na skośne cienkie płaty.</li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Pieczenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Schab nadziewany</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Schab bez kości – 700 g</li>
                <li>Śliwki suszone – 200 g</li>
                <li>Wino czerwone wytrawne – 200 ml</li>
                <li>Majeranek – 10 g</li>
                <li>Czosnek – 50 g</li>
                <li>Masło – 35 g</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso opłukać i odsączyć, usunąć błony i nadmiar tłuszczu.
                </li>
                <li>
                  Mięso natrzeć solą, zmiażdżonym czosnkiem i majerankiem.
                </li>
                <li>Namoczyć śliwki w winie.</li>
                <li>
                  Wzdłuż włókien mięsa zrobić otwór, używając wąskiego noża lub
                  szpikulca.
                </li>
                <li>W otwór włożyć śliwki.</li>
                <li>
                  Mięso ułożyć w brytfannie, polać roztopionym masłem i wstawić
                  do nagrzanego piekarnika.
                </li>
                <li>
                  Podczas pieczenia polewać mięso sosem, skrapiać wodą lub
                  czerwonym winem.
                </li>
                <li>Po upieczeniu mięso wystudzić i porcjować.</li>
              </ol>
            </td>
            <td className={classes.td}>Pieczenie</td>
          </tr>

          <tr>
            <td className={classes.td}>Sznycel ministerski</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Cielęcina lub wieprzowina bez kości(łopatka) – 350 g</li>
                <li>Czerstwa bułka – 50 g</li>
                <li>Mleko – 100 ml</li>
                <li>Jajo – 1 szt.</li>
                <li>Bułka na grzanki – 80 g</li>
                <li>Bułka tarta – 40 g</li>
                <li>Tłuszcz – 80 g</li>
                <li>Sól</li>
                <li>Biały pieprz</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Czerstwą bułkę namoczyć w mleku, odcisnąć i zemleć z mięsem.
                </li>
                <li>Dodać jajo, sól i pozostałe mleko.</li>
                <li>Wyrobić masę.</li>
                <li>
                  Bułkę pokroić na grzanki (1,5-2 cm x 0,5 cm x 3 mm) i
                  wymieszać z bułką tartą.
                </li>
                <li>
                  Masę podzielić na porcje, obtoczyć w bułce z grzankami,
                  uformować sznycle o grubości 1,5 cm i kształcie prostokąta.
                </li>
                <li>Smażyć na rozgrzanym tłuszczu, rumieniąc z obu stron.</li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Z masy mielonej</td>
          </tr>

          <tr>
            <td className={classes.td}>Klopsiki w sosie pomidorowym</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Mięso mielone wieprzowo-wołowe – 350 g</li>
                <li>Czerstwa bułka – 40 g</li>
                <li>Cebula – 40 g</li>
                <li>Mąka ziemniaczana – 10 g</li>
                <li>Jajo – 1/2 szt.</li>
                <li>Mąka pszenna – 50 g</li>
                <li>Koncentrat pomidorowy – 50 g</li>
                <li>Śmietana 18% – 100 g</li>
                <li>Tłuszcz – 40 g</li>
                <li>Wywar z warzyw – 350-400 ml</li>
                <li>Sól</li>
                <li>Pieprz</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Z mięsa sporządzić masę mieloną.</li>
                <li>Uformować klopsiki i obtoczyć w mące pszennej.</li>
                <li>Obsmażyć na rozgrzanym tłuszczu.</li>
                <li>
                  Klopsiki przełożyć do garnka i zalać niewielką ilością wywaru
                  z warzyw.
                </li>
                <li>Dusić przez kilkanaście minut.</li>
                <li>
                  Dodać koncentrat pomidorowy i zagęścić zawiesiną ze śmietany i
                  pozostałej mąki.
                </li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Z masy mielonej</td>
          </tr>

          <tr>
            <td className={classes.td}>Kotlet pożarski z mięsa mieszanego</td>
            <td className={classes.td}>
              <ul className={classes.ul}>
                <li>Cielęcina bez kości – 300 g</li>
                <li>Wieprzowina bez kości – 80 g</li>
                <li>Czerstwa bułka – 50 g</li>
                <li>Mleko – 100 &#13220;</li>
                <li>Mąka – 50 g</li>
                <li>Jajo – 1 lub 2 szt.</li>
                <li>Bułka tarta – 50 g</li>
                <li>Olej – 50 g</li>
                <li>Sól</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso umyć, osuszyć, pokroić na kawałki i sporządzić masę
                  mieloną.
                </li>
                <li>Czerstwą bułkę namoczyć w mleku, odcisnąć i zemleć.</li>
                <li>
                  Wyrobić masę, dodając zmieloną bułkę, jaja, sól i pozostałe
                  mleko.
                </li>
                <li>
                  Uformować 10 kotletów, które należy panierować w mące, jaju,
                  bułce tartej.
                </li>
                <li>Smażyć na rozgrzanym oleju, rumieniąc z obu stron.</li>
                <li>Podawać z wybranym dodatkiem.</li>
              </ol>
            </td>
            <td className={classes.td}>Z masy mielonej</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default KnowledgeBaseData04;
