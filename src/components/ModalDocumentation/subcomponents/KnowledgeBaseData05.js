import React from 'react';
import useStyles from '../styles';

const KnowledgeBaseData05 = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h3 className={classes.heading}>
        Urządzenia do obróbki cieplnej potraw z mięsa wraz z metodami obróbki
        cieplnej
      </h3>

      <table className={classes.table}>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj obróbki cieplnej</th>
            <th className={classes.th}>Maszyna/urządzenie</th>
            <th className={classes.th}>Instrukcja obsługi</th>
            <th className={classes.th}>Potrawa</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td rowSpan={'5'} className={classes.td}>
              Gotowanie
            </td>
            <td className={classes.td}>
              <p>Kocioł warzelny </p>
              Kocioł warzelny może być:
              <ul>
                <li>elektryczny</li>
                <li>gazowy</li>
                <li>parowy</li>
              </ul>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Osoba pracująca przy kotle warzelnym musi odbyć szkolenie z
                  zakresu BHP dotyczącepracy z urządzeniami pod ciśnieniem.
                </li>
                <li>Należy sprawdzić na wodomierzu poziom wody.</li>
                <li>Napełnić odpowiednio komorę warzelną.</li>
                <li>Włączyć włącznik główny.</li>
                <li>
                  Regulować poziom nagrzewania zgodnie ze wskazaniami manometru.
                </li>
                <li>Po zakończonej pracy odłączyć kocioł od zasilania.</li>
                <li>
                  Po wyładowaniu potrawy, urządzenie należy ostudzić i dokładnie
                  umyć komorę warzelną.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>
                Kotły warzelne służą do gotowania potraw płynnych i półpłynnych,
                w komorze roboczej o dużej pojemności – nawet do 120 l, takich
                jak:
              </p>
              <ul>
                <li>zupy</li>
                <li>wywary</li>
                <li>potrawki</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Taboret grzewczy</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Taboret grzewczy to sprzęt uzupełniający dla kotła warzelnego,
                  ponieważ jest niski i można na nim ustawić garnek o dużej
                  pojemności
                </li>
                <li>
                  Osoba pracująca przy taborecie musi przejść odpowiednie
                  szkolenie BHP.
                </li>
                <li>
                  Przed przystąpieniem do pracy należy dokładnie sprawdzić, czy
                  wszystkie elementy taboretu gazowego znajdują się w
                  odpowiednim miejscu. Taboret elektryczny i indukcyjny nie mają
                  kilku części palnika.
                </li>
                <li>
                  Jeżeli używamy taboretu gazowego, to najpierw należy podpalić
                  zapalnik, tak zwany pilot płomienia, a następnie odkręcić
                  kurek z gazem i zaczekać, aż się zapali.
                </li>
                <li>
                  Ustawić napełniony garnek i przeprowadzić obróbkę cieplną,
                  dostosowując pokrętłem regulującym dopływ gazu wielkość
                  płomienia do garnka tak, aby płomień nie wychodził poza
                  garnek.
                </li>
                <li>
                  Po zakończeniu pracy wyłączyć dopływ gazu poprzez zakręcenie
                  kurka gazu i zdjąć garnek.
                </li>
                <li>
                  Odczekać, aż elementy taboretu ostygną i dokładnie go umyć.
                </li>
                <li>
                  Taboret elektryczny i indukcyjny nie mają otwartego płomienia,
                  więc aby z nich korzystać, należy włączyć je specjalnym
                  pokrętłem, a następnie ustawić garnek.
                </li>
                <li>
                  Po pracy należy odłączyć zasilanie tym samym pokrętłem i zdjąć
                  garnek, a następnie dokładnie umyć urządzenie.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>
                Taborety grzewcze są stosowane jako urządzenie podgrzewcze lub –
                z odpowiedniej wielkości garnkiem o pojemności 30 lub 50 litrów
                – mogą zastąpić kotły warzelne. Służą do gotowania:
              </p>
              <ul>
                <li>zup</li>
                <li>wywarów</li>
                <li>sosów potrawkowych.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Szybkowar</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Szybkowar to urządzenie pracujące pod zwiększonym ciśnieniem,
                  co znacznie skraca czas obróbki cieplnej potraw.
                </li>
                <li>
                  Ciśnienie jest wytwarzane przez paręwodną w szczelnie
                  zamkniętym,dzięki specjalnej pokrywie, granku.
                </li>
                <li>
                  Na pokrywie znajduje się specjalny zawór, który reguluje
                  wypływ pary z garnka oraz dodatkowe zabezpieczenie przed zbyt
                  dużym wzrostem ciśnienia wewnątrz.
                </li>
                <li>
                  Aby ugotować potrawę, należy ściśle przestrzegać instrukcji.
                </li>
                <li>
                  Należy napełnić garnek wodą do określonego poziomu, którego
                  nie wolno przekroczyć.
                </li>
                <li>
                  Należy wybrać odpowiednią opcję gotowania, zgodną z
                  instrukcją, najczęściej jest to mięso lub warzywa.
                </li>
                <li>
                  Podczas pracy należy obserwować specjalny zawór, który
                  reguluje wypływ nadmiaru pary wodnej i w razie potrzeby
                  dostosować parametry grzania.
                </li>
                <li>
                  Podczas gotowania nie wolno na siłę otwierać pokrywy
                  szybkowaru.
                </li>
                <li>
                  Po zakończeniu pracy odczekać chwilę, aby poziom pary wodnej
                  oraz ciśnienia wyregulowały się, a następnie otworzyć garnek.
                </li>
                <li>Po wyładowaniu potrawy dokładnie umyć szybkowar.</li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>
                Szybkowary skracają czas obróbki cieplnej, a ich pojemność
                mieścisię w granicach od 2,5 do 8,5 litra. Szybkowary służą do
                gotowania:
              </p>
              <ul>
                <li>zup</li>
                <li>wywarów</li>
                <li>mięsa.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Steamer (parownik)</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Steamer to urządzenie gotujące w parze wodnej pod zwiększonym
                  ciśnieniem lub w ciśnieniu atmosferycznym.
                </li>
                <li>Budową przypomina piec wolnostojący.</li>
                <li>
                  Może być ogrzewany gazem lub elektryczny. Wyposażony jest w
                  wytwornicę pary.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Steamer (parownik) służy do gotowania:</p>
              <ul>
                <li>pulpetów mięsnych</li>
                <li>głęboko mrożonych produktów żywnościowych</li>
              </ul>
              <p>oraz</p>
              <ul>
                <li>odmrażania;</li>
                <li>podgrzewania.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Cyrkulator temperatury sous vide</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Należy wybrać odpowiednie naczynie do gotowania.</li>
                <li>
                  Umieścić cyrkulator temperatury tak, aby panel sterowania
                  znajdował się na zewnątrz naczynia do gotowania.
                </li>
                <li>
                  Napełnić naczynie do gotowania odpowiednią ilością wody
                  (według miarki, co najmniej do poziomu minimum).
                </li>
                <li>
                  Woda zaczyna parować, gdy osiągnie wyższą temperaturę.
                  Generator jest wyposażony w czujnik monitorujący poziom wody.
                  Jeżeli poziom wody spadnie poniżej minimum, cyrkulator
                  automatycznie się wyłącza.
                </li>
                <li>
                  Zaprogramować zalecaną temperaturę i czas do sporządzenia
                  potrawy.
                </li>
                <li>Aktualna temperatura zacznie migać na wyświetlaczu.</li>
                <li>Produkt spakować próżniowo z przyprawami.</li>
                <li>Spakowany produkt umieścić w naczyniu z wodą.</li>
                <li>
                  Po obróbce cieplnej, urządzenie należy ostudzić, a następnie
                  umyć i oczyścić naczynie i cyrkulator.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>
                Cyrkulator temperatury sous vide służy do gotowania surowych
                produktów pakowanych próżniowo, w niskiej temperaturze. Można w
                nim gotować takie produkty, jak:
              </p>
              <ul>
                <li>cielęcinę, 65-68ºC;</li>
                <li>wołowinę, 54-63°C;</li>
                <li>wieprzowinę, 52-68°C.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td rowSpan={'5'} className={classes.td}>
              Smażenie
            </td>
            <td className={classes.td}>Patelnie gastronomiczne</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Patelnię należy ustawić na stabilnym podłożu i podłączyć do
                  zasilania.
                </li>
                <li>
                  Komorę roboczą napełnić wskazaną w recepturze ilością
                  tłuszczu.
                </li>
                <li>Ustawić odpowiednią temperaturę komory roboczej.</li>
                <li>
                  Gdy tłuszcz osiągnie odpowiednią temperaturę, smażyć
                  półprodukty.
                </li>
                <li>
                  Po pracy należy użyć specjalnego mechanizmu, który pomoże w
                  usunięciu pozostałości potrawy – najczęściej jest to pokrętło,
                  które zmienia nachylenie komory roboczej.
                </li>
                <li>
                  Urządzenie należy ostudzić, a następnie dokładnie umyć komorę
                  roboczą i ją osuszyć.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>
                Patelnie gastronomiczne służą do smażenia większej liczby
                produktów w małej i średniej ilości tłuszczu,np.:
              </p>
              <ul>
                <li>kotletów panierowanych, po wiedeńsku, mielonych.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Frytownica gastronomiczna</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Frytownicę należy ustawić na stabilnym podłożu i podłączyć do
                  zasilania.
                </li>
                <li>
                  Napełnić ją odpowiednią ilością tłuszczu – do zaznaczonego
                  miejsca w komorze roboczej.
                </li>
                <li>Włączyć frytownicę i ustawić odpowiednią temperaturę.</li>
                <li>
                  Półprodukt włożyć do ażurowego koszyka i zanurzyć w tłuszczu –
                  smażyć.
                </li>
                <li>
                  Po usmażeniu wyjąć koszyk z komory roboczej i ustawić na
                  specjalnych haczykach tak, aby nadmiar tłuszczu po smażeniu
                  ociekł do komory roboczej.
                </li>
                <li>Wyjąć usmażony produkt.</li>
                <li>Urządzenie należy ostudzić, a następnie dokładnie umyć.</li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>
                Frytownice gastronomiczne służą do smażenia w tłuszczu głębokim,
                takich produktów, jak
              </p>
              <ul>
                <li>kotlety de volaille;</li>
                <li>kotlety po wiedeńsku;</li>
                <li>mięsa nadziewane.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Grill</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Grille mogąbyć elektryczne, gazowe lub drzewne.</li>
                <li>
                  Grill elektryczny należy ustawić na stabilnym podłożu i
                  podłączyć do zasilania.
                </li>
                <li>
                  Posmarować płyty grzewcze tłuszczem przeznaczonym do smażenia
                  potraw.
                </li>
                <li>Włączyć grill specjalnym włącznikiem.</li>
                <li>
                  Ustawić docelową temperaturę smażenia. Gdy urządzenie nagrzeje
                  się do ustawionej temperatury, zasygnalizuje to specjalna
                  lampka.
                </li>
                <li>
                  Umieścić półprodukt na ryflowanych płytach do grillowania.
                </li>
                <li>
                  Nadmiar tłuszczu będzie spływał do specjalnego pojemnika.
                </li>
                <li>
                  Układając półprodukty, należy zwrócić uwagę, aby nie zagrodzić
                  „ujścia” tłuszczu.
                </li>
                <li>
                  W trakcie pracy urządzenia termostat reguluje temperaturę
                  smażenia, co sygnalizuje lampka kontrolna.
                </li>
                <li>
                  Podczas pracy należy uważać na spływający tłuszcz, gdyż można
                  się poparzyć.
                </li>
                <li>Obracać produkt za pomocą specjalnych szczypiec.</li>
                <li>
                  Po pracy należy zmniejszyć temperaturę i wyłączyć grill oraz
                  odczekaćaż ostygnie.
                </li>
                <li>
                  Ostudzone urządzenie należy następnie dokładnie umyć i
                  oczyścić.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Smażeniu na grillu można poddać:</p>
              <ul>
                <li>mięso.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Ruszt</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Ruszt paleniskowy umieszcza się nad paleniskiem z ogniem.
                </li>
                <li>
                  Smaruje tłuszczem przeznaczonym do przygotowania potrawy.
                </li>
                <li>Umieszcza na nim przygotowane półprodukty i smaży.</li>
                <li>Do przewracania półproduktu służą specjalne szczypce.</li>
                <li>
                  Po pracy ruszt należy zdjąć, gdy ogień już wygaśnie,
                  zachowując wszelkie środki ostrożności.
                </li>
                <li>
                  Urządzenie należy ostudzić, a następnie dokładnie umyć i
                  oczyścić.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Smażeniu na ruszcie można poddać:</p>
              <ul>
                <li>mięso.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Griddle grill</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Griddle grill należy umieścić na stabilnym podłożu i podłączyć
                  do zasilania.
                </li>
                <li>Nad urządzeniem musi być zainstalowany wyciąg.</li>
                <li>
                  Przed użyciem płytę należy nasmarować tłuszczem przeznaczonym
                  do sporządzenia potrawy.
                </li>
                <li>
                  Włączyć urządzenie i nastawić odpowiednią temperaturę, a gdy
                  płyty grzewcze osiągną właściwą temperaturę, zasygnalizuje to
                  lampka kontrolna.
                </li>
                <li>
                  Ułożyć na płycie półprodukt i smażyć z obu stron, używając
                  specjalnych szczypiec.
                </li>
                <li>
                  Po pracy wyłączyć urządzenie i ostudzone dokładnie umyć oraz
                  oczyścić.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Smażeniu na griddle grill możnapoddać:</p>
              <ul>
                <li>mięso;</li>
                <li>hamburgery.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Duszenie</td>
            <td className={classes.td}>Patelnia elektryczna</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Patelnię należy ustawić na stabilnym podłożu i podłączyć do
                  zasilania.
                </li>
                <li>
                  Komorę roboczą napełnić ilością tłuszczu wskazaną w
                  recepturze.
                </li>
                <li>Ustawić odpowiednią temperaturę komory roboczej.</li>
                <li>
                  Gdy tłuszcz osiągnie odpowiednią temperaturę, smażyć
                  półprodukty.
                </li>
                <li>
                  Następnie wlać odpowiednią ilość wywaru lub wody, przykryć
                  pokrywą i dusić potrawę.
                </li>
                <li>
                  Po pracy należy użyć specjalnego mechanizmu, który pomoże w
                  usunięciu pozostałości potrawy – najczęściej jest to pokrętło,
                  które zmienia nachylenie komory roboczej.
                </li>
                <li>
                  Urządzenie należy ostudzić, a następnie dokładnie umyć i
                  oczyścić.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>W patelni można dusić:</p>
              <ul>
                <li>pieczeń z sosem;</li>
                <li>zrazy nadziewane, bite, mielone;</li>
                <li>bigos;</li>
                <li>gulasze.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td rowSpan={'3'} className={classes.td}>
              Pieczenie
            </td>
            <td className={classes.td}>Piekarnik elektryczny</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Piekarnik ustawić na stabilnym podłożu.</li>
                <li>Podłączyć do zasilania.</li>
                <li>
                  Ustawić odpowiednią temperaturę i czas pracy komory roboczej.
                </li>
                <li>
                  Do nagrzanej komory roboczej wstawić przygotowany półprodukt i
                  piec.
                </li>
                <li>
                  Gdy urządzenie zakończy pracę, zasygnalizują to odpowiednie
                  czujniki i alarmy dźwiękowe.
                </li>
                <li>Po zakończonej pracy wyłączyć urządzenie.</li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Pieczeniu poddaje się:</p>
              <ul>
                <li>mięso.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Piec konwekcyjny</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Piec konwekcyjny ustawić na stabilnym podłożu.</li>
                <li>Podłączyć do zasilania.</li>
                <li>
                  Ustawić odpowiednią temperaturę i czas pracy komory roboczej –
                  można użyć wgranego programu.
                </li>
                <li>
                  Do nagrzanej komory roboczej wstawić przygotowany półprodukt i
                  piec.
                </li>
                <li>
                  Gdy urządzenie zakończy pracę, zasygnalizują to odpowiednie
                  czujniki i alarmy dźwiękowe.
                </li>
                <li>Po zakończonej pracy wyłączyć urządzenie.</li>
                <li>
                  Piec konwekcyjny ma wgrane programy myjąco-czyszczące. Po
                  zakończonej pracy należy uruchomić taki program.
                </li>
                <li>
                  Obróbka termiczna w piecu konwekcyjnym trwa krócej dzięki
                  wymuszonej konwekcji ciepłego powietrza.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Pieczeniu poddaje się:</p>
              <ul>
                <li>mięsa.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Rożen</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Urządzenie należy ustawić na stabilnym podłożu.</li>
                <li>Podłączyć do zasilania.</li>
                <li>Ustawić odpowiednią temperaturę oraz czas pracy rożna.</li>
                <li>
                  Umieścić półprodukt na specjalnych obrotowych prętach,
                  napędzanych silnikiem.
                </li>
                <li>Włączyć rożen.</li>
                <li>
                  Wytopiony podczas pracy tłuszcz gromadzi się na specjalnej
                  tacy.
                </li>
                <li>
                  Komora robocza zamykana jest drzwiami szklanymi, aby można
                  było obserwować pracę i stopień pieczenia produktu.
                </li>
                <li>
                  Po pracy należy odczekać aż urządzenie ostygnie i dokładnie je
                  umyć oraz oczyścić.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Pieczeniu na rożnie można poddać:</p>
              <ul>
                <li>mięso.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Opiekanie</td>
            <td className={classes.td}>Salamandry</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Urządzenie ustawić na stabilnym podłożu.</li>
                <li>Podłączyć do zasilania.</li>
                <li>Wybrać odpowiedni program pracy.</li>
                <li>Ustawić czas i temperaturę.</li>
                <li>Ułożyć na ażurowej tacce półprodukt i opiekać.</li>
                <li>
                  Po pracy urządzenie należy ostudzić, a następnie dokładnie
                  umyć i oczyścić.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Salamandry służą do opiekania:</p>
              <ul>
                <li>szaszłyków;</li>
                <li>kiełbasek.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td rowSpan={'4'} className={classes.td}>
              Urządzenia wielofunkcyjne do obróbki cieplnej
            </td>
            <td className={classes.td}>Trzon kuchenny: gazowy</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Trzon kuchenny jest podstawowym sprzętem w kuchni gorącej,
                  ponieważ z jego pomocą można przeprowadzać większość obróbki
                  termicznej.
                </li>
                <li>
                  Osoba pracująca przy trzonie kuchennym musi przejść
                  odpowiednie szkolenie BHP.
                </li>
                <li>
                  Przed przystąpieniem do pracy należy dokładnie sprawdzić, czy
                  wszystkie elementy są w odpowiednim miejscu (dotyczy to trzonu
                  kuchennego gazowego). Trzony kuchenne elektryczne i indukcyjne
                  nie mają kilku części palnika.
                </li>
                <li>
                  W przypadku trzonu gazowego, najpierw należy podpalić
                  zapalnik, tak zwany pilot płomienia, a następnie odkręcić
                  kurek z gazem i zaczekać aż się zapali.
                </li>
                <li>
                  Ustawić napełniony garnek lub inny drobny sprzęt kuchenny i
                  przeprowadzić obróbkę cieplną, dostosowując wielkość płomienia
                  do garnka z wykorzystaniem pokrętła regulującego dopływ gazu
                  tak, aby płomień nie wychodził poza garnek.
                </li>
                <li>
                  Po zakończeniu pracy wyłączyć dopływ gazu poprzez zakręcenie
                  kurka gazu i zdjąć garnek.
                </li>
                <li>Odczekać aż elementy trzonu ostygną i dokładniego umyć.</li>
                <li>
                  Trzony kuchenne – elektryczny i indukcyjny – nie posiadają
                  otwartego płomienia, aby z nich korzystać, należy włączyć je
                  specjalnym pokrętłem, następnie ustawić garnek.
                </li>
                <li>
                  Po pracy należy odłączyć zasilanie tym samym pokrętłem i zdjąć
                  garnek, a następnie dokładnie umyć urządzenie.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Trzon kuchenny służy do obróbki termicznej potraw:</p>
              <ul>
                <li>gotowanych;</li>
                <li>smażonych;</li>
                <li>duszonych.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Piec konwekcyjno-parowy</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Piec konwekcyjno-parowy ustawić na stabilnym podłożu.</li>
                <li>Podłączyć do zasilania.</li>
                <li>
                  Ustawić odpowiednią temperaturę i czas pracy komory roboczej –
                  można użyć wgranego programu.
                </li>
                <li>
                  Do nagrzanej komory roboczej wstawić przygotowany półprodukt i
                  przeprowadzić wskazaną obróbkę cieplną
                </li>
                <li>
                  Gdy urządzenie zakończy pracę, zasygnalizują to odpowiednie
                  czujniki i alarmy dźwiękowe.
                </li>
                <li>Po zakończonej pracy wyłączyć urządzenie.</li>
                <li>
                  Piece konwekcyjne posiadają wgrane programy myjąco-czyszczące,
                  wybrany należy uruchomić po zakończonej pracy.
                </li>
                <li>
                  W piecach konwekcyjnych obróbka termiczna trwa krócej poprzez
                  wymuszoną konwekcję ciepłego oraz nawilżonego powietrza.
                </li>
                <li>Stopień nawilżenia powietrza należy ustawić.</li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>
                Za pomocą pieców konwekcyjno-parowych można przeprowadzać
                następującą obróbkę termiczną:
              </p>
              <ul>
                <li>pieczenie;</li>
                <li>smażenie;</li>
                <li>grillowanie;</li>
                <li>gotowanie w parze;</li>
                <li>obróbkę łączoną – pieczenie w połączeniu z parowaniem.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Kuchenka mikrofalowa</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Kuchenkę mikrofalową należy ustawić na stabilnym podłożu i
                  podłączyć do sieci.
                </li>
                <li>Wstawić potrawę lub surowiec i zamknąć drzwiczki.</li>
                <li>Ustawić pokrętłami odpowiedni czas lub wybrany program.</li>
                <li>Uruchomić specjalnym włącznikiem.</li>
                <li>
                  Sygnał powiadomi o zakończeniu pracy kuchenki mikrofalowej.
                </li>
                <li>Otworzyć drzwiczki i wyjąć potrawę.</li>
                <li>Odłączyć urządzenie od zasilania.</li>
                <li>
                  Należy pamiętać, że do kuchenki mikrofalowej nie wolno
                  wstawiać metalowych naczyń, ani zastawy z metalowymi
                  zdobieniami.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              Kuchenka mikrofalowa służy do podgrzewania, rozmrażania oraz
              ewentualnie do gotowania delikatnych potraw.
            </td>
          </tr>

          <tr>
            <td className={classes.td}>Kuchenka indukcyjna</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Kuchenka indukcyjna jest stosowana w kuchni gorącej, ponieważ
                  z jej pomocą można przeprowadzać większość obróbki termicznej.
                </li>
                <li>
                  Kuchenka indukcyjna nie ma otwartego płomienia i aby z niej
                  korzystać, należy włączyć ją specjalnym pokrętłem, a następnie
                  ustawić garnek lub inny sprzęt i przeprowadzić obróbkę
                  cieplną.
                </li>
                <li>
                  Po pracy, tym samym pokrętłem, należy wyłączyć kuchenkę, zdjąć
                  garnek, a następnie dokładnie umyć urządzenie.
                </li>
                <li>
                  Kuchenka indukcyjna wymaga specjalnych naczyń do
                  przygotowywania potraw.
                </li>
                <li>
                  Urządzenie to jest energooszczędne i ogrzewa wyłącznie pole
                  wykorzystywanego sprzętu.
                </li>
              </ol>
            </td>
            <td className={classes.td}>
              <p>Kuchenka indukcyjna służy do obróbki termicznej potraw:</p>
              <ul>
                <li>gotowanych;</li>
                <li>smażonych;</li>
                <li>duszonych.</li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default KnowledgeBaseData05;
