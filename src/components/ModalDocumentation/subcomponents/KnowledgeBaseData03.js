import React from 'react';
import useStyles from '../styles';

const KnowledgeBaseData03 = () => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h3 className={classes.heading}>Rodzaje metod obróbki cieplnej</h3>
      <p className={classes.paragraph}>
        O wyborze metody obróbki cieplnej decydują cechy surowca, czyli:
      </p>
      <ul className={classes.description}>
        <li className={classes.paragraph}>
          grubość – im grubsze mięso, tym proces obróbki termicznej jest
          wolniejszy. Mięso pieczone czy smażone powinno być równej grubości,
          dzięki czemu unika się nierównomiernego przyrządzenia;
        </li>
        <li className={classes.paragraph}>
          zawartość tłuszczu pozwala na zabezpieczenie przed wyparowywaniem wody
          z mięsa;
        </li>
        <li className={classes.paragraph}>
          obecność kości – kości wydłużają czas obróbki cieplnej;
        </li>
        <li className={classes.paragraph}>
          charakter mięśni – obecność dużej ilości tkanki łącznej powoduje, że
          dane mięso nie nadaje się ani do smażenia, ani do pieczenia;
        </li>
        <li className={classes.paragraph}>
          sposób wykończenia półproduktów – ważna jest kontrola parametrów
          procesu obróbki cieplnej;
        </li>
        <li className={classes.paragraph}>
          czas procesu – czas potrzebny na obróbkę cieplną uzależniony jest od
          nośnika ciepła.
        </li>
      </ul>

      <table className={classes.table}>
        <thead>
          <tr>
            <th className={classes.th}>Rodzaj obróbki cieplnej</th>
            <th className={classes.th}>
              Cechy surowca przeznaczonego do danej obróbki cieplnej
            </th>
            <th className={classes.th}>Element gastronomiczny</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={classes.td}>Gotowanie w płynie</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso ze sztuk starszych ze średnią i dużą zawartością tkanki
                  łącznej
                </li>
                <li>Cienkie mięśnie poprzerastane tkanką łączną</li>
                <li>Mięsne masy mielone</li>
              </ol>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Wieprzowina: golonka, boczek, żeberka, karkówka, głowizna,
                  ryj, ucho, nogi, ogon
                </li>
                <li>
                  Wołowina: pręga, łata, rostbef, szponder, nogi, łopatka,
                  mostek
                </li>
                <li>Cielęcina: górka, goleń przednia i tylna, karkówka</li>
              </ol>
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Smażenie</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso z młodych sztuk, ze zwartych mięśni, bez tkanki łącznej
                </li>
                <li>Mięso rozdrobnione bez dodatków</li>
                <li>Mięso rozdrobnione z dodatkam</li>
              </ol>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Wieprzowina: schab, szynka, biodrówka, karkówka</li>
                <li>
                  Wołowina: polędwica, rostbef, skrzydło, zrazowa, antrykot z
                  młodych sztuk
                </li>
                <li>
                  Cielęcina: górka, udziec, nerkówka, mostek (po ugotowaniu)
                </li>
              </ol>
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Duszenie</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>Mięso zawierające średnią ilość tkanki łącznej</li>
                <li>Mięso z fragmentem kości</li>
                <li>Mięso ze zwartych mięśni</li>
              </ol>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Wieprzowina: żeberka, karkówka, łopatka, biodrówka, szynka,
                  schab
                </li>
                <li>
                  Wołowina: pręga, polędwica, łata, rostbef, łopatka, skrzydło,
                  zrazowa, antrykot, ligawa, krzyżowa, kark, mostek, rozbratel
                </li>
                <li>
                  Cielęcina: górka, karkówka, łopatka, mostek, łata, udziec,
                  nerkówka
                </li>
              </ol>
            </td>
          </tr>
          <tr>
            <td className={classes.td}>Pieczenie</td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Mięso formowane lub grube partie mięśni bez tkanki łącznej
                </li>
                <li>Mięso z młodych sztuk</li>
              </ol>
            </td>
            <td className={classes.td}>
              <ol className={classes.ol}>
                <li>
                  Wieprzowina: schab, szynka, łopatka, boczek, żeberka, karkówka
                </li>
                <li>
                  Wołowina: polędwica, rostbef, łopatka, zrazowa, skrzydło
                </li>
                <li>Cielęcina: udziec, łopatka, mostek</li>
              </ol>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default KnowledgeBaseData03;
