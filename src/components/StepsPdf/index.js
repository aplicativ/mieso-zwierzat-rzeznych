import React, { useContext } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import DataContext from '../../context/DataContext';

import './style.css';

import { dataTask6 } from '@data/features';

const useStyles = makeStyles((theme) => ({
  wrap: {
    fontSize: '30px',
    padding: '10px',
    // position:"absolute",
    // top:"0",
    // left:"0",
    // zIndex:-1
  },
  label: {
    margin: '20px 0 40px 0',
    paddingBottom: '10px',
    borderBottom: '3px solid #1e1c37',
    fontSize: '40px',
  },
  message: {
    color: 'white',
    borderRadius: '10px',
    textAlign: 'center',
    padding: '5px',
    position: 'relative',
    top: '-30px',
  },
  success: {
    backgroundColor: '#28a745',
  },
  error: {
    backgroundColor: '#dc3545',
  },
}));

const RenderTable = (type, rows) => {
  const { data, level, state, task4Columns } = useContext(DataContext);

  return (
    <table id="table-steps" className="third">
      <tr>
        <th>Element gastronomiczny</th>
        <th>Potrawa</th>
        <th>Metoda obróbki cieplnej</th>
      </tr>

      {[...Array(rows)].map((x, i) => (
        <tr key={i}>
          <td>
            {task4Columns[`content${type}Elements${i}`].list.length > 0
              ? task4Columns[`content${type}Elements${i}`].list[0].text
              : 'Brak odpowiedzi'}
          </td>
          <td>
            {task4Columns[`content${type}Dish${i}`].list.length > 0
              ? task4Columns[`content${type}Dish${i}`].list[0].text
              : 'Brak odpowiedzi'}
          </td>
          <td>
            {task4Columns[`content${type}Methods${i}`].list.length > 0
              ? task4Columns[`content${type}Methods${i}`].list[0].text
              : 'Brak odpowiedzi'}
          </td>
        </tr>
      ))}
    </table>
  );
};

const Steps = ({ type }) => {
  const classes = useStyles();
  const {
    isSecondTask,
    isThirdTask,
    isBeefMeat,
    isPorkMeat,
    isVealMeat,
    isSheepMeat,
    isDeerMeat,
    isRoeDeerMeat,
    isWildBoarMeat,
    isFifthTask,
    cullinaryElColumns,
    meatAndDishesColumns,
    task05Data,
    fifthTask,
    inputData,
    data,
    task4Columns,
    isFourTask,
    isSixTask,
  } = useContext(DataContext);

  const findItemInColumns = (keyList, columns) => {
    const columnItem = columns[keyList];
    const { list } = columnItem;
    return list.length > 0 ? list[0].text : '';
  };

  const findItemInColumnsArray = (keyList, columns) => {
    const columnItem = columns[keyList];
    const { list } = columnItem;
    const data = [];
    list.map((item) => data.push(item.text));
    return data;
  };

  const CheckValidation = () => {
    if (inputData.length === dataTask6.length) {
      let error = false;
      for (let i = 0; i < dataTask6.length; i++) {
        let filter = inputData.filter(
          (item) => item.key === dataTask6[i].key
        )[0];
        if (filter.value !== dataTask6[i].correctAnswer.toString()) {
          error = true;
        }
      }

      if (error) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  };

  return (
    <>
      <div
        className="pdf"
        style={{ display: 'none', width: '297mm' }}
        id="pdf-file"
      >
        <div className={classes.wrap}>
          <div className={classes.label}>Ćwiczenie 1</div>
          <table id="table-steps">
            <tr>
              <td>
                Jaką półtuszę przedstawia schemat? Nazwij elementy tej półtuszy.
              </td>
            </tr>
          </table>
          <table id="table-steps">
            <tr>
              <th>Półtusza</th>
              <th></th>
            </tr>
            <tr>
              <td>Półtusza wołowa</td>
              {isBeefMeat ? (
                <td>Rozwiązano prawidłowo</td>
              ) : (
                <td>Rozwiązano nieprawidłowo</td>
              )}
            </tr>
            <tr>
              <td>Półtusza wieprzowa</td>
              {isPorkMeat ? (
                <td>Rozwiązano prawidłowo</td>
              ) : (
                <td>Rozwiązano nieprawidłowo</td>
              )}
            </tr>
            <tr>
              <td>Półtusza cielęca</td>
              {isVealMeat ? (
                <td>Rozwiązano prawidłowo</td>
              ) : (
                <td>Rozwiązano nieprawidłowo</td>
              )}
            </tr>
            <tr>
              <td>Półtusza barania</td>
              {isSheepMeat ? (
                <td>Rozwiązano prawidłowo</td>
              ) : (
                <td>Rozwiązano nieprawidłowo</td>
              )}
            </tr>{' '}
            <tr>
              <td>Półtusza jelenia</td>
              {isDeerMeat ? (
                <td>Rozwiązano prawidłowo</td>
              ) : (
                <td>Rozwiązano nieprawidłowo</td>
              )}
            </tr>{' '}
            <tr>
              <td>Półtusza sarny</td>
              {isRoeDeerMeat ? (
                <td>Rozwiązano prawidłowo</td>
              ) : (
                <td>Rozwiązano nieprawidłowo</td>
              )}
            </tr>{' '}
            <tr>
              <td>Półtusza dzika</td>
              {isWildBoarMeat ? (
                <td>Rozwiązano prawidłowo</td>
              ) : (
                <td>Rozwiązano nieprawidłowo</td>
              )}
            </tr>
          </table>

          <div className={classes.label}>Ćwiczenie 2</div>
          <table id="table-steps">
            <tr>
              <td>
                Wpisz pod rysunkami nazwy elementów kulinarnych półtuszy
                wieprzowej i podaj ich zastosowanie w technologii
                gastronomicznej.
              </td>
            </tr>
          </table>

          <table className="third" id="table-steps">
            <tr>
              <th>Nazwa</th>
              <th>Element</th>
              <th>Zastosowanie</th>
            </tr>

            <tr>
              <td>Boczek</td>
              <td>{findItemInColumns('content18', cullinaryElColumns)}</td>
              <td>{findItemInColumns('content19', cullinaryElColumns)}</td>
            </tr>
            <tr>
              <td>Łopatka</td>
              <td>{findItemInColumns('content20', cullinaryElColumns)}</td>
              <td>{findItemInColumns('content21', cullinaryElColumns)}</td>
            </tr>
            <tr>
              <td>Golonka przednia</td>
              <td>{findItemInColumns('content22', cullinaryElColumns)}</td>
              <td>{findItemInColumns('content23', cullinaryElColumns)}</td>
            </tr>
            <tr>
              <td>Biodrówka</td>
              <td>{findItemInColumns('content24', cullinaryElColumns)}</td>
              <td>{findItemInColumns('content25', cullinaryElColumns)}</td>
            </tr>

            <tr>
              <td>Szynka</td>
              <td>{findItemInColumns('content26', cullinaryElColumns)}</td>
              <td>{findItemInColumns('content27', cullinaryElColumns)}</td>
            </tr>
          </table>

          {isSecondTask ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}
          <div className={classes.label}>Ćwiczenie 3</div>
          <table id="table-steps">
            <tr>
              <td>
                Do podanych potraw przyporządkuj odpowiedni asortyment mięsa.
              </td>
            </tr>
          </table>

          <table className="third" id="table-steps">
            <tr>
              <th>Potrawa</th>
              <th>Asortyment</th>
            </tr>
            <tr>
              <td>Mięso z rosołu</td>
              <td>
                {findItemInColumnsArray('thirdTask1', meatAndDishesColumns).map(
                  (item) => (
                    <div>{item}</div>
                  )
                )}
              </td>
            </tr>
            <tr>
              <td>Rumsztyk</td>
              <td>
                {findItemInColumnsArray('thirdTask2', meatAndDishesColumns).map(
                  (item) => (
                    <div>{item}</div>
                  )
                )}
              </td>
            </tr>
            <tr>
              <td>Medalion</td>
              <td>
                {findItemInColumnsArray('thirdTask3', meatAndDishesColumns).map(
                  (item) => (
                    <div>{item}</div>
                  )
                )}
              </td>
            </tr>
            <tr>
              <td>Stek</td>
              <td>
                {findItemInColumnsArray('thirdTask4', meatAndDishesColumns).map(
                  (item) => (
                    <div>{item}</div>
                  )
                )}
              </td>
            </tr>
            <tr>
              <td>Boeuf Strogonow</td>
              <td>
                {findItemInColumnsArray('thirdTask5', meatAndDishesColumns).map(
                  (item) => (
                    <div>{item}</div>
                  )
                )}
              </td>
            </tr>
          </table>

          {isThirdTask ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}

          <div className={classes.label}>Ćwiczenie 4</div>
          <table id="table-steps">
            <tr>
              <td>
                Z menu bocznego wybierz elementy gastronomiczne półtuszy
                wieprzowej, wołowej lub cielęcej (np. boczek czy górkę), a
                następnie zaplanuj potrawę z zastosowaniem danego elementu,
                uwzględniając również właściwą metodę obróbki cieplnej (np.
                gotowanie czy smażenie).
              </td>
            </tr>
          </table>

          <h3 className={classes.mainTitle}>Półtusza wieprzowa</h3>
          {RenderTable('Pork', 6)}

          <h3 className={classes.mainTitle}>Półtusza wołowa</h3>
          {RenderTable('Beef', 5)}

          <h3 className={classes.mainTitle}>Półtusza cielęca</h3>
          {RenderTable('Veal', 3)}

          {isFourTask ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}

          <div className={classes.label}>Ćwiczenie 5</div>
          <table id="table-steps">
            <tr>
              <td>
                Wykonaj korektę błędnych kompozycji przedstawionych w tabeli,
                przesuwając poszczególne elementy gastronomiczne tak, aby
                odpowiadały właściwej potrawie.
              </td>
            </tr>
          </table>

          <table className="third" id="table-steps">
            {task05Data.map((data, index) => {
              return (
                <>
                  <thead key={index}>
                    <tr>
                      <th align="left">{data.first}</th>
                      <th>{data.second}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {data.items.map((dish, index) => {
                      if (data.type === dish.type) {
                        return (
                          <tr key={index}>
                            <td>{dish.dish}</td>
                            {fifthTask.map((item) => {
                              if (item.key === dish.id) {
                                return <td>{item.chosen}</td>;
                              }
                            })}
                          </tr>
                        );
                      }
                    })}
                  </tbody>
                </>
              );
            })}
          </table>

          {isFifthTask ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}

          <div className={classes.label}>Ćwiczenie 6</div>
          <table id="table-steps">
            <tr>
              <td>
                Oceń prawdziwość podanych zdań. Zaznacz Prawda, jeśli zdanie
                jest prawdziwe, lub Fałsz – jeśli jest fałszywe. Pamiętaj, że w
                dolnym menu znajdziesz również ikonę, po kliknięciu na którą
                będziesz mógł wrócić do wiedzy dostępnej w innych multimediach
                e-materiału oraz skorzystać z Bazy wiedzy, w której znajdziesz
                charakterystykę elementów gastronomicznych mięsa zwierząt
                rzeźnych, metody obróbki cieplnej, bazę potraw oraz urządzeń do
                obróbki cieplnej.
              </td>
            </tr>
          </table>

          <table className="third" id="table-steps">
            <tr>
              <th>Pytanie</th>
              <th>Odpowiedz</th>
            </tr>
            {inputData.map((item, index) => (
              <tr key={index}>
                <td>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: dataTask6.filter(
                        (input) => input.key === item.key
                      )[0].title,
                    }}
                  />
                </td>
                <td>{item.value === 'true' ? 'Prawda' : 'Fałsz'}</td>
              </tr>
            ))}
          </table>

          {isSixTask ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Steps;
