import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Validation from "@components/Validation";

import DataContext from "@context/DataContext";
import ModalContext from "@context/ModalContext";

import BeefMeatSchema from "./subcomponents/beefMeat/beefMeat";
import PorkMeatSchema from "./subcomponents/porkMeat/porkMeat";
import VealMeatSchema from "./subcomponents/vealMeat/vealMeat";
import SheepMeatSchema from "./subcomponents/sheepMeat/sheepMeat";
import DeerMeatSchema from "./subcomponents/deerMeat/deerMeat";
import RoeDeerMeatSchema from "./subcomponents/roeDeerMeat/roeDeerMeat";
import WildBoarMeatSchema from "./subcomponents/wildBoarMeat/wildBoarMeat";
import MealsTable from "./subcomponents/MealsTable.js";
import Task05 from "./subcomponents/Task05";

import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";

import Table from "@components/Table";
import Task06 from "./Task06";
import Task04 from "./Task04";
const useStyles = makeStyles({
  content: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    paddingLeft: "10px",
    margin: "30px auto",
    width: "97%",
    minHeight: "300px",
    maxWidth: "1100px",
  },
});

const Content = () => {
  const classes = useStyles();
  const { data, level } = useContext(DataContext);
  const { setIsInstructionOpen } = useContext(ModalContext);
  // const { product } = data;

  return (
    <>
      <Stack justifyContent="center">
        <Button
          style={{
            width: "97%",
            maxWidth: "700px",
            margin: "20px auto 20px auto",
          }}
          onClick={() => setIsInstructionOpen(true)}
          variant="outlined"
        >
          Polecenie
        </Button>
      </Stack>
      <div className={classes.content}>
        {level === "easy" && (
          <>
            {data.typeOfMeat === "beefMeat" ? <BeefMeatSchema /> : null}
            {data.typeOfMeat === "porkMeat" ? <PorkMeatSchema /> : null}
            {data.typeOfMeat === "vealMeat" ? <VealMeatSchema /> : null}{" "}
            {data.typeOfMeat === "sheepMeat" ? <SheepMeatSchema /> : null}
            {data.typeOfMeat === "deerMeat" ? <DeerMeatSchema /> : null}
            {data.typeOfMeat === "roeDeerMeat" ? <RoeDeerMeatSchema /> : null}
            {data.typeOfMeat === "wildBoarMeat" ? <WildBoarMeatSchema /> : null}
          </>
        )}
        {level === "average" && <Table />}
        {level === "hard" && <MealsTable />}
        {level === "task05" && <Task05 />}
        {level === "task04" && <Task04 />}
        {level === "task06" && <Task06 />}
      </div>
      <div className={classes.content}>
        <Validation />
      </div>
    </>
  );
};

export default Content;
