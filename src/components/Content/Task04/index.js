import React, { useContext, useState } from "react";
import DragDropItem from "@components/DragDropTransfer/subcomponents/item-task04-content";
import { makeStyles } from "@material-ui/styles";

import "./style.css";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import FindInPageIcon from "@mui/icons-material/FindInPage";

import Pork from "@data/task04/img/półtusza-wieprzowa.png";
import Beef from "@data/task04/img/półtusza-wołowa.png";
import Veal from "@data/task04/img/półtusza-cieleca.png";

import DataContext from "@context/DataContext";

const useStyles = makeStyles({
  content: {
    color: "black",
    display: "flex",
    flexDirection: "column",
    textAlign: "left",
    fontSize: "18px",
    width: "100%",
  },
  task: {
    margin: "30px 0px",
  },
  description: {
    fontSize: "16px",
  },
  title: {
    fontSize: "18px",
    fontWeight: 700,
  },
  mainTitle: {
    fontSize: "18px",
    fontWeight: 700,
    textAlign: "center",
  },
  link: {
    textDecoration: "underline",
    fontSize: "18px",
    textAlign: "center",
    cursor: "pointer",

    "&:hover": {
      color: "#aaa",
    },
  },
  preview: {
    textAlign: "center",
    marginBottom: "10px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "#2d81ff",
    textDecoration: "none",
    "&:hover": {
      textDecoration: "underline",
      cursor: "pointer",
    },
  },
});

const RenderTable = (type, rows) => {
  return (
    <table id="table">
      <tr>
        <th>Element gastronomiczny</th>
        <th>Potrawa</th>
        <th>Metoda obróbki cieplnej</th>
      </tr>

      {[...Array(rows)].map((x, i) => (
        <tr>
          <td>
            <DragDropItem keyData={`content${type}Elements${i}`} />
          </td>
          <td>
            <DragDropItem keyData={`content${type}Dish${i}`} />
          </td>
          <td>
            <DragDropItem keyData={`content${type}Methods${i}`} />
          </td>
        </tr>
      ))}
    </table>
  );
};

const renderImg = (imgType, state, setState, getImagePath) => {
  const handleClose = () => {
    setState(false);
  };

  console.log(imgType);

  return (
    <div>
      <Dialog
        fullWidth
        open={state}
        onClose={handleClose}
        style={{ zIndex: "999999" }}
        maxWidth="lg"
      >
        <DialogContent style={{ padding: "25px 40px" }}>
          <img style={{ width: "100%" }} src={getImagePath(imgType)}></img>
          <div style={{ position: "absolute", top: "15px", right: "15px" }}>
            <IconButton onClick={() => setState(false)} aria-label="Example">
              <CloseIcon />
            </IconButton>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export const Task04 = () => {
  const { getImagePath } = useContext(DataContext);
  const [isImageOpen, setIsImageOpen] = useState(false);
  const [image, setImage] = useState(null);
  const classes = useStyles();

  const OpenImage = (type) => {
    setImage(type);
    setIsImageOpen(true);
  };

  return (
    <div className={classes.content}>
      {renderImg(image, isImageOpen, setIsImageOpen, getImagePath)}
      <h3 className={classes.mainTitle}>Półtusza wieprzowa</h3>
      <p onClick={() => OpenImage(Pork)} className={classes.preview}>
        Zobacz podgląd <FindInPageIcon />
      </p>

      {RenderTable("Pork", 6)}

      <h3 className={classes.mainTitle}>Półtusza wołowa</h3>
      <p onClick={() => OpenImage(Beef)} className={classes.preview}>
        Zobacz podgląd <FindInPageIcon />
      </p>

      {RenderTable("Beef", 5)}

      <h3 className={classes.mainTitle}>Półtusza cielęca</h3>
      <p onClick={() => OpenImage(Veal)} className={classes.preview}>
        Zobacz podgląd <FindInPageIcon />
      </p>

      {RenderTable("Veal", 3)}
    </div>
  );
};

export default Task04;
