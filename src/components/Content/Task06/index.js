import React, { useEffect, useContext } from "react";
import { dataTask6 } from "@data/features";
import Box from "@mui/material/Box";
import { makeStyles } from "@material-ui/styles";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import DataContext from "@context/DataContext";

const useStyles = makeStyles({
  content: {
    color: "black",
    display: "flex",
    flexDirection: "column",
    textAlign: "left",
  },
  task: {
    margin: "30px 0px",
  },
  description: {
    fontSize: "16px",

    "& > ul > li": {
      fontFamily: ["OpenSans", "sans-serif"].join(","),
    },
  },
  title: {
    fontSize: "18px",
    fontWeight: 700,

    "& > ul > li": {
      fontFamily: ["OpenSans", "sans-serif"].join(","),
    },
  },
});

const Task06 = () => {
  const classes = useStyles();
  const { inputData, setInputData } = useContext(DataContext);

  const handleChange = (key, value) => {
    const filter = inputData.filter((item) => item.key === key);

    if (filter.length > 0) {
      setInputData(
        inputData.map((item) =>
          item.key === key ? { ...item, value: value } : item
        )
      );
    } else {
      setInputData([...inputData, { key: key, value: value }]);
    }
  };

  useEffect(() => {}, [inputData, setInputData]);

  return (
    <Box className={classes.content}>
      {dataTask6.map((item) => (
        <Box className={classes.task}>
          <Box
            sx={{ fontFamily: ["OpenSans", "sans-serif"].join(",") }}
            className={classes.title}
            dangerouslySetInnerHTML={{ __html: item.title }}
          />
          <RadioGroup
            aria-labelledby="demo-controlled-radio-buttons-group"
            onChange={(e) => handleChange(item.key, e.target.value)}
            value={
              inputData.filter((input) => input.key === item.key).length !== 0
                ? inputData.filter((input) => input.key === item.key)[0].value
                : null
            }
          >
            <FormControlLabel value={true} control={<Radio />} label="Prawda" />
            <FormControlLabel value={false} control={<Radio />} label="Fałsz" />
          </RadioGroup>

          {inputData.filter((input) => input.key === item.key).length !== 0 && (
            <Box
              className={classes.description}
              sx={
                inputData.filter((input) => input.key === item.key)[0].value ===
                item.correctAnswer.toString()
                  ? {
                      color: "green",
                      fontFamily: ["OpenSans", "sans-serif"].join(","),
                    }
                  : {
                      color: "red",
                      fontFamily: ["OpenSans", "sans-serif"].join(","),
                    }
              }
              dangerouslySetInnerHTML={{
                __html:
                  inputData.filter((input) => input.key === item.key)[0]
                    .value === "true"
                    ? item.descriptionTrue
                    : item.descriptionFalse,
              }}
            />
          )}
        </Box>
      ))}
    </Box>
  );
};

export default Task06;
