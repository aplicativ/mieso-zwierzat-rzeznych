import React from "react";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { makeStyles } from "@material-ui/core/styles";

import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import RotateLeftIcon from "@material-ui/icons/RotateLeft";

const Zoom = ({ children }) => {
  const useStyles = makeStyles({
    tools: {
      display: "flex",
      justifyContent: "end",
      alignItems: "center",
      flexDirection: "column",
    },
    contentActionToolsItem: {
      "&:hover": {
        background: "orange",
      },
      display: "block",
      background: "purple",
      color: "white !important",
      margin: "10px",
    },
    info: {
      display: "block",
      width: "300px",
      fontSize: "10pt",
      textAlign: "left",
      padding: "20px",
      float: "right",
      border: "1px solid lightgrey",
    },
    zoomer: { display: "flex" },
  });

  const classes = useStyles();

  return (
    <>
      <TransformWrapper
        initialScale={1}
        initialPositionX={0}
        initialPositionY={0}
      >
        {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
          <React.Fragment>
            <div className={classes.zoomer}>
              <div className={classes.tools}>
                <IconButton
                  onClick={() => zoomIn()}
                  aria-label="delete"
                  className={classes.contentActionToolsItem}
                >
                  <AddIcon fontSize="small" htmlColor="white" />
                </IconButton>
                <IconButton
                  onClick={() => zoomOut()}
                  aria-label="delete"
                  className={classes.contentActionToolsItem}
                >
                  <RemoveIcon fontSize="small" htmlColor="white" />
                </IconButton>
                <IconButton
                  onClick={() => resetTransform()}
                  aria-label="delete"
                  className={classes.contentActionToolsItem}
                >
                  <RotateLeftIcon fontSize="small" htmlColor="white" />
                </IconButton>
              </div>
              <TransformComponent>{children}</TransformComponent>
            </div>
          </React.Fragment>
        )}
      </TransformWrapper>
    </>
  );
};

export default Zoom;
