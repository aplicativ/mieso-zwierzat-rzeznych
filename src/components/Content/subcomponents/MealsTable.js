import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DragDropItem from "@components/DragDropTransfer/subcomponents/item-average-hard";

const useStyles = makeStyles({
  content: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    padding: "10px",
    margin: "30px auto",
    width: "97%",
    maxWidth: "1100px",
  },
  table: {
    maxWidth: "650px",
    width: "100%",
    borderCollapse: "collapse",
    borderSpacing: "0",
    marginBottom: "1%",
    color: "black",
    fontSize: 15,
  },
  meatInfo: {
    display: "flex",
    "& ul": {
      margin: "10px",
    },
  },
});
export default function MealsTable() {
  const classes = useStyles();
  return (
    <>
      <table className={classes.table} border="1">
        <tbody>
          <tr>
            <td style={{ width: "50%" }}>
              <strong>Potrawa</strong>
            </td>
            <td style={{ width: "50%" }}>
              <strong>Asortyment mięsa</strong>
            </td>
          </tr>
          <tr>
            <td style={{ textAlign: "center" }}>Mięso z rosołu:</td>
            <td>
              <DragDropItem keyData="thirdTask1" />
            </td>
          </tr>
          <tr>
            <td style={{ textAlign: "center" }}>Rumsztyk:</td>
            <td>
              <DragDropItem keyData="thirdTask2" />
            </td>
          </tr>
          <tr>
            <td style={{ textAlign: "center" }}>Medalion:</td>
            <td>
              <DragDropItem keyData="thirdTask3" />
            </td>
          </tr>
          <tr>
            <td style={{ textAlign: "center" }}>Stek:</td>
            <td>
              <DragDropItem keyData="thirdTask4" />
            </td>
          </tr>
          <tr>
            <td style={{ textAlign: "center" }}>Boeuf Strogonow:</td>
            <td>
              <DragDropItem keyData="thirdTask5" />
            </td>
          </tr>
        </tbody>
      </table>
    </>
  );
}
