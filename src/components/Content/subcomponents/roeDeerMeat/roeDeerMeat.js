import React, { useContext } from 'react';
import { Droppable } from 'react-beautiful-dnd';

import DataContext from '@context/DataContext';
import './../index.css';
import './style.css';
import { makeStyles } from '@material-ui/styles';
import CloseIcon from '@mui/icons-material/Close';
import Zoom from '../zoom';

const useStyles = makeStyles({
  droppable: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: 'white',
    fontSize: '29px',
  },
});

const SVGDropable = ({ keyData }) => {
  const classes = useStyles();
  const { columnsInTasks, setColumnsInTasks } = useContext(DataContext);
  const { [keyData]: data } = columnsInTasks;

  const RemoveValue = () => {
    columnsInTasks.menu.list.splice(
      parseInt(columnsInTasks[keyData].list[0].key) - 1,
      0,
      columnsInTasks[keyData].list[0]
    );

    setColumnsInTasks((prevState) => ({
      ...prevState,
      [keyData]: {
        ...prevState[keyData],

        list: [],
      },
    }));

    setColumnsInTasks((prevState) => ({
      ...prevState,
      menu: {
        ...prevState.menu,
        list: [...prevState.menu.list],
      },
    }));
  };

  return (
    <Droppable droppableId={data.id}>
      {(provided) => {
        return (
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: '0 20px',
            }}
            xmlns="http://www.w3.org/1999/xhtml"
            ref={provided.innerRef}
            className={`dragable ${classes.droppable}`}
            columnID={data.id}
          >
            <div>{data.list.length > 0 && data.list[0].text}</div>
            {data.list.length > 0 && data.list[0].text && (
              <CloseIcon onClick={() => RemoveValue()} />
            )}
          </div>
        );
      }}
    </Droppable>
  );
};

export default function RoeDeerMeat() {
  return (
    <div style={{ border: '5px dashed #1C6EA4' }}>
      <Zoom>
        <div style={{ width: '1000px', height: '600px', paddingTop: '50px' }}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 1783.276 841.89"
            className="svg"
            style={{ height: '90wh' }}
          >
            <defs></defs>
            <g id="e64dba1a-f4a0-4187-8bb1-8552077007c2" data-name="Warstwa 4">
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="642.333"
                y1="281.333"
                x2="636.61"
                y2="283.134"
              />
              <line
                class="ad0702ef-3d53-447e-bbb2-ed1bf3167d0a"
                x1="624.76"
                y1="286.861"
                x2="369.982"
                y2="367.002"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="364.057"
                y1="368.866"
                x2="358.333"
                y2="370.667"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="47.109"
                y="342.485"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(167.304 381.471)"
        >
          Szyja
        </text> */}
              <foreignObject x="47.109" y="342.485" width="296" height="63.333">
                <SVGDropable keyData="contentRoeDeer1"></SVGDropable>
              </foreignObject>
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="757"
                y1="382.667"
                x2="751.079"
                y2="383.637"
              />
              <line
                class="bba82c86-2c52-4b52-b250-250bbec57292"
                x1="739.357"
                y1="385.558"
                x2="370.115"
                y2="446.069"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="364.254"
                y1="447.03"
                x2="358.333"
                y2="448"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="47.109"
                y="419.818"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(150.402 458.805)"
        >
          Łopatka
        </text> */}
              <foreignObject x="47.109" y="419.818" width="296" height="63.333">
                <SVGDropable keyData="contentRoeDeer8"></SVGDropable>
              </foreignObject>
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="757"
                y1="460"
                x2="751.079"
                y2="460.97"
              />
              <line
                class="bba82c86-2c52-4b52-b250-250bbec57292"
                x1="739.357"
                y1="462.891"
                x2="370.115"
                y2="523.402"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="364.254"
                y1="524.363"
                x2="358.333"
                y2="525.333"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="47.109"
                y="497.152"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(109.111 536.138)"
        >
          Goleń przednia
        </text> */}
              <foreignObject x="47.109" y="497.152" width="296" height="63.333">
                <SVGDropable keyData="contentRoeDeer7"></SVGDropable>
              </foreignObject>
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="898.333"
                y1="392"
                x2="893.748"
                y2="395.87"
              />
              <line
                class="a5333f7f-35c0-4641-a908-48c0838889a6"
                x1="884.847"
                y1="403.383"
                x2="622.257"
                y2="625.009"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="617.807"
                y1="628.766"
                x2="613.221"
                y2="632.635"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="301.997"
                y="604.454"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(408.907 643.44)"
        >
          Mostek
        </text> */}
              <foreignObject
                x="301.997"
                y="604.454"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentRoeDeer6"></SVGDropable>
              </foreignObject>
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="887.667"
                y1="129.333"
                x2="883.295"
                y2="133.443"
              />
              <line
                class="e42b217a-daae-4ad8-8cb2-967d1ab26d99"
                x1="874.989"
                y1="141.25"
                x2="762.858"
                y2="246.653"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="758.705"
                y1="250.557"
                x2="754.333"
                y2="254.667"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="745.775"
                y="54.485"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(850.351 93.471)"
        >
          Karczek
        </text> */}
              <foreignObject x="745.775" y="54.485" width="296" height="63.333">
                <SVGDropable keyData="contentRoeDeer2"></SVGDropable>
              </foreignObject>
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="1205.558"
                y1="129.333"
                x2="1199.95"
                y2="131.467"
              />
              <line
                class="ae8295e9-5ab7-4e53-b0fa-4af9bd9e057d"
                x1="1188.703"
                y1="135.745"
                x2="946.898"
                y2="227.728"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="941.275"
                y1="229.867"
                x2="935.667"
                y2="232"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="1063.667"
                y="54.485"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(1168.243 93.471)"
        >
          Comber
        </text> */}
              <foreignObject
                x="1063.667"
                y="54.485"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentRoeDeer3"></SVGDropable>
              </foreignObject>
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="641.639 271.254 647.677 289.017 660.183 274.104 641.639 271.254"
              />
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="760.031 263.092 746.88 249.71 742.087 268.573 760.031 263.092"
              />
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="938.104 241.29 931.277 223.814 919.451 239.271 938.104 241.29"
              />
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="1197.764 289.755 1189.559 272.882 1179.009 289.238 1197.764 289.755"
              />
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="1151.097 466.755 1142.892 449.882 1132.342 466.238 1151.097 466.755"
              />
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="893.701 384.261 906.022 398.41 911.942 379.87 893.701 384.261"
              />
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="757.993 373.286 762.455 391.509 776.217 377.747 757.993 373.286"
              />
              <polygon
                class="ee6f2741-c5e0-44f3-860b-a3cf8e5304e0"
                points="757.327 449.952 761.788 468.176 775.55 454.414 757.327 449.952"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="1195.499"
                y1="280.326"
                x2="1200.918"
                y2="277.749"
              />
              <line
                class="ff657c18-60e6-4821-afe3-50dafb0016a6"
                x1="1211.711"
                y1="272.615"
                x2="1508.517"
                y2="131.442"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="1513.913"
                y1="128.875"
                x2="1519.332"
                y2="126.298"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="1380.44"
                y="54.45"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(1490.566 93.435)"
        >
          Udziec
        </text> */}
              <foreignObject x="1380.44" y="54.45" width="296" height="63.333">
                <SVGDropable keyData="contentRoeDeer4"></SVGDropable>
              </foreignObject>
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="1149"
                y1="457.333"
                x2="1154.576"
                y2="455.118"
              />
              <line
                class="a0b6dd77-70dd-4354-a9c7-0b51fee3ea5e"
                x1="1165.46"
                y1="450.793"
                x2="1508.314"
                y2="314.574"
              />
              <line
                class="bec8aac0-7d90-49fe-a94a-2a2476dcbee8"
                x1="1513.756"
                y1="312.412"
                x2="1519.332"
                y2="310.196"
              />
              <rect
                class="bc1a2851-9c53-421a-a8d2-84af22a48aeb"
                x="1380.44"
                y="238.348"
                width="296"
                height="63.333"
              />
              {/* <text
          class="f297bfa0-3f5a-4e38-9850-bc39c5da4bf2"
          transform="translate(1464.93 277.333)"
        >
          Goleń tylna
        </text> */}
              <foreignObject
                x="1380.44"
                y="238.348"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentRoeDeer5"></SVGDropable>
              </foreignObject>
              <path
                class="ad1fd6de-c314-4c3e-8948-9a062d9ca34e"
                d="M554.023,252.321c-15.665-5.974-33.275-.354-48.746,6.082s-31.73,13.886-48.358,11.414-31.8-20.284-24.257-34.946c29.09-23.31,60.089-49.663,66.892-85.781,6.687-35.5,141.265-58.579,152.546-101.83,13.011,23.559,2.327,56.641-22.178,68.673-10.189,5-24.208,9.34-24.643,20.459l52.414,55.143c17.967,18.9,38.63,39.111,65,40.728,21.292,1.306,40.924-9.951,60.175-18.92,80.482-37.5,173.16-38.621,262.33-36.741,48.1,1.015,96.846,2.795,142.871,16.475s89.719,40.755,113.17,81.733c14.353,25.081,20.441,53.9,26.659,81.754a21.3,21.3,0,0,1-10.782,23.682c-12.64,6.471-22.854-4.3-24.97-15.124-1.648-8.429.223-17.077.807-25.64a86.942,86.942,0,0,0-20.131-61.243,428.294,428.294,0,0,0-6.729,107.57c.6,9.331,1.721,19.215,7.615,26.588,12.27,15.35,37.19,11.79,54.68,21.163,23.085,12.373,26.928,42.437,29.638,67.993a635.67,635.67,0,0,0,54.8,198.709c4.305,9.34,9,19.383.167,30.107a28.951,28.951,0,0,1-13.619,9.036c-11.817,3.745-24.3,1.371-36.291-1,19.836-32.547,9.991-73.853-.416-110.331q-15.7-55.008-31.39-110.017c-2.14-7.5-4.4-15.239-9.477-21.248-7.035-8.335-18.19-11.94-28.568-15.692a322.932,322.932,0,0,1-139.29-98.155,133.218,133.218,0,0,0,34.185,83.078c15.749,17.413,37.743,34.58,35.824,57.724-.822,9.917-6.168,18.858-11.381,27.409L1099.606,736.448c-1.515,2.485-5.871,4.728-6.593,1.925q.983,9.285,1.966,18.57l-42.364-2.249.07-21.354c28.359-6.58,45.584-33.934,59.093-59.13q37.685-70.284,69.385-143.423a334,334,0,0,1-119.187-120.268q-68.466,11.058-137.727,16.425c-12.186,94.9,5.941,193.241,50.939,278.183a15.8,15.8,0,0,1-9.582,22.439h0l.013,32.536a94.906,94.906,0,0,1-46.258,3.776l.224-21.845c10.909-1.7,18.139-12.8,19-23.546s-3.106-21.225-6.831-31.369a923.412,923.412,0,0,1-55.191-267.13L805,428.008c-13.511,57.088-57.778,102.8-76.894,158.344-10.423,30.284-68.921,171.424-68.921,171.424s-10.722,2.677-7.68.377l-3.9,16.552a137.418,137.418,0,0,1-30.1.286c-.866-11.957-.671-9.269-1.537-21.225,30.168-8.059,43.764-42.5,52.5-72.216l23.658-80.445c2.34-7.955,4.7-16.232,3.32-24.4-1.121-6.623-4.662-12.778-5.025-19.481-.552-10.172,6.169-19.158,12.116-27.531,29.491-41.519,33.64-119.7,33.64-119.7C700.362,392.438,569.164,288.525,554.023,252.321Z"
              />
              <path d="M1089.924,451.489a94.34,94.34,0,0,0,66.657-57.092c1.425-3.589-4.376-5.147-5.786-1.595-10.592,26.685-34.511,46.7-62.466,52.9-3.768.836-2.173,6.622,1.6,5.786Z" />
              <path d="M737.72,407.956c-2.3-17.5.76-34.9,7.009-51.3,5.574-14.635,13.589-31.2,27.852-39.122,14.01-7.779,32.406-4.327,43.737,6.6,12.032,11.6,15.573,28.962,11.959,44.892-2.278,10.042-6.525,19.606-10.653,29q-6.414,14.588-13.717,28.756c-1.76,3.429,3.415,6.468,5.181,3.028q7.49-14.593,14.092-29.614c4.076-9.317,8.274-18.753,10.665-28.67,4.073-16.891,1.166-34.84-10.219-48.339a44.98,44.98,0,0,0-44.252-14.663c-17.157,4.133-27.978,19.327-35.207,34.387-9.682,20.171-15.394,42.624-12.447,65.051.5,3.773,6.5,3.823,6,0Z" />
              <path d="M735.813,411.96a79.008,79.008,0,0,0,62.967,29.331c3.856-.081,3.869-6.081,0-6a73.342,73.342,0,0,1-58.724-27.574c-2.413-3.014-6.634,1.256-4.243,4.243Z" />
              <path d="M579.679,284.478c10.592.929,18.4-9.751,25-16.514,7.771-7.952,15.374-16.09,22.348-24.758,13.478-16.751,24.56-35.554,28.445-56.943.686-3.775-5.1-5.4-5.786-1.595-3.6,19.807-13.508,37.384-25.891,53.03-6.061,7.658-12.643,14.891-19.4,21.936-3.365,3.508-6.779,6.969-10.207,10.415-4.007,4.027-8.319,8.972-14.514,8.429-3.846-.338-3.823,5.664,0,6Z" />
              <path d="M787.626,213.333a833.917,833.917,0,0,0,5.618,96.089c.442,3.79,6.447,3.835,6,0a833.917,833.917,0,0,1-5.618-96.089c0-3.86-6-3.867-6,0Z" />
              <path d="M711.162,235.116a394.7,394.7,0,0,1,31.991,109.537c.536,3.807,6.318,2.187,5.786-1.6a399.739,399.739,0,0,0-32.6-110.971c-1.607-3.5-6.78-.46-5.18,3.029Z" />
              <path d="M827.666,334.961q113.038-2.968,225.976-8.806c3.842-.2,3.866-6.2,0-6q-112.929,5.779-225.976,8.806c-3.854.1-3.869,6.1,0,6Z" />
              <path d="M1127.076,179.153c-18.945,26.485-34.376,55.105-48.728,84.279-14.535,29.546-28.324,59.868-33.836,92.582a160.793,160.793,0,0,0-.347,53.755c.643,3.8,6.426,2.188,5.785-1.6-5.434-32.1,1.317-64.514,12.537-94.644,11.123-29.866,26.07-58.819,41.579-86.627a471.531,471.531,0,0,1,28.191-44.722c2.25-3.146-2.951-6.145-5.181-3.028Z" />
            </g>
          </svg>
        </div>
      </Zoom>
    </div>
  );
}
