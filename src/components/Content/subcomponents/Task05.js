import React from "react";
import { useContext } from "react";

import DataContext from "@context/DataContext";

import { makeStyles } from "@material-ui/core/styles";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

const useStyles = makeStyles({
  image: {
    width: "700px",
    border: "1px solid black",
  },
});

const Task05 = () => {
  const { setFifthTask, task05Data, fifthTask, getImagePath } =
    useContext(DataContext);

  const classes = useStyles();

  const updateData = (e) => {
    const newValue = e.target.value;
    const id = e.target.id;

    const newState = fifthTask.map((obj) => {
      if (obj.key === id) {
        return { ...obj, chosen: newValue };
      }
      return obj;
    });

    setFifthTask(newState);
  };

  return (
    <>
      <TableContainer component={Paper}>
        {task05Data.map((data) => (
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell
                  sx={{ fontSize: 20, fontWeight: "bold" }}
                  align="left"
                >
                  {data.first}
                </TableCell>
                <TableCell sx={{ fontSize: 20, fontWeight: "bold" }}>
                  {data.second}
                </TableCell>
                <TableCell sx={{ fontSize: 20, fontWeight: "bold" }}>
                  Zdjęcie
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.items.map((dish, index) => {
                if (data.type === dish.type) {
                  return (
                    <TableRow className={classes.row}>
                      <TableCell align="left">{dish.dish}</TableCell>
                      <TableCell>
                        <form>
                          <select
                            value={
                              fifthTask.filter(
                                (item) => item.key === dish.id
                              )[0].chosen === null
                                ? data.items.filter(
                                    (item) => item.id === dish.id
                                  )[0].name
                                : fifthTask.filter(
                                    (item) => item.key === dish.id
                                  )[0].chosen
                            }
                            onChange={updateData}
                            id={dish.id}
                            defaultValue={
                              data.items.filter(
                                (item) => item.id === dish.id
                              )[0].name
                            }
                          >
                            {data.items.map((item) => (
                              <option>{item.name}</option>
                            ))}
                          </select>
                        </form>
                      </TableCell>
                      <TableCell align="left">
                        <div>
                          <img
                            className={classes.image}
                            src={getImagePath(
                              fifthTask.filter(
                                (item) => item.key === dish.id
                              )[0].chosen === null
                                ? data.items.filter(
                                    (item) => item.id === dish.id
                                  )[0].src
                                : data.items.filter(
                                    (item) =>
                                      item.name ===
                                      fifthTask.filter(
                                        (item) => item.key === dish.id
                                      )[0].chosen
                                  )[0].src
                            )}
                          />
                        </div>
                      </TableCell>
                    </TableRow>
                  );
                }
              })}
            </TableBody>
          </Table>
        ))}
      </TableContainer>
    </>
  );
};

export default Task05;
