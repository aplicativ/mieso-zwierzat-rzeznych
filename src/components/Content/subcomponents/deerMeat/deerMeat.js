import React, { useContext } from 'react';
import { Droppable } from 'react-beautiful-dnd';

import DataContext from '@context/DataContext';
import './../index.css';
import './style.css';
import { makeStyles } from '@material-ui/styles';
import CloseIcon from '@mui/icons-material/Close';
import Zoom from '../zoom';

const useStyles = makeStyles({
  droppable: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: 'white',
    fontSize: '29px',
  },
});

const SVGDropable = ({ keyData }) => {
  const classes = useStyles();
  const { columnsInTasks, setColumnsInTasks } = useContext(DataContext);
  const { [keyData]: data } = columnsInTasks;

  const RemoveValue = () => {
    columnsInTasks.menu.list.splice(
      parseInt(columnsInTasks[keyData].list[0].key) - 1,
      0,
      columnsInTasks[keyData].list[0]
    );

    setColumnsInTasks((prevState) => ({
      ...prevState,
      [keyData]: {
        ...prevState[keyData],

        list: [],
      },
    }));

    setColumnsInTasks((prevState) => ({
      ...prevState,
      menu: {
        ...prevState.menu,
        list: [...prevState.menu.list],
      },
    }));
  };

  return (
    <Droppable droppableId={data.id}>
      {(provided) => {
        return (
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: '0 20px',
            }}
            xmlns="http://www.w3.org/1999/xhtml"
            ref={provided.innerRef}
            className={`dragable ${classes.droppable}`}
            columnID={data.id}
          >
            <div>{data.list.length > 0 && data.list[0].text}</div>
            {data.list.length > 0 && data.list[0].text && (
              <CloseIcon onClick={() => RemoveValue()} />
            )}
          </div>
        );
      }}
    </Droppable>
  );
};

export default function deerMeat() {
  return (
    <div style={{ border: '5px dashed #1C6EA4' }}>
      <Zoom >
        <div style={{width: '1000px', height: '600px', paddingTop: '50px'}}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 1783.276 841.89"
            className="svg"
            style={{ height: '90wh' }}
          >
            <defs></defs>
            <g id="b35bb955-8cd3-418c-86fd-0c81a4d8a94a" data-name="Warstwa 4">
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="1073.333"
                y1="297"
                x2="1077.705"
                y2="292.89"
              />
              <line
                class="ac3682f6-1ad6-493a-aaf3-97b67142ae5e"
                x1="1086.542"
                y1="284.581"
                x2="1276.543"
                y2="105.931"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="1280.962"
                y1="101.777"
                x2="1285.333"
                y2="97.667"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="1292.775"
                y="66.152"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(1402.9 105.139)"
        >
          Udziec
        </text> */}
              <foreignObject
                x="1292.775"
                y="66.152"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentDeer4"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="943"
                y1="321"
                x2="945.656"
                y2="315.62"
              />
              <line
                class="b8ed89a9-8265-4af8-98bc-1717a1f9cbab"
                x1="950.657"
                y1="305.488"
                x2="1028.177"
                y2="148.446"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="1030.678"
                y1="143.38"
                x2="1033.333"
                y2="138"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="894.143"
                y="66.152"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(985.881 105.139)"
        >
          Polędwica
        </text> */}
              <foreignObject x="894.143" y="66.152" width="296" height="63.333">
                <SVGDropable keyData="contentDeer3"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="886.667"
                y1="277"
                x2="882.348"
                y2="272.835"
              />
              <line
                class="bfb8a185-4a61-4a95-996c-bf5ba8dbed72"
                x1="874.248"
                y1="265.025"
                x2="829.702"
                y2="222.07"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="825.652"
                y1="218.165"
                x2="821.333"
                y2="214"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="682.143"
                y="142.152"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(787.269 181.139)"
        >
          Rostbef
        </text> */}
              <foreignObject
                x="682.143"
                y="142.152"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentDeer2"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="976"
                y1="439.667"
                x2="980.594"
                y2="435.808"
              />
              <line
                class="a00b350d-13d7-45b3-ad57-24789eab643d"
                x1="989.975"
                y1="427.927"
                x2="1266.715"
                y2="195.466"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="1271.406"
                y1="191.526"
                x2="1276"
                y2="187.667"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="1292.775"
                y="154.152"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(1416.912 193.139)"
        >
          Łata
        </text> */}
              <foreignObject
                x="1292.775"
                y="154.152"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentDeer5"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="650.667"
                y1="338.333"
                x2="644.879"
                y2="339.914"
              />
              <line
                class="a96effad-7761-407a-80f2-47d5912d38d8"
                x1="632.86"
                y1="343.197"
                x2="374.464"
                y2="413.778"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="368.455"
                y1="415.419"
                x2="362.667"
                y2="417"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="51.442"
                y="386.418"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(155.389 427.805)"
        >
          Górnica
        </text> */}
              <foreignObject x="51.442" y="386.418" width="296" height="63.333">
                <SVGDropable keyData="contentDeer11"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="666.667"
                y1="401"
                x2="660.967"
                y2="402.875"
              />
              <line
                class="b663e9ea-4ead-4cb5-9896-aab6f8876297"
                x1="649.263"
                y1="406.725"
                x2="374.218"
                y2="497.2"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="368.366"
                y1="499.125"
                x2="362.667"
                y2="501"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="51.442"
                y="470.685"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(145.471 509.138)"
        >
          Szponder
        </text> */}
              <foreignObject x="51.442" y="470.685" width="296" height="63.333">
                <SVGDropable keyData="contentDeer10"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="766.667"
                y1="470.333"
                x2="761.268"
                y2="472.952"
              />
              <line
                class="f3d13887-81a0-4ad9-84e6-bfc791c22a75"
                x1="750.641"
                y1="478.108"
                x2="373.378"
                y2="661.137"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="368.065"
                y1="663.714"
                x2="362.667"
                y2="666.333"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="51.442"
                y="639.218"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(154.735 677.139)"
        >
          Łopatka
        </text> */}
              <foreignObject x="51.442" y="639.218" width="296" height="63.333">
                <SVGDropable keyData="contentDeer8"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="734.667"
                y1="538.333"
                x2="729.462"
                y2="541.318"
              />
              <line
                class="acbb9097-d276-48b4-8f27-12bf1e840b5c"
                x1="719.131"
                y1="547.243"
                x2="373.037"
                y2="745.719"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="367.872"
                y1="748.682"
                x2="362.667"
                y2="751.667"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="51.442"
                y="723.485"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(113.444 762.472)"
        >
          Goleń przednia
        </text> */}
              <foreignObject x="51.442" y="723.485" width="296" height="63.333">
                <SVGDropable keyData="contentDeer7"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="590.667"
                y1="307.667"
                x2="584.696"
                y2="308.26"
              />
              <line
                class="f2bffe03-29b1-43bc-a920-9fe8aa8adff7"
                x1="573.325"
                y1="309.391"
                x2="374.323"
                y2="329.175"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="368.637"
                y1="329.74"
                x2="362.667"
                y2="330.333"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="51.442"
                y="302.152"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(171.637 341.139)"
        >
          Szyja
        </text> */}
              <foreignObject x="51.442" y="302.152" width="296" height="63.333">
                <SVGDropable keyData="contentDeer1"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="694.667"
                y1="445"
                x2="689.091"
                y2="447.217"
              />
              <line
                class="a636eae8-0b1b-4a61-a89b-4e6504f2f9f5"
                x1="678.027"
                y1="451.616"
                x2="373.774"
                y2="572.584"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="368.242"
                y1="574.783"
                x2="362.667"
                y2="577"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="51.442"
                y="554.952"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(158.353 595.805)"
        >
          Mostek
        </text> */}
              <foreignObject x="51.442" y="554.952" width="296" height="63.333">
                <SVGDropable keyData="contentDeer9"></SVGDropable>
              </foreignObject>
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="1108"
                y1="533"
                x2="1112.987"
                y2="529.664"
              />
              <line
                class="e671cdc0-e554-4845-8b4c-0b5f32ee0ef3"
                x1="1123.106"
                y1="522.895"
                x2="1421.62"
                y2="323.201"
              />
              <line
                class="ba8d730f-d655-4c32-a754-3b9009facbe4"
                x1="1426.68"
                y1="319.816"
                x2="1431.667"
                y2="316.48"
              />
              <rect
                class="acf22754-6c41-4ac9-bc7c-5eb98b42d539"
                x="1292.775"
                y="244.631"
                width="296"
                height="63.333"
              />
              {/* <text
          class="e2052cb0-162d-414b-b163-64397541d2de"
          transform="translate(1377.265 283.618)"
        >
          Goleń tylna
        </text> */}
              <foreignObject
                x="1292.775"
                y="244.631"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentDeer6"></SVGDropable>
              </foreignObject>
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="592.581 297.849 593.588 316.583 609.662 305.61 592.581 297.849"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="649.815 329.036 655.122 347.031 668.227 332.642 649.815 329.036"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="665.747 391.169 672.082 408.829 684.336 393.709 665.747 391.169"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="684.247 438.336 690.582 455.996 702.836 440.876 684.247 438.336"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="765.4 461.413 773.45 478.36 784.149 462.102 765.4 461.413"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="732.184 529.014 741.908 545.059 750.907 527.802 732.184 529.014"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="895.629 271.733 882.112 284.744 900.924 289.732 895.629 271.733"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="1077.966 305.471 1065.392 291.546 1059.808 310.191 1077.966 305.471"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="980.188 448.582 967.614 434.658 962.03 453.302 980.188 448.582"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="1111.976 541.129 1101.527 525.547 1093.329 543.199 1111.976 541.129"
              />
              <polygon
                class="e712c35a-d78e-4701-b638-02a2af93c96d"
                points="950.894 326.228 934.272 317.528 935.499 336.952 950.894 326.228"
              />
              <path
                class="b5a296e9-611d-4cb5-b34e-3ea475f47a06"
                d="M547.091,328.349c-14.639-5.609-31.1-.333-45.556,5.709s-29.654,13.036-45.194,10.715-29.724-19.042-22.671-32.806c27.187-21.883,56.158-46.623,62.516-80.529,6.25-33.326-11.017-69.271,1.695-100.782C517.632,81.695,595.84,74.907,612.947,25c1.739,27.219-24.975,46.762-47.7,62.507s-46.9,40.6-37.806,66.371c-2.9-20.264,12.8-39.04,30.942-49.176s39.072-14.436,57.885-23.324,36.615-24.735,38.023-45.151c5.947,20.184-8.666,41.026-26.17,53.16s-38.433,19.206-54.917,32.641c-24,19.566-35.915,52.385-29.9,82.406,42.694,2.348,84.913-27.989,95.457-68.592,12.159,22.116,2.174,53.173-20.728,64.469-9.522,4.7-22.624,8.768-23.031,19.206q24.493,25.882,48.985,51.767c16.792,17.744,36.1,36.716,60.752,38.235,19.9,1.226,38.246-9.342,56.238-17.762,75.216-35.2,161.831-36.257,245.168-34.491,44.955.952,90.51,2.623,133.523,15.465s83.85,38.26,105.767,76.73c13.414,23.545,19.1,50.6,24.915,76.749a20.024,20.024,0,0,1-10.077,22.231c-11.813,6.075-21.358-4.038-23.336-14.2-1.541-7.913.208-16.033.754-24.07a81.859,81.859,0,0,0-18.814-57.494,403.852,403.852,0,0,0-6.289,100.984c.558,8.759,1.608,18.039,7.116,24.96,11.469,14.41,34.758,11.068,51.1,19.868,21.574,11.615,25.166,39.838,27.7,63.829a598.88,598.88,0,0,0,51.216,186.544c4.024,8.768,8.411,18.2.156,28.264a27.032,27.032,0,0,1-12.728,8.482c-11.044,3.516-22.707,1.288-33.917-.942,18.539-30.554,9.338-69.332-.389-103.576q-14.667-51.64-29.336-103.281c-2-7.04-4.116-14.306-8.857-19.948-6.574-7.825-17-11.209-26.7-14.731a301.825,301.825,0,0,1-130.177-92.145,125.36,125.36,0,0,0,31.949,77.991c14.718,16.347,35.273,32.463,33.48,54.19-.768,9.31-5.764,17.7-10.637,25.731L1056.981,782.836c-1.415,2.332-5.486,4.438-6.161,1.806q.918,8.718,1.837,17.434l-39.593-2.112.066-20.046c26.5-6.177,42.6-31.856,55.227-55.51q35.22-65.982,64.846-134.642a312.881,312.881,0,0,1-111.39-112.9q-63.986,10.381-128.717,15.42c-11.388,89.084,5.553,181.409,47.607,261.151a14.85,14.85,0,0,1-8.955,21.065h0l.012,30.544a88.311,88.311,0,0,1-43.231,3.545l.209-20.507c10.195-1.6,16.952-12.02,17.756-22.1s-2.9-19.925-6.385-29.448a870.329,870.329,0,0,1-51.58-250.776l-66.881,7.53c-12.627,53.592-54,96.507-71.863,148.649-9.741,28.43-64.413,160.929-64.413,160.929s-10.02,2.513-7.177.354q-1.821,7.768-3.641,15.539a127.871,127.871,0,0,1-28.136.267c-.809-11.224-.627-8.7-1.436-19.925,28.194-7.566,40.9-39.9,49.069-67.794q11.057-37.761,22.111-75.52c2.186-7.468,4.394-15.239,3.1-22.9-1.047-6.218-4.357-12-4.7-18.289-.515-9.549,5.765-17.985,11.323-25.845,27.562-38.978,31.44-112.372,31.44-112.372C683.857,459.887,561.242,362.336,547.091,328.349Z"
              />
              <path d="M964.252,362.082a260.713,260.713,0,0,0,21.974,61.62c8.868,17.845,18.974,35.669,30.893,51.675,2.28,3.061,7.49.073,5.181-3.029-11.579-15.548-21.4-32.822-30.111-50.112a255.873,255.873,0,0,1-22.151-61.749c-.8-3.774-6.589-2.177-5.786,1.595Z" />
              <path d="M1002.487,257.746c-8.106,7.5-14.548,16.994-20.67,26.118a308.843,308.843,0,0,0-17.676,29.344c-1.78,3.421,3.4,6.459,5.181,3.028a307.12,307.12,0,0,1,16.889-28.164c6.074-9.135,12.424-18.59,20.519-26.083,2.84-2.629-1.411-6.864-4.243-4.243Z" />
              <path d="M1046.286,513.94a88.108,88.108,0,0,0,62.218-53.311c1.427-3.589-4.374-5.146-5.786-1.6a81.973,81.973,0,0,1-58.027,49.12c-3.768.838-2.173,6.624,1.6,5.786Z" />
              <path d="M718.447,473.2c-2.119-16.246.684-32.372,6.479-47.6,5.241-13.775,12.829-29.56,26.468-36.731,13-6.836,29.79-3.5,40.109,6.631,10.888,10.686,14.19,26.519,10.931,41.1-4.2,18.79-14,37-22.735,54.03-1.759,3.429,3.416,6.468,5.181,3.028q7.011-13.67,13.194-27.738c3.806-8.711,7.757-17.55,9.946-26.834,3.729-15.814.928-32.491-9.735-45.081a42.088,42.088,0,0,0-40.775-13.721c-16.018,3.6-26.3,17.556-33.1,31.535-9.248,19-14.727,40.2-11.962,61.388.493,3.775,6.5,3.824,6,0Z" />
              <path d="M716.319,477.075A73.817,73.817,0,0,0,775.1,504.45c3.856-.077,3.869-6.078,0-6a68.155,68.155,0,0,1-54.541-25.617c-2.415-3.013-6.636,1.257-4.243,4.242Z" />
              <path d="M873.292,260.851c-13.613,23.4-16.752,54.671-6.12,79.82,6.28,14.856,17.134,27.3,25.966,40.639,9.367,14.152,17.677,29.164,22.8,45.408,6.23,19.768,7.965,40.326.514,60.005-1.368,3.615,4.43,5.178,5.786,1.595,11.9-31.43,1.661-65.873-14.1-94.027-7.992-14.278-18.021-27.072-26.991-40.7-8.528-12.959-13.6-26.711-14.125-42.307-.558-16.448,3.157-33.137,11.455-47.4,1.946-3.344-3.238-6.368-5.181-3.028Z" />
              <path d="M571.065,358.418c9.511.834,16.346-8.22,22.315-14.3,7.455-7.594,14.768-15.353,21.471-23.626,12.783-15.777,23.316-33.488,27-53.688.688-3.775-5.093-5.394-5.786-1.595-3.4,18.619-12.753,35.095-24.435,49.77-5.612,7.049-11.7,13.712-17.935,20.207q-4.8,5-9.7,9.888c-3.593,3.6-7.42,7.827-12.927,7.344-3.846-.338-3.823,5.664,0,6Z" />
              <path d="M580.47,369.568c53.89,4.293,106.04,19.88,159.657,26.172,3.828.449,3.8-5.554,0-6-53.621-6.293-105.749-21.878-159.657-26.172-3.849-.307-3.829,5.695,0,6Z" />
              <path d="M804.487,410.433c28.105,1.946,56.6,3.765,84.053-3.993,6.694-1.892,13.267-4.226,20.16-5.316,7.622-1.205,15.446-1.351,23.145-1.641q22.614-.849,45.242-.073c3.862.126,3.858-5.874,0-6-28.67-.936-58.8-2.148-86.5,6.172-28.221,8.478-57.086,6.86-86.1,4.851-3.854-.266-3.837,5.735,0,6Z" />
              <path d="M1012.532,329.929c-19.793-10.645-42.461-14.946-64.252-19.484-9.252-1.926-18.525-3.819-27.621-6.4-6.188-1.758-17.308-8.659-22.977-2.358-7.313,8.128.188,21.9,5.4,28.854a79.294,79.294,0,0,0,24.723,21.343c23.579,13.247,51.7,15,78.055,11.73,8.881-1.1,13.882-4.576,15.25-13.432,1.386-8.966-2.234-17.181-10.648-21.078-3.481-1.612-6.532,3.558-3.028,5.181,6.578,3.047,9.046,9.009,7.591,15.981-.3,1.455-.807,3.66-1.74,4.738-3.19,3.684-12.416,3.205-16.829,3.533-21.877,1.629-44.649-.525-64.24-11.074a75.2,75.2,0,0,1-22.477-18.1c-3.419-4.141-6.729-8.915-8.134-14.162-1.04-3.885-1.712-10.874,3.964-9.826,3.72.687,7.256,2.479,10.836,3.651,3.482,1.141,7.086,2.032,10.645,2.907,7.926,1.946,15.94,3.523,23.926,5.2,19.846,4.157,40.512,8.294,58.527,17.984,3.4,1.829,6.434-3.349,3.029-5.181Z" />
              <path d="M641.238,423.778c23.811,6.337,47.742,12.214,71.553,18.551,3.736.994,5.329-4.792,1.595-5.785-23.811-6.337-47.742-12.215-71.553-18.552-3.735-.994-5.329,4.792-1.595,5.786Z" />
              <path d="M801.383,454.306c19.82.94,37.1,11.086,55.01,18.523a112.733,112.733,0,0,0,27.725,7.9c11.1,1.552,22.6,1.855,33.009,6.448,3.5,1.545,6.557-3.624,3.028-5.181-9.848-4.344-20.243-5.334-30.813-6.6a116.513,116.513,0,0,1-31.354-8.354c-18.507-7.716-36.112-17.765-56.605-18.737-3.86-.184-3.851,5.817,0,6Z" />
            </g>
          </svg>
        </div>
      </Zoom>
    </div>
  );
}
