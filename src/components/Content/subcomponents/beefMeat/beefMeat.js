import React, { useContext } from 'react';
import { Droppable } from 'react-beautiful-dnd';

import DataContext from '@context/DataContext';
import './../index.css';
import './style.css';
import { makeStyles } from '@material-ui/styles';
import CloseIcon from '@mui/icons-material/Close';
import Zoom from '../zoom';

const useStyles = makeStyles({
  droppable: {
    // height: "100%",
    // display: "flex",
    // justifyContent: "center",
    // alignItems: "center",
    // textAlign: "center",
    // color: "white",
    // fontSize: "29px",
  },
  zoomBorder: {
    border: '5px dashed #1C6EA4',
  },
});

const SVGDropable = ({ keyData }) => {
  const classes = useStyles();
  const { columnsInTasks, setColumnsInTasks } = useContext(DataContext);
  const { [keyData]: data } = columnsInTasks;

  const RemoveValue = () => {
    columnsInTasks.menu.list.splice(
      parseInt(columnsInTasks[keyData].list[0].key) - 1,
      0,
      columnsInTasks[keyData].list[0]
    );

    setColumnsInTasks((prevState) => ({
      ...prevState,
      [keyData]: {
        ...prevState[keyData],

        list: [],
      },
    }));

    setColumnsInTasks((prevState) => ({
      ...prevState,
      menu: {
        ...prevState.menu,
        list: [...prevState.menu.list],
      },
    }));
  };

  return (
    <Droppable droppableId={data.id}>
      {(provided) => {
        return (
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: '0 20px',
            }}
            xmlns="http://www.w3.org/1999/xhtml"
            ref={provided.innerRef}
            className={`dragable ${classes.droppable}`}
            columnID={data.id}
          >
            <div>{data.list.length > 0 && data.list[0].text}</div>
            {data.list.length > 0 && data.list[0].text && (
              <CloseIcon onClick={() => RemoveValue()} />
            )}
          </div>
        );
      }}
    </Droppable>
  );
};

export default function beefMeat() {
  return (
    <div style={{ border: '5px dashed #1C6EA4' }}>
      <Zoom>
        <div style={{width: '1000px', height: '600px', paddingTop: '50px'}}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 1783.276 841.89"
            className="svg"
            style={{ height: '90wh' }}
          >
            <defs></defs>
            <g id="fce72aaf-1f65-44c4-ac9a-401d7eaea5f9" data-name="Warstwa 3">
              <polygon
                class="a90bbb51-6c66-450d-b6c6-a5b074c05fc4"
                points="550.718 357.365 550.716 357.365 550.714 357.365 550.718 357.365"
              />
              <path
                class="a90bbb51-6c66-450d-b6c6-a5b074c05fc4"
                d="M1334.774,586.165c1.556.405-12.418-9.468-16.569-19.524,8.337,7.044,12.755,5.558,12.915,6.066-.16-.508-3.463-18-3.619-18.5a69.654,69.654,0,0,0-10.939-22.279c-8.4-10.836-19.352-8.183-20.242-8.47-.28-.584.149.81-.28-.584-3.164-10.179-8.806-30.337-9.44-34.32-3.291-20.7-1.533-35-.336-45.553,1.177-10.541,5.9-29.209,8.871-40.817,5.84-23.277,11.742-35.932,18.938-63.3,1.762-6.853,14.192-43.779,2.928-78.844-7.674-23.88-23.154-41.585-37.667-53.258A112.9,112.9,0,0,0,1251,190a97.209,97.209,0,0,0-15.373-16.672,85.292,85.292,0,0,0-20.043-12.507c-14.043-6.287-28.468-8.768-41.792-9.543a175.8,175.8,0,0,0-36.918,2.087,206.637,206.637,0,0,0-33.46,8.259c-3.26-.864-7.912-1.991-13.9-3.138-12.674-2.476-31.273-4.6-54.229-6.3-11.462-.909-24.026-1.777-37.381-3.241-6.677-.741-13.568-1.605-20.563-2.824a204.139,204.139,0,0,0-22.31-3.031c-15.392-1.265-31.5-1.113-48.039-.887l-12.477.138-6.29.011c-2.179,0-4.474.046-6.683.2-8.907.607-17.559,2.323-26.145,3.826-8.566,1.506-16.927,2.82-24.912,2.625a54.631,54.631,0,0,1-5.853-.478,43.966,43.966,0,0,1-5.651-1.234,104.643,104.643,0,0,1-11.335-4.032c-3.8-1.537-7.694-3.173-11.824-4.442a49.883,49.883,0,0,0-12.84-2.331,47.659,47.659,0,0,0-6.489.187,44.6,44.6,0,0,0-6.293,1.116,78.125,78.125,0,0,0-11.478,3.853,217.13,217.13,0,0,0-20.2,10.079c-3.168,1.731-6.261,3.436-9.314,5.034-1.547.81-3,1.555-4.6,2.339-1.554.814-3.131,1.686-4.6,2.526-11.9,6.766-22.352,13.932-31.827,20.487-1.229.857-2.4,1.671-3.6,2.507-7.74,3.146-19.4,7.928-32.162,13.275-4.114,1.724-8.08,3.344-11.845,4.808-5.894-.194-10.211-1.8-15.525-3.164,21.1-3.708,53.644-12.044,53.441-45.717-11.763,8.225-29.215,2.041-43.538,8.191-11.029,4.735-7.883,14.2-19.471,15.285,9.568-14.517,9.518-12.855,12.231-23.476,3.169-12.419.79-32.558-12.383-38.127,4.355,15.48,1.563,42.614-18.433,49.291-14.457-6.627-37.335-10.626-52.585-9.166-16.267,1.564-35.625,9.166-44.446,21.661-20.759-.631-23.6-26.6-24.46-40.3-13.48,8.993-13.338,32.53-7.886,43.592,6.735,13.66,11.228,12.548,24.994,20.972-12.436,3.207-20.606-8.061-34.228-5.087-12.424,2.706-23.29,8.733-35.185,13.293,21.475,11.218,33.7,35.978,60.817,18.706,6.138,32.764,6.022,74.639-22.9,100.726-5.6,7.564,1.185,41.413,27.216,37.858,3.623,16.424,24.181,6.888,38.093.635,8.289-3.723,16.321-6.926,24.395-10.5l0,0c4.923-2.186,9.867-4.511,14.891-7.178a39.724,39.724,0,0,1,4.4-2.018,34,34,0,0,1,6.979-.666,38.65,38.65,0,0,1,18.9,5.52c12.916,7.4,24.8,21.553,35.293,38.439,5.221,8.512,9.9,18.068,15.791,27.776,2.967,4.839,2-1.2,2.967,4.839,2.571,15.977,30.248,51.993,30.343,52.089,27.52,26.782,63.353,29.644,63.534,29.629.231-.015,0,0,.231,0h0l-.231,0,.231,0a8.552,8.552,0,0,0,.449,1.468c1.038,2.744-1.6-3.968-.449-1.468,2.309,4.98,4.7,9.563,7.016,13.825,2.276,4.173,4.476,8.011,6.561,11.554-.14,8.921-.417,18.094-.631,27.5-.126,5.863-.206,11.669-.2,17.4.073,5.691-.093,11.069-.443,16.516-.7,10.828-1.982,21.3-3.513,31.215a373.832,373.832,0,0,1-11.766,52.054c-4.488,14.536-9.411,25.888-13.51,33.372a89.593,89.593,0,0,1-7.09,11.222,20,20,0,0,0-2.782,3.157,8.054,8.054,0,0,0-1.067,2.152,3.832,3.832,0,0,0,.189,2.913,5.58,5.58,0,0,0,2.661,2.273,16.333,16.333,0,0,0,3.653,1.136c5.277,1,11.8.886,19.257.038,1.865-.226,3.793-.512,5.781-.875.993-.191,2-.4,3.034-.65l.811-.207a8.555,8.555,0,0,0,.887-.328c.529-.2,1.036-.451,1.555-.7a25.866,25.866,0,0,0,10.263-9.83c5.173-8.528,7.44-19.05,8.89-30.069,1.458-11.054,1.726-22.964,2.511-35.08.421-5.886,1.422-12.155,2.405-18.427,1.043-6.3,2.281-12.674,3.808-19.092a198.391,198.391,0,0,1,5.537-19.24q1.65-4.788,3.65-9.483l1.024-2.336c.352-.734.631-1.464,1.036-2.171.766-1.387,1.624-2.874,2.525-4.365,3.556-5.928,7.587-11.65,11.33-17.681,1.659-2.668,3.228-5.416,4.686-8.21A201.518,201.518,0,0,1,818.84,579.1a264.157,264.157,0,0,1,4.4,61.874,200.128,200.128,0,0,1-4.944,36.654,115.165,115.165,0,0,1-6,18.5,71.669,71.669,0,0,1-4.444,8.73q-1.263,2.092-2.694,4.052l-1.177,1.54c-.558.669-1.1,1.331-1.7,2l-3.647,4.006c1.506,1.074,3.236,2.228,4.809,3.31a30.64,30.64,0,0,0,2.681,1.521c.459.248.952.451,1.443.665.466.2,1.07.462,1.282.535a35.394,35.394,0,0,0,7.26,1.858,44.035,44.035,0,0,0,14.747-.44,37.454,37.454,0,0,0,7.3-2.194,40.905,40.905,0,0,0,6.769-3.554,18.15,18.15,0,0,0,6.067-6.5,23.244,23.244,0,0,0,2.315-8.11c.55-4.771.642-8.784,1.026-13.194.264-4.553.2-8.845.136-13.118-.144-8.52-.465-16.818-.27-24.81a69.717,69.717,0,0,1,1.015-11.356,72.745,72.745,0,0,1,3.5-10.759,72.927,72.927,0,0,0,3.731-11.757,36.451,36.451,0,0,0,.724-6.368c.028-1.066-.006-2.133-.065-3.188-.055-.581-.067-.993-.164-1.7-.09-.589-.186-1.082-.287-1.579-1.645-7.579-3.419-14.555-4.971-21.53-1.569-6.934-3.006-13.684-4.267-20.219-2.532-13.08,1.869-36.827.824-48.084s1.316-25.046,2.094-33.956c.352-4.147,1.928-.527,5.867-1.192a163.388,163.388,0,0,1,26.142-2.064c9.49-.111,19.663.313,30.493.55a185.365,185.365,0,0,0,34.327-2.874c11.694-1.9,23.558-4.461,35.667-6.414l4.4-.672,4.491-.4q4.626-.418,9.2-.955,9.133-1.056,17.911-2.565a284.84,284.84,0,0,0,33.363-7.767c.593-.176,1.162-.374,1.755-.558q1.324,2.586,2.863,5.714c2.843,5.89,6.184,13.194,9.406,21.947a159.507,159.507,0,0,1,8.187,30.471,93.082,93.082,0,0,1,1.151,18.163c-.077,1.563.263-4.319.065-2.756-.1.787-.677,9.043-.818,9.815l-.23,1.155-.237.97c-.072.157-.057.379-.221.493l-.581.547-5.982,11.99c-19.726,36.578-47.743,75.355-56.125,89.691-4.445,7.614-8.741,15.541-12.663,23.87-1.968,4.17-3.826,8.435-5.53,12.854-.853,2.2-1.648,4.445-2.4,6.735l-.543,1.715c-.191.643-.3.922-.653,2.233l-1.743,6.662c1.556.661,3.039,1.315,4.621,1.949.8.3,1.628.566,2.462.822l1.842.493c1.861.474,3.711.845,5.565,1.15a99.529,99.529,0,0,0,11.168,1.174,84.649,84.649,0,0,0,22.883-1.682,40.75,40.75,0,0,0,7.358-2.462c.344-1.933.7-3.864,1.032-5.794.069-.818.126-1.628.161-2.484a14.774,14.774,0,0,1,1.575-6.246v-2.434a13.945,13.945,0,0,1,1.613-6.727c1.422-8.818,3.046-17.628,5.947-26,5.026-14.5,12.666-26.938,22.589-38.627,8.986-10.591,19.642-38.29,30.428-46.917,8.053-6.83,16.286-13.454,24.241-20.391a97.621,97.621,0,0,0,8.515-22.016c2.018-7.461,3.425-14.677,4.717-21.538,2.343-12.754,4.346-24.309,6.807-34.449,9.464,12.009,19.237,22.723,28.345,32.634,5.944,6.222,11.925,11.439,17.319,16.5,5.408,5.026,10.293,9.812,14.455,14.444,1.292,1.41,2.481,2.806,3.62,4.189a271.925,271.925,0,0,1,.111,37.155l-1.422,23.675c-.218,4.124-.4,8.39-.424,12.7-.054,4.063-.341,8.26-.761,12.5-3.639,33.994-13.412,64.285-25.02,83.5-1.12,1.877-2.263,3.608-3.413,5.255-.207.631-.436,1.258-.658,1.889-1.162,4.758-2.416,9.5-3.654,14.245a55.314,55.314,0,0,0,19.822,7.858c10.935,2.1,21.332.582,28.311-2.289-.149.4-.287.764-.424,1.143.474-.3,4.724-5.332,4.873-6.467-.306.068-.608.137-.94.2.439-.993.89-2.056,1.338-3.153.076-.539.133-1.07.213-1.606a45.9,45.9,0,0,1,2.412-15.093c.023-.107.042-.233.061-.344a.29.29,0,0,1,.054.019c.413-1.132.852-2.263,1.384-3.409.065-.157,8.015-26.87,8.06-27.257.478-4.07.841-8.317,1-12.724.077-2.2.107-4.464.034-6.742-.045-2.217-.076-4.365-.065-6.62.027-17.872,2.408-37.515,4.7-58.372l.8-7.774c.256-2.439.626-4.832,1.074-7.27.9-4.835,2.067-9.64,3.275-14.387,2.431-9.5,5.015-18.809,6.509-28.043.753-4.617,1.212-.042,1.216-4.625a8.292,8.292,0,0,0,.474-6.8c-2.752-8.061-19.062-46.943-21.233-51.641-.29-2.324-.542-4.992-.7-8.019a126.035,126.035,0,0,1,1.071-22.5,221.317,221.317,0,0,1,6.122-29.22c2.879-10.706,6.6-22.3,10.508-34.847,3.86-12.556,8.007-26.071,10.687-40.893a237.037,237.037,0,0,0,3.332-46.868A219.786,219.786,0,0,0,1273,237.968c-1.452-5.507-3.18-10.908-5.144-16.179.546.455,1.089.868,1.647,1.353a84.5,84.5,0,0,1,15.021,16.749,76.414,76.414,0,0,1,10.771,24.634c2.313,9.422,6.834,26.082,6.349,37.086-.948,22.081-7.282,44.028-13.611,69.22-6.352,25.192-15.686,45.381-18.217,68.184a178.525,178.525,0,0,0-.913,32.6,112.323,112.323,0,0,0,5.427,27.994c3.647,10.878,5.13,21.545,10.1,28.089-1.759.829,0,0-1.407,2.221-2.1,3.306-2.267,7.958-1.231,12.334,2.1,9,8.325,17.987,16.531,26.243C1301.663,571.813,1317.028,582.614,1334.774,586.165Z"
              />
              <path
                class="a90bbb51-6c66-450d-b6c6-a5b074c05fc4"
                d="M423.094,209.944c-.4-1.728.134,1.758,0,0Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M594.824,349.153a227.633,227.633,0,0,0,35.848-51.5,231.8,231.8,0,0,0,20.6-59.079,227.363,227.363,0,0,0,4.283-35.241c.136-3.218-4.864-3.211-5,0a222.731,222.731,0,0,1-11.07,60.224A226.94,226.94,0,0,1,612.6,318.622a218.834,218.834,0,0,1-21.315,27c-2.184,2.359,1.344,5.9,3.535,3.535Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M744.288,149.439a489.707,489.707,0,0,0-12.042,73,484.612,484.612,0,0,0-.891,73.574c.77,12.014,2.612,24.041,2.545,36.093-.054,9.891-1.981,19.512-7,28.109-7.984,13.689-21.46,23.109-34.238,31.937C679.158,401.478,664.9,410.67,654.6,423.7c-1.3,1.644-2.542,3.328-3.7,5.07-1.792,2.684,2.537,5.191,4.317,2.524,9.124-13.67,23.047-23.172,36.372-32.346,13.285-9.147,27.186-18.389,36.7-31.691a55.488,55.488,0,0,0,9.877-24.687c1.666-12.031.007-24.1-1.027-36.108a482.3,482.3,0,0,1,9.884-146.8q1-4.459,2.086-8.9c.763-3.125-4.058-4.459-4.821-1.329Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M731.011,361.775a519.553,519.553,0,0,1,151.108-38.083q22.042-2.113,44.21-2.334c3.215-.033,3.223-5.033,0-5A524.285,524.285,0,0,0,771.194,341.57q-21.073,6.8-41.512,15.383a2.585,2.585,0,0,0-1.746,3.076,2.515,2.515,0,0,0,3.075,1.746Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M908.576,142a610.637,610.637,0,0,0,.557,65.963,623.434,623.434,0,0,0,7.63,65.616c3.61,21.213,7.946,42.428,14.9,62.82,3.345,9.813,7.021,19.512,10.2,29.383a212.944,212.944,0,0,1,7.436,30.947,288.794,288.794,0,0,1,3.061,66.411q-.288,4.157-.7,8.3a2.518,2.518,0,0,0,2.5,2.5,2.555,2.555,0,0,0,2.5-2.5,291.988,291.988,0,0,0-1.142-66.5,226.691,226.691,0,0,0-6.229-31.873c-2.816-10-6.378-19.756-9.848-29.536a304.218,304.218,0,0,1-9.281-29.651q-3.9-16.026-6.9-32.256a611.406,611.406,0,0,1-10-131.39q.133-4.119.323-8.235c.147-3.217-4.853-3.21-5,0Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M940.936,357.3q24.156-2.886,48.11-7.2,23.966-4.335,47.636-10.132,13.3-3.256,26.481-6.971c3.092-.87,1.778-5.7-1.33-4.821a797.529,797.529,0,0,1-94.191,20.47Q954.32,350.7,940.936,352.3a2.578,2.578,0,0,0-2.5,2.5,2.516,2.516,0,0,0,2.5,2.5Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M1102.032,163.778a597.609,597.609,0,0,0-35.379,126.584q-3.018,18.433-4.9,37.031a2.516,2.516,0,0,0,2.5,2.5,2.556,2.556,0,0,0,2.5-2.5,592.075,592.075,0,0,1,10.185-64.584,598.047,598.047,0,0,1,17.191-63.047q5.823-17.52,12.724-34.655a2.517,2.517,0,0,0-1.746-3.076,2.561,2.561,0,0,0-3.075,1.747Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M1060.545,329.148a251.058,251.058,0,0,0-4.455,106.578,246.111,246.111,0,0,0,6.982,29.459c.93,3.071,5.757,1.762,4.821-1.33a244.29,244.29,0,0,1-9.761-51.732,247.643,247.643,0,0,1,1.438-52.508,244.392,244.392,0,0,1,5.8-29.138c.817-3.115-4-4.444-4.821-1.329Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M1159.755,462.114c8.376-6.7,20.58-5.393,30.075-2.242,11.867,3.939,22.55,10.959,34.856,13.656,6.867,1.5,14.126,1.743,20.842-.581a2.561,2.561,0,0,0,1.746-3.075,2.523,2.523,0,0,0-3.075-1.746c-12.076,4.179-24.732-.743-35.731-5.767-10.978-5.014-22.227-10.531-34.62-10a29.8,29.8,0,0,0-17.628,6.215,2.516,2.516,0,0,0,0,3.536,2.56,2.56,0,0,0,3.535,0Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M1291.176,242.617l4.01-20a2.564,2.564,0,0,0-1.746-3.075,2.525,2.525,0,0,0-3.076,1.746l-4.01,20a2.565,2.565,0,0,0,1.746,3.076,2.525,2.525,0,0,0,3.076-1.747Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M818.28,149.259a449.228,449.228,0,0,0-11.539,143q1.632,20.28,5.1,40.359a2.521,2.521,0,0,0,3.075,1.746,2.558,2.558,0,0,0,1.746-3.076,444.96,444.96,0,0,1-1.36-141.463q3.012-19.785,7.8-39.241c.768-3.124-4.053-4.457-4.822-1.329Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M730.986,357.035a208.264,208.264,0,0,0,35.559,27.246,436.131,436.131,0,0,0,39.423,21.206c13.555,6.579,27.262,12.844,40.755,19.553A359.417,359.417,0,0,1,885.4,446.834,180.024,180.024,0,0,1,918.945,475q1.848,2.016,3.621,4.1a2.52,2.52,0,0,0,3.535,0,2.551,2.551,0,0,0,0-3.535,176.358,176.358,0,0,0-33.138-29.613A316.685,316.685,0,0,0,854.8,423.516c-13.413-6.84-27.1-13.131-40.674-19.632-13.483-6.455-26.889-13.119-39.768-20.724A215.731,215.731,0,0,1,738.5,357.245q-2.021-1.841-3.981-3.745c-2.311-2.242-5.85,1.29-3.536,3.535Z"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="508.314"
                y1="547.041"
                x2="511.85"
                y2="542.194"
              />
              <line
                class="ea9e5d9e-2bfc-454a-8093-616a1999651f"
                x1="519.109"
                y1="532.246"
                x2="631.612"
                y2="378.044"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="635.241"
                y1="373.069"
                x2="638.778"
                y2="368.222"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="204.981"
                y="511.708"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(299.083 550.695)"
        >
          Karkówka
        </text> */}
              <foreignObject
                x="204.981"
                y="511.708"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef13"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="585.314"
                y1="624.041"
                x2="588.96"
                y2="619.276"
              />
              <line
                class="fc8de12b-8f88-4a3b-8ad8-c24a150b14e9"
                x1="596.085"
                y1="609.96"
                x2="777.792"
                y2="372.423"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="781.355"
                y1="367.766"
                x2="785"
                y2="363"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="281.981"
                y="588.708"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(346.708 627.695)"
        >
          Mięso z łopatki
        </text> */}
              <foreignObject
                x="281.981"
                y="588.708"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef12"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="948.314"
                y1="726.041"
                x2="948.038"
                y2="720.047"
              />
              <line
                class="bc278656-d43b-4d50-842b-19662928b7d0"
                x1="947.467"
                y1="707.626"
                x2="932.895"
                y2="390.871"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="932.609"
                y1="384.66"
                x2="932.333"
                y2="378.667"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="882.981"
                y="736.708"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(977.01 775.695)"
        >
          Szponder
        </text> */}
              <foreignObject
                x="882.981"
                y="736.708"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef9"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1333.275"
                y1="726.041"
                x2="1329.064"
                y2="721.767"
              />
              <line
                class="f3f492cd-dbb4-407e-8812-4294281d33c5"
                x1="1320.635"
                y1="713.214"
                x2="1021.425"
                y2="409.551"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1017.211"
                y1="405.274"
                x2="1013"
                y2="401"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="1267.942"
                y="736.708"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(1392.079 775.695)"
        >
          Łata
        </text> */}
              <foreignObject
                x="1267.942"
                y="736.708"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef8"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="585.314"
                y1="700.781"
                x2="588.96"
                y2="696.016"
              />
              <line
                class="fc8de12b-8f88-4a3b-8ad8-c24a150b14e9"
                x1="596.085"
                y1="686.701"
                x2="777.792"
                y2="449.163"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="781.355"
                y1="444.506"
                x2="785"
                y2="439.74"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="281.981"
                y="665.448"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(388.891 704.435)"
        >
          Mostek
        </text> */}
              <foreignObject
                x="281.981"
                y="665.448"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef11"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="585.314"
                y1="776.781"
                x2="588.96"
                y2="772.016"
              />
              <line
                class="fc8de12b-8f88-4a3b-8ad8-c24a150b14e9"
                x1="596.085"
                y1="762.701"
                x2="777.792"
                y2="525.163"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="781.355"
                y1="520.506"
                x2="785"
                y2="515.74"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="281.981"
                y="741.448"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(345.266 780.435)"
        >
          Pręga przednia
        </text> */}
              <foreignObject
                x="281.981"
                y="741.448"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef10"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="447.647"
                y1="471.708"
                x2="451.977"
                y2="467.554"
              />
              <line
                class="bc92f7f9-b03b-4a9f-a19a-300c0da4e0f7"
                x1="460.173"
                y1="459.69"
                x2="587.22"
                y2="337.794"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="591.318"
                y1="333.862"
                x2="595.647"
                y2="329.708"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="144.314"
                y="436.375"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(264.509 475.361)"
        >
          Szyja
        </text> */}
              <foreignObject
                x="144.314"
                y="436.375"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef14"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1304.868"
                y1="334.343"
                x2="1310.867"
                y2="334.244"
              />
              <line
                class="bf72db33-e1c2-4e98-bfbe-d4a481997153"
                x1="1324.761"
                y1="334.015"
                x2="1373.387"
                y2="333.213"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1380.334"
                y1="333.099"
                x2="1386.333"
                y2="333"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="1392.086"
                y="297.255"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(1509.293 336.242)"
        >
          Ogon
        </text> */}{' '}
              <foreignObject
                x="1392.086"
                y="297.255"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef6"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1223.599"
                y1="177.29"
                x2="1229.598"
                y2="177.191"
              />
              <line
                class="bf72db33-e1c2-4e98-bfbe-d4a481997153"
                x1="1243.491"
                y1="176.962"
                x2="1292.118"
                y2="176.161"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1299.065"
                y1="176.046"
                x2="1305.064"
                y2="175.948"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="1310.816"
                y="140.203"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(1420.941 179.19)"
        >
          Udziec
        </text> */}
              <foreignObject
                x="1310.816"
                y="140.203"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef5"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1234"
                y1="502"
                x2="1239.665"
                y2="500.024"
              />
              <line
                class="a619b71b-2f07-41a5-ba3d-36aec7022c8e"
                x1="1251.267"
                y1="495.976"
                x2="1407.89"
                y2="441.339"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1413.691"
                y1="439.315"
                x2="1419.356"
                y2="437.339"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="1322.023"
                y="369.005"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(1407.796 407.992)"
        >
          Pręga tylna
        </text> */}{' '}
              <foreignObject
                x="1322.023"
                y="369.005"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef7"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="757.139"
                y1="222.008"
                x2="753.592"
                y2="217.168"
              />
              <polyline
                class="bf7c9077-c338-438f-8112-d5827a1f616a"
                points="746.496 207.484 667 99 568.828 82.05"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="562.913"
                y1="81.029"
                x2="557"
                y2="80.008"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="307.805"
                y="11.674"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(408.519 50.661)"
        > </text> */}
              <foreignObject x="307.805" y="11.674" width="296" height="63.333">
                <SVGDropable keyData="contentBeef1"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="833"
                y1="195"
                x2="829.692"
                y2="189.995"
              />
              <line
                class="b69252aa-46d3-4863-a883-858de1131400"
                x1="823.384"
                y1="180.451"
                x2="763.462"
                y2="89.785"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="760.308"
                y1="85.013"
                x2="757"
                y2="80.008"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="618.805"
                y="11.674"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(713.415 50.661)"
        >
          Rozbratel
        </text> */}{' '}
              <foreignObject x="618.805" y="11.674" width="296" height="63.333">
                <SVGDropable keyData="contentBeef2"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="939.667"
                y1="214"
                x2="943.909"
                y2="209.757"
              />
              <line
                class="bc90d2f3-dfed-4cd4-9877-2d33b82ea864"
                x1="951.944"
                y1="201.723"
                x2="1060.407"
                y2="93.26"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1064.424"
                y1="89.243"
                x2="1068.667"
                y2="85"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="929.103"
                y="11.674"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(1034.229 50.661)"
        >
          Rostbef
        </text> */}{' '}
              <foreignObject x="929.103" y="11.674" width="296" height="63.333">
                <SVGDropable keyData="contentBeef3"></SVGDropable>
              </foreignObject>
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1039"
                y1="194"
                x2="1043.902"
                y2="190.54"
              />
              <polyline
                class="b7025df5-47c1-4476-925f-4d7c4652a8bc"
                points="1053.824 183.536 1141 122 1364.746 82.126"
              />
              <line
                class="a38c6631-9baa-47f7-99a7-f76dc2e438dd"
                x1="1370.724"
                y1="81.06"
                x2="1376.631"
                y2="80.008"
              />
              <rect
                class="a018e03f-c7a1-4573-8ffb-2476d9912796"
                x="1238.436"
                y="11.674"
                width="296"
                height="63.333"
              />
              {/* <text
          class="b5971788-8437-4acb-960d-39f9225282c7"
          transform="translate(1330.174 50.661)"
        >
          Polędwica
        </text> */}{' '}
              <foreignObject
                x="1238.436"
                y="11.674"
                width="296"
                height="63.333"
              >
                <SVGDropable keyData="contentBeef4"></SVGDropable>
              </foreignObject>
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M562.677,351.436c15.626-9.9,20.258-29.647,25.453-46.15l20.4-64.809,11.55-36.693c.969-3.076-3.857-4.392-4.821-1.329l-20.1,63.856-10.05,31.927c-2.881,9.152-5.461,18.482-9.051,27.39-3.36,8.338-8.142,16.57-15.9,21.491-2.71,1.718-.205,6.047,2.524,4.317Z"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="751.806 230.683 768.62 239.007 766.956 219.616 751.806 230.683"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="827.806 202.683 844.62 211.007 842.956 191.616 827.806 202.683"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="589.299 321.517 608.815 315.329 603.802 335.969 589.299 321.517"
              />
              <path
                class="a077cc25-0b49-4826-b36d-e155dc2225a4"
                d="M608.12,316.074l-4.594,18.913-13.291-13.242,17.885-5.671m1.391-1.49-1.693.537-17.885,5.67-1.571.5,1.168,1.164L602.82,335.7l1.258,1.254.42-1.726,4.594-18.912.419-1.727Z"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="631.654 362.464 649.713 352.818 648.551 374.025 631.654 362.464"
              />
              <path
                class="a077cc25-0b49-4826-b36d-e155dc2225a4"
                d="M649.165,353.678,648.1,373.111l-15.484-10.594,16.549-8.839m1.1-1.719-1.567.837-16.549,8.839-1.454.776,1.36.931,15.485,10.6,1.466,1,.1-1.774,1.065-19.433.1-1.774Z"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="778.654 356.798 796.713 347.151 795.551 368.358 778.654 356.798"
              />
              <path
                class="a077cc25-0b49-4826-b36d-e155dc2225a4"
                d="M796.165,348.011,795.1,367.445,779.616,356.85l16.549-8.839m1.1-1.719-1.567.837-16.549,8.839-1.454.777,1.36.931,15.485,10.594,1.466,1,.1-1.774,1.065-19.433.1-1.774Z"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="777.32 433.131 795.379 423.485 794.218 444.692 777.32 433.131"
              />
              <path
                class="a077cc25-0b49-4826-b36d-e155dc2225a4"
                d="M794.832,424.344l-1.065,19.434-15.484-10.594,16.549-8.84m1.1-1.719-1.566.837-16.549,8.84-1.454.776,1.36.931L793.2,444.6l1.466,1,.1-1.773L795.83,424.4l.1-1.774Z"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="777.987 508.131 796.046 498.485 794.884 519.692 777.987 508.131"
              />
              <path
                class="a077cc25-0b49-4826-b36d-e155dc2225a4"
                d="M795.5,499.344l-1.064,19.434-15.485-10.594,16.549-8.84m1.1-1.719-1.567.837-16.549,8.84-1.454.776,1.361.931L793.869,519.6l1.466,1,.1-1.773L796.5,499.4l.1-1.774Z"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="944.929 221.618 931.213 208.817 927.24 227.87 944.929 221.618"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="1003.97 403.755 1018.794 392.255 1000.618 385.296 1003.97 403.755"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="1041.94 203.148 1029.796 188.846 1023.646 207.312 1041.94 203.148"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="1234.979 512.901 1228.085 495.451 1216.318 510.954 1234.979 512.901"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="1220.377 187.343 1219.609 168.598 1203.396 179.365 1220.377 187.343"
              />
              <polygon
                class="fcd69e55-56f6-4931-bda4-798220e84f17"
                points="922.219 377.352 940.965 376.584 930.197 360.371 922.219 377.352"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M1076.871,188.488c-9.406,11.478-22.6,19.012-36.9,22.61a72.923,72.923,0,0,1-46.872-3.765c-3.2-1.413-6.821-2.971-9.111-5.73a4.842,4.842,0,0,1-.173-6.414c1.817-2.265,4.738-3.673,7.386-4.707,3.93-1.534,8-2.778,12.01-4.081a175.936,175.936,0,0,1,25.85-6.664c3.941-.645,7.894-.891,11.864-1.25,3.122-.283,6.232-.688,9.35-1.017,5.648-.6,11.508-1.147,16.787.752a14.222,14.222,0,0,1,6.041,4.341,19.047,19.047,0,0,1,2.324,3.763c.1.2.323.974.22.443a2.443,2.443,0,0,1,.045.317c.028.5.142,1.014.211,1.51l.325,2.325a2.516,2.516,0,0,0,3.075,1.746,2.567,2.567,0,0,0,1.747-3.075,18.381,18.381,0,0,0-1.306-5.789,21.176,21.176,0,0,0-6.53-7.922c-5.02-3.662-11.572-4.17-17.579-3.857-3.352.175-6.685.59-10.018.97-3.707.422-7.429.631-11.138,1.015a146.088,146.088,0,0,0-27.854,6.068c-4.6,1.416-9.164,2.955-13.722,4.506a35.722,35.722,0,0,0-9.881,4.623c-2.917,2.152-5.255,5.073-5.366,8.846-.122,4.121,2.6,7.386,5.743,9.7a46.964,46.964,0,0,0,11.172,5.551,74.689,74.689,0,0,0,12.5,3.558,79.177,79.177,0,0,0,26.64.586c15.876-2.346,31.445-9.554,42.767-21.038,1.38-1.4,2.687-2.865,3.932-4.384a2.573,2.573,0,0,0,0-3.536A2.52,2.52,0,0,0,1076.871,188.488Z"
              />
              <path
                class="aee77a73-b0f7-45ab-a525-71ba516c3c09"
                d="M863.247,322.443a209.446,209.446,0,0,1-11.627,103.85c-.687,1.807,2.212,2.588,2.892.8A211.681,211.681,0,0,0,866.14,321.645c-.272-1.9-3.163-1.093-2.893.8Z"
              />
            </g>
          </svg>
        </div>
      </Zoom>
    </div>
  );
}
