import React, { useContext } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import DataContext from "@context/DataContext";
import ModalContext from "@context/ModalContext";

const DragDropTransfer = ({ children }) => {
  const {
    columnsInTasks: columns,
    setColumnsInTasks: setColumns,
    level,
  } = useContext(DataContext);
  const { setAlertParams } = useContext(ModalContext);
  const onDragEnd = ({ source, destination }) => {
    // Make sure we have a valid destination
    if (destination === undefined || destination === null) return null;

    // Make sure we're actually moving the item
    if (
      source.droppableId === destination.droppableId &&
      destination.index === source.index
    )
      return null;

    // Set start and end variables
    const start = columns[source.droppableId];
    const end = columns[destination.droppableId];

    // If start is the same as end, we're in the same column
    if (start.id === end.id) {
      // Move the item within the list
      // Start by making a new list without the dragged item

      const newList = start.list.filter((_, idx) => idx !== source.index);

      // Then insert the item at the right location
      newList.splice(destination.index, 0, start.list[source.index]);

      // Then create a new copy of the column object
      const newCol = {
        id: start.id,
        list: newList,
      };

      // Update the state
      setColumns((state) => ({ ...state, [newCol.id]: newCol }));
      return null;
    } else {
      if (level === "hard") {
        let newStartList = [];
        let newEndList = [];
        newStartList = start.list.filter((_, idx) => idx !== source.index);
        newEndList = end.list;
        newEndList.splice(destination.index, 0, start.list[source.index]);
        const newStartCol = {
          id: start.id,
          list: newStartList,
        };
        const newEndCol = {
          id: end.id,
          list: newEndList,
        };

        // Update the state
        setColumns((state) => ({
          ...state,
          [newStartCol.id]: newStartCol,
          [newEndCol.id]: newEndCol,
        }));

        return null;
      } else if (level === "task04") {
        let newStartList = [];
        let newEndList = [];

        if (source.droppableId === "menu") {
          let startType = start.list[source.index].type;
          let startCategory = start.list[source.index].category;
          let endID = destination.droppableId;

          if (startType && !endID.includes(startType)) {
            setAlertParams({
              type: "error",
              text: "Ten element nie należy do tej półtuszy!",
              isOpen: true,
            });
            return null;
          }

          if (startCategory && !endID.includes(startCategory)) {
            setAlertParams({
              type: "error",
              text: "To nie jest prawidłowa kategoria!",
              isOpen: true,
            });
            return null;
          }

          if(end.list.length > 0) {
            setAlertParams({
              type: "error",
              text: "Nie można dodać dwóch elementów do tego samego wiersza",
              isOpen: true,
            });
            return null
          }
        }

        if (
          source.droppableId !== "menu" &&
          destination.droppableId !== "menu"
        ) {
          let startType = start.list[source.index].type;
          let startCategory = start.list[source.index].category;
          let endID = destination.droppableId;

          if (startType && !endID.includes(startType)) {
            setAlertParams({
              type: "error",
              text: "Ten element nie należy do tej półtuszy!",
              isOpen: true,
            });
            return null;
          }

          if (startCategory && !endID.includes(startCategory)) {
            setAlertParams({
              type: "error",
              text: "To nie jest prawidłowa kategoria!",
              isOpen: true,
            });
            return null;
          }

          if(end.list.length > 0) {
            setAlertParams({
              type: "error",
              text: "Nie można dodać dwóch elementów do tego samego wiersza",
              isOpen: true,
            });
            return null
          }
        }

        newStartList = start.list.filter((_, idx) => idx !== source.index);
        newEndList = end.list;

        newEndList.splice(destination.index, 0, start.list[source.index]);
        const newStartCol = {
          id: start.id,
          list: newStartList,
        };
        const newEndCol = {
          id: end.id,
          list: newEndList,
        };

        // Update the state
        setColumns((state) => ({
          ...state,
          [newStartCol.id]: newStartCol,
          [newEndCol.id]: newEndCol,
        }));

        return null;
      } else {
        // If start is different from end, we need to update multiple columns
        // Filter the start list like before

        let newStartList = [];
        let newEndList = [];
        // console.log({ source, destination, start, end });
        if (source.droppableId == "menu") {
          // console.log('source.droppableId == "menu"');
          // movment between menu and elements
          newStartList = start.list.filter((_, idx) => idx !== source.index); //list without drag element
          const prevNewEndList = end.list;
          // console.log({ newStartList, prevNewEndList });
          //replace element if something is in the drop zone
          if (prevNewEndList.length) {
            newStartList.push(prevNewEndList[0]);
            // console.log({ newStartList, prevNewEndList });
          }
          newEndList.push(start.list[source.index]);
          // console.log({ newStartList, prevNewEndList });
        } else if (source.droppableId !== "menu" && end.id !== "menu") {
          // console.log('source.droppableId !== "menu" && end.id !== "menu"');

          const prevNewStartList = start.list;
          const prevNewEndList = end.list;

          newStartList = start.list.filter((_, idx) => idx !== source.index);
          newEndList = [];
          // console.log({ newStartList, prevNewEndList });
          if (prevNewEndList.length) {
            newStartList.push(prevNewEndList[0]);
            // console.log({ newStartList, prevNewEndList });
          }
          if (prevNewStartList.length) {
            newEndList.push(prevNewStartList[0]);
            // console.log({ newStartList, prevNewEndList });
          }
        } else {
          // console.log("else");
          newStartList = start.list.filter((_, idx) => idx !== source.index);
          newEndList = end.list;
          newEndList.splice(destination.index, 0, start.list[source.index]);
          // console.log({ newStartList });
        }

        // Create a new start column
        const newStartCol = {
          id: start.id,
          list: newStartList,
        };

        // Make a new end list array
        // const newEndList = end.list;
        // const prevNewEndList = end.list;
        // console.log(prevNewEndList,prevNewEndList);
        // const newEndList = [];

        // Insert the item into the end list
        // newEndList.splice(destination.index, 0, start.list[source.index]);

        // Create a new end column
        const newEndCol = {
          id: end.id,
          list: newEndList,
        };

        // Update the state
        setColumns((state) => ({
          ...state,
          [newStartCol.id]: newStartCol,
          [newEndCol.id]: newEndCol,
        }));

        return null;
      }
    }
  };

  return <DragDropContext onDragEnd={onDragEnd}>{children}</DragDropContext>;
};

export default DragDropTransfer;
