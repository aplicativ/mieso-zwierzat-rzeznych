import React, { useContext } from "react";
import RootRef from "@material-ui/core/RootRef";
import { Droppable } from "react-beautiful-dnd";
import DataContext from "@context/DataContext";

import List from "@material-ui/core/List";
import Box from "@mui/material/Box";
import ListTask04 from "./list-task04";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  list: {
    paddingInlineStart: "0",
    backgroundColor: "#f4f4f4",
    width: "100%",
    padding: "0",
  },
}));

const ItemTask04 = ({ keyData }) => {
  const { columnsInTasks } = useContext(DataContext);

  const { [keyData]: data } = columnsInTasks;

  const classes = useStyles();

  // sort items alphabetically

  const sortAlphabetically = data.list.sort((a, b) => a.text.localeCompare(b.text));
  const sortCategory = sortAlphabetically.sort((a, b) => a.category.localeCompare(b.category));
 
  const currentFirstIndexElement = data.list.findIndex(
    (element) => element.category === "Elements"
  );

  const currentFirstIndexDish = data.list.findIndex(
    (element) => element.category === "Dish"
  );

  const currentFirstIndextool = data.list.findIndex(
    (element) => element.category === "Methods"
  );

  return (
    <Droppable droppableId={data.id}>
      {(provided) => (
        <RootRef rootRef={provided.innerRef}>
          <List className={classes.list}>
            {sortCategory.map((itemObject, index) => {
              return (
                <>
                  {currentFirstIndexElement === index && (
                    <Box
                      style={{
                        textAlign: "center",
                        fontWeight: 600,
                        fontSize: 15,
                        padding: "10px 0",
                        backgroundColor: "#464343",
                        color: "white",
                        fontFamily: ["OpenSans", "sans-serif"].join(","),
                      }}
                    >
                      Element
                    </Box>
                  )}
                  {currentFirstIndexDish === index && (
                    <Box
                      style={{
                        textAlign: "center",
                        fontWeight: 600,
                        fontSize: 15,
                        padding: "10px 0",
                        backgroundColor: "#464343",
                        color: "white",
                        fontFamily: ["OpenSans", "sans-serif"].join(","),
                      }}
                    >
                      Potrawa
                    </Box>
                  )}
                  {currentFirstIndextool === index && (
                    <Box
                      style={{
                        textAlign: "center",
                        fontWeight: 600,
                        fontSize: 15,
                        padding: "10px 0",
                        backgroundColor: "#464343",
                        color: "white",
                        fontFamily: ["OpenSans", "sans-serif"].join(","),
                      }}
                    >
                      Metoda obróbki
                    </Box>
                  )}

                  <ListTask04
                    index={index}
                    itemObject={itemObject}
                    columnID={data.id}
                  />
                </>
              );
            })}
            {provided.placeholder}
          </List>
        </RootRef>
      )}
    </Droppable>
  );
};

export default ItemTask04;
