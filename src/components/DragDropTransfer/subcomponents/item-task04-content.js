import React, { useContext } from "react";
import RootRef from "@material-ui/core/RootRef";
import { Droppable } from "react-beautiful-dnd";
import DataContext from "@context/DataContext";

import List from "@material-ui/core/List";
import ListTask04 from "./list-task04";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  list: {
    paddingInlineStart: "0",
    // backgroundColor: "#f4f4f4",
    width: "100%",
    padding: "0",
    minHeight: 30
  },
}));

const ItemTask04 = ({ keyData, type }) => {
  const { columnsInTasks } = useContext(DataContext);

  const { [keyData]: data } = columnsInTasks;

  const classes = useStyles();

  // const filteredData = data.list.filter((item) => item.category === type);

  const filteredData = data.list;

  return (
    <Droppable droppableId={data.id}>
      {(provided) => (
        <RootRef rootRef={provided.innerRef}>
          <List className={classes.list}>
            {filteredData.map((itemObject, index) => {
              return (
                <>
                  <ListTask04
                    index={index}
                    itemObject={itemObject}
                    columnID={data.id}
                  />
                </>
              );
            })}
            {provided.placeholder}
          </List>
        </RootRef>
      )}
    </Droppable>
  );
};

export default ItemTask04;
