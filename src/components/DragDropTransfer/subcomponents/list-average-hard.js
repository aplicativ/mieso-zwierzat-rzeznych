import React from "react";

import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import { Draggable } from "react-beautiful-dnd";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  listItem: {
    position: "relative",
    paddingLeft: "2px",
    marginLeft: "2px",
    width: "150px",
    color:"black"
  },
}));

const ListItemCustom = ({ itemObject, index }) => {
  const classes = useStyles();

  return (
    <Draggable draggableId={itemObject.key} key={itemObject.key} index={index}>
      {(provided) => (
        <ListItem
          key={itemObject.key}
          role={undefined}
          dense
          button
          ContainerComponent="li"
          ContainerProps={{ ref: provided.innerRef }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <ListItemText
            primary={
              <div type="body2" className={classes.listItem}>
                {itemObject.text}
              </div>
            }
            style={{ paddingLeft: "0 !import", paddingRight: "0 !import" }}
          />
          <ListItemSecondaryAction style={{ width: "0" }}>
            {/* <IconButton
              edge="end"
              aria-label="comments"
              question-uid={itemObject.key}
            >
            </IconButton> */}
          </ListItemSecondaryAction>
        </ListItem>
      )}
    </Draggable>
  );
};

export default ListItemCustom;
